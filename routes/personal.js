const { Router } = require('express');
const middlewares = require('../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const personalService = require('../services/personal');
const route = Router();

module.exports = (app) => {

    function nocache(req, res, next) {
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.header('Expires', '-1');
        res.header('Pragma', 'no-cache');
        res.header('Access-Control-Allow-Origin', '*');

        /*header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
        header('Access-Control-Allow-Headers: *');*/
        next();
    }

    app.use('/personal', route);
    app.use(errors());

    route.get('/', function (req, res) {
        console.log('personal');
        res.send('Hello World!')
    })

    route.post('/list', nocache, 
        celebrate({
            [Segments.BODY]: Joi.object({
                page: Joi.number().allow(null),
                limit: Joi.number().allow(null),
                search: Joi.string().allow(null,""),
                sort: Joi.string().allow(null,""),
                order: Joi.string().allow(null,""),
            })
        }),
        async function (req, res) {
            console.log('personal list');
            const list = await personalService.list(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : list.list , total : list.count[0].count,  })
    })

     route.get('/detail/:PersonalId', nocache, async function (req, res) {
        console.log('personal detail');
        const detail = await personalService.detail(req.params.PersonalId);
        if(detail.error_msg) res.status(200).json({code : 0, isSuccess : false , error_msg : detail.error_msg })
        else res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.get('/detailByIDCard/:IDCard', nocache, async function (req, res) {
        console.log('personal detail');
        const detail = await personalService.detailByIDCard(req.params.IDCard);
        if(detail.error_msg) res.status(200).json({code : 0, isSuccess : false , error_msg : detail.error_msg })
        else res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.get('/detailByUser/:UserID', nocache, async function (req, res) {
        console.log('personal detail');
        const detail = await personalService.detailByUser(req.params.UserID);
        if(detail.error_msg) res.status(200).json({code : 0, isSuccess : false , error_msg : detail.error_msg })
        else res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.get('/list_year', nocache, async function (req, res) {
        console.log('partner detail');
        const list = await personalService.getYear();
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.get('/list_round', nocache, async function (req, res) {
        console.log('partner detail');
        const list = await personalService.getRound(req.query.BgtYear);
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.get('/list_prefix', nocache, async function (req, res) {
        const list = await personalService.getPrefix();
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.get('/list_position', nocache, async function (req, res) {
        const list = await personalService.getPosition();
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.get('/list_personal_type', nocache, async function (req, res) {
        const list = await personalService.getPersonalType();
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.get('/list_org', nocache, async function (req, res) {
        console.log('partner detail');
        const list = await personalService.getOrganization(req.query.BgtYear, req.query.RoundId, req.query.isOperation);
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.post('/list_personal_org',nocache, 
        celebrate({
            [Segments.BODY]: Joi.object({
                page: Joi.number().allow(null),
                limit: Joi.number().allow(null),
                search: Joi.string().allow(null,""),
                sort: Joi.string().allow(null,""),
                order: Joi.string().allow(null,""),
                OrgId: Joi.string().allow(null),
                RoundId: Joi.string().allow(null),
                PositionId: Joi.string().allow(null),
            })
        }),
        async function (req, res) {
            console.log('list_personal_cattegory list');
            const list = await personalService.listPersonalOrg(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : list.list , total : list.count[0].count })
    })


    route.get('/list_personal_org',nocache, 
        celebrate({
            [Segments.QUERY]: Joi.object({
                page: Joi.number().allow(null),
                limit: Joi.number().allow(null),
                search: Joi.string().allow(null,""),
                sort: Joi.string().allow(null,""),
                order: Joi.string().allow(null,""),
                OrgId: Joi.string().allow(null),
                RoundId: Joi.string().allow(null),
                PositionId: Joi.string().allow(null),
            })
        }),
        async function (req, res) {
            console.log('list_personal_cattegory list');
            const list = await personalService.listPersonalOrg(req.query);
            res.status(200).json({code : 0, isSuccess : true , data : list.list , total : list.count[0].count })
    })
    route.get('/list_personal_org_all',nocache, 
        celebrate({
            [Segments.QUERY]: Joi.object({
                page: Joi.number().allow(null),
                limit: Joi.number().allow(null),
                search: Joi.string().allow(null,""),
                sort: Joi.string().allow(null,""),
                order: Joi.string().allow(null,""),
                OrgId: Joi.string().allow(null),
                RoundId: Joi.string().allow(null),
                PositionId: Joi.string().allow(null),
            })
        }),
        async function (req, res) {
            console.log('list_personal_cattegory list');
            const list = await personalService.listPersonalOrgAll(req.query);
            res.status(200).json({code : 0, isSuccess : true , data : list.list , total : list.count })
    })

    route.post('/absence',nocache, 
        celebrate({
            [Segments.BODY]: Joi.object({
                PersonalID: Joi.number().required(),
                date: Joi.date().required(),
                type: Joi.string().allow(null,""),
            })
        }),
        async function (req, res) {
            console.log('absence');
            const detail = await personalService.getAbsence(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : detail})
    })
    
};