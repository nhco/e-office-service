const { Op, QueryTypes } = require('sequelize');
const { sequelize } = require('../models/index')
const { insertSQL, updateSQL, deleteSQL } = require('../services/tool')
const db = require("../models");
const moment = require("moment");
const address_type  = db.address_type;
const fields_log = [
  'Provider',
  'CategoryName',
  'Method',
  'UserId',
  'LogDate',
  'Token',
  'URL',
  'Data',
  'UserName',
];

exports.list = async (query) => {
  var whereSearch = '';
  if(query.search && query.search) {
    whereSearch += ` AND ( FirstName LIKE '%${query.search}%' OR LastName LIKE '%${query.search}%' OR IDCard LIKE '%${query.search}%' ) `;
  }
  if(query.startDate && query.endDate) {
    whereSearch += ` AND ( Personal.UpdateDate BETWEEN '${query.startDate}' AND '${query.endDate}' ) `;
  }
  let sql = `SELECT 
      Personal.* ,ReligionName, PrefixName,PerType, DocPath, 
      Pro.Province, 
      Dis.District, 
      Sub.SubDistrict,
      Pro2.Province AS ProvinceName2,
      Dis2.District AS DistrictName2,
      Sub2.SubDistrict AS SubDistrictName2
    FROM tblpersonal AS Personal 
    LEFT JOIN tblpersonal_religion AS Religion ON Religion.ReligionId=Personal.ReligionId 
    LEFT JOIN tblpersonal_prefix AS Prefix ON Prefix.PrefixId=Personal.PrefixId
    LEFT JOIN tblmaster_province AS Pro ON Pro.ProvinceCode=Personal.ProvinceId
    LEFT JOIN tblmaster_district AS Dis ON Dis.DistrictCode=Personal.Zone
    LEFT JOIN tblmaster_sub_district AS Sub ON Sub.SubDistrictCode=Personal.District 
    LEFT JOIN tblmaster_province AS Pro2 ON Pro2.ProvinceCode=Personal.ProvinceId2
    LEFT JOIN tblmaster_district AS Dis2 ON Dis2.DistrictCode=Personal.Zone2
    LEFT JOIN tblmaster_sub_district AS Sub2 ON Sub2.SubDistrictCode=Personal.District2
    LEFT JOIN tblpersonal_type AS Type ON Type.PerTypeId=Personal.PerTypeId 
    LEFT JOIN tblintra_edoc_center AS Doc ON Doc.DocId=Personal.PictureFile 
    WHERE Personal.DeleteStatus='N'  AND Personal.EnableStatus='Y' AND Personal.ResignStatus='N' ${whereSearch}
    ORDER BY Personal.UpdateDate DESC`;

  if(query.page && query.limit) {
    sql += ` LIMIT ${((query.page-1)*query.limit)} , ${query.limit}`;
  }
  console.log(sql);
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  let sqlCount = `SELECT COUNT(*) as count FROM tblpersonal  AS Personal WHERE DeleteStatus='N' ${whereSearch}`;
  const count = await sequelize.query(sqlCount , { type: QueryTypes.SELECT });

  if(query.currentUser){
    const log = {
      Provider : 'API',
      Method : 'POST',
      UserId : query.currentUser.UserID || '',
      LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
      Token : query.currentUser.token || '',
      URL : 'personal/list',
      Data: sql,
      UserName: query.currentUser.Username || ''
    };
    const insert = await insertSQL('provider_log', log, fields_log);
  }
  return { list , count }
};

exports.detail = async (PersonalId, currentUser) => {
  const sql = `SELECT Personal.* ,
        ReligionName, 
        Prefix.PrefixName,
        PrefixEng.PrefixName AS PrefixNameEng,
        PerType, DocPath, 
        Pro.Province, 
        Pro.ProvinceCode,
        Dis.District, 
        Dis.DistrictCode,
        Sub.SubDistrict,
        Sub.SubDistrictCode,
        Pro2.Province AS ProvinceName2,
        Dis2.District AS DistrictName2,
        Sub2.SubDistrict AS SubDistrictName2
      FROM tblpersonal AS Personal 
      LEFT JOIN tblpersonal_religion AS Religion ON Religion.ReligionId=Personal.ReligionId 
      LEFT JOIN tblpersonal_prefix AS Prefix ON Prefix.PrefixId=Personal.PrefixId
      LEFT JOIN tblpersonal_prefix AS PrefixEng ON PrefixEng.PrefixId=Personal.PrefixIdE
      LEFT JOIN tblmaster_province AS Pro ON Pro.ProvinceCode=Personal.ProvinceId
      LEFT JOIN tblmaster_district AS Dis ON Dis.DistrictCode=Personal.Zone
      LEFT JOIN tblmaster_sub_district AS Sub ON Sub.SubDistrictCode=Personal.District 
      LEFT JOIN tblmaster_province AS Pro2 ON Pro2.ProvinceCode=Personal.ProvinceId2
      LEFT JOIN tblmaster_district AS Dis2 ON Dis2.DistrictCode=Personal.Zone2
      LEFT JOIN tblmaster_sub_district AS Sub2 ON Sub2.SubDistrictCode=Personal.District2
      LEFT JOIN tblpersonal_type AS Type ON Type.PerTypeId=Personal.PerTypeId 
      LEFT JOIN tblintra_edoc_center AS Doc ON Doc.DocId=Personal.PictureFile 
      WHERE Personal.PersonalID='${PersonalId}' AND Personal.DeleteStatus='N'  AND Personal.EnableStatus='Y' AND Personal.ResignStatus='N'`;
  let detail = await sequelize.query(sql , { type: QueryTypes.SELECT });

  if(detail && detail.BirthDate=='0000-00-00') detail.BirthDate = null;

  if(detail==null || detail[0]==null) {
    return { error_msg : 'Not found data'}
  }
  detail[0].organization = await this.getOrgByPersonalId(detail[0].PersonalId);
  if(currentUser){
    const log = {
      Provider : 'API',
      Method : 'GET',
      UserId : currentUser.UserID || '',
      LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
      Token : currentUser.token || '',
      URL : 'personal/detail',
      Data: sql,
      UserName: currentUser.Username || ''
    };
    const insert = await insertSQL('provider_log', log, fields_log);
  }
  return detail
};

exports.detailByID = async (PersonalId, currentUser) => {
  const sql = `SELECT Personal.* ,
        ReligionName, 
        Prefix.PrefixName,
        PrefixEng.PrefixName AS PrefixNameEng,
        PerType, DocPath, 
        Pro.Province, 
        Pro.ProvinceCode,
        Dis.District, 
        Dis.DistrictCode,
        Sub.SubDistrict,
        Sub.SubDistrictCode,
        Pro2.Province AS ProvinceName2,
        Dis2.District AS DistrictName2,
        Sub2.SubDistrict AS SubDistrictName2
      FROM tblpersonal AS Personal 
      LEFT JOIN tblpersonal_religion AS Religion ON Religion.ReligionId=Personal.ReligionId 
      LEFT JOIN tblpersonal_prefix AS Prefix ON Prefix.PrefixId=Personal.PrefixId
      LEFT JOIN tblpersonal_prefix AS PrefixEng ON PrefixEng.PrefixId=Personal.PrefixIdE
      LEFT JOIN tblmaster_province AS Pro ON Pro.ProvinceCode=Personal.ProvinceId
      LEFT JOIN tblmaster_district AS Dis ON Dis.DistrictCode=Personal.Zone
      LEFT JOIN tblmaster_sub_district AS Sub ON Sub.SubDistrictCode=Personal.District 
      LEFT JOIN tblmaster_province AS Pro2 ON Pro2.ProvinceCode=Personal.ProvinceId2
      LEFT JOIN tblmaster_district AS Dis2 ON Dis2.DistrictCode=Personal.Zone2
      LEFT JOIN tblmaster_sub_district AS Sub2 ON Sub2.SubDistrictCode=Personal.District2
      LEFT JOIN tblpersonal_type AS Type ON Type.PerTypeId=Personal.PerTypeId 
      LEFT JOIN tblintra_edoc_center AS Doc ON Doc.DocId=Personal.PictureFile 
      WHERE Personal.PersonalID='${PersonalId}' AND Personal.DeleteStatus='N'  AND Personal.EnableStatus='Y' AND Personal.ResignStatus='N'`;
  console.log(sql);
  const detail = await sequelize.query(sql , { type: QueryTypes.SELECT });
  console.log(detail);
  if(detail==null || detail[0]==null) {
    return { error_msg : 'Not found data'}
  }

  let fullDetail = await getFullDetail(detail);
  fullDetail.position = fullDetail.position ? fullDetail.position[0] : null
  delete fullDetail.education

  if(currentUser){
    const log = {
      Provider : 'API',
      Method : 'GET',
      UserId : currentUser.UserID || '',
      LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
      Token : currentUser.token || '',
      URL : 'personal/detail',
      Data: sql,
      UserName: currentUser.Username || ''
    };
    const insert = await insertSQL('provider_log', log, fields_log);console.log(insert);
  }
  return fullDetail
};

exports.detailByIDCard = async (IDCard, currentUser) => {
  const sql = `SELECT Personal.* ,
        ReligionName, 
        Prefix.PrefixName,
        PrefixEng.PrefixName AS PrefixNameEng,
        PerType, DocPath, 
        Pro.Province, 
        Pro.ProvinceCode,
        Dis.District, 
        Dis.DistrictCode,
        Sub.SubDistrict,
        Sub.SubDistrictCode,
        Pro2.Province AS ProvinceName2,
        Dis2.District AS DistrictName2,
        Sub2.SubDistrict AS SubDistrictName2
      FROM tblpersonal AS Personal 
      LEFT JOIN tblpersonal_religion AS Religion ON Religion.ReligionId=Personal.ReligionId 
      LEFT JOIN tblpersonal_prefix AS Prefix ON Prefix.PrefixId=Personal.PrefixId
      LEFT JOIN tblpersonal_prefix AS PrefixEng ON PrefixEng.PrefixId=Personal.PrefixIdE
      LEFT JOIN tblmaster_province AS Pro ON Pro.ProvinceCode=Personal.ProvinceId
      LEFT JOIN tblmaster_district AS Dis ON Dis.DistrictCode=Personal.Zone
      LEFT JOIN tblmaster_sub_district AS Sub ON Sub.SubDistrictCode=Personal.District 
      LEFT JOIN tblmaster_province AS Pro2 ON Pro2.ProvinceCode=Personal.ProvinceId2
      LEFT JOIN tblmaster_district AS Dis2 ON Dis2.DistrictCode=Personal.Zone2
      LEFT JOIN tblmaster_sub_district AS Sub2 ON Sub2.SubDistrictCode=Personal.District2
      LEFT JOIN tblpersonal_type AS Type ON Type.PerTypeId=Personal.PerTypeId 
      LEFT JOIN tblintra_edoc_center AS Doc ON Doc.DocId=Personal.PictureFile 
      WHERE IDCard='${IDCard}' AND Personal.DeleteStatus='N'  AND Personal.EnableStatus='Y' AND Personal.ResignStatus='N'`;
  console.log(sql);
  const detail = await sequelize.query(sql , { type: QueryTypes.SELECT });
  console.log(detail);
  if(detail==null || detail[0]==null) {
    return { error_msg : 'Not found data'}
  }
  if(currentUser){
    const log = {
      Provider : 'API',
      Method : 'GET',
      UserId : currentUser.UserID || '',
      LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
      Token : currentUser.token || '',
      URL : 'personal/detailByIDCard',
      Data: sql,
      UserName: currentUser.Username || ''
    };
    const insert = await insertSQL('provider_log', log, fields_log);
  }
  return await getFullDetail(detail);
};


exports.detailByUser = async (UserID,currentUser) => {
  const sql = `SELECT Personal.* ,
        ReligionName, 
        Prefix.PrefixName,
        PrefixEng.PrefixName AS PrefixNameEng,
        PerType, DocPath, 
        Pro.Province, 
        Pro.ProvinceCode,
        Dis.District, 
        Dis.DistrictCode,
        Sub.SubDistrict,
        Sub.SubDistrictCode,
        Pro2.Province AS ProvinceName2,
        Dis2.District AS DistrictName2,
        Sub2.SubDistrict AS SubDistrictName2,
        User.UserID,
        User.Username,
        User.UserType,
        User.FirstName AS UserFirstName,
        User.LastName AS UserLastName,
        User.Email AS UserEmail,
        User.LastLogin,
        User.Active
      FROM tblpersonal AS Personal 
      LEFT JOIN authen_users AS User ON User.PersonalId=Personal.PersonalId 
      LEFT JOIN tblpersonal_religion AS Religion ON Religion.ReligionId=Personal.ReligionId 
      LEFT JOIN tblpersonal_prefix AS Prefix ON Prefix.PrefixId=Personal.PrefixId
      LEFT JOIN tblpersonal_prefix AS PrefixEng ON PrefixEng.PrefixId=Personal.PrefixIdE
      LEFT JOIN tblmaster_province AS Pro ON Pro.ProvinceCode=Personal.ProvinceId
      LEFT JOIN tblmaster_district AS Dis ON Dis.DistrictCode=Personal.Zone
      LEFT JOIN tblmaster_sub_district AS Sub ON Sub.SubDistrictCode=Personal.District 
      LEFT JOIN tblmaster_province AS Pro2 ON Pro2.ProvinceCode=Personal.ProvinceId2
      LEFT JOIN tblmaster_district AS Dis2 ON Dis2.DistrictCode=Personal.Zone2
      LEFT JOIN tblmaster_sub_district AS Sub2 ON Sub2.SubDistrictCode=Personal.District2
      LEFT JOIN tblpersonal_type AS Type ON Type.PerTypeId=Personal.PerTypeId 
      LEFT JOIN tblintra_edoc_center AS Doc ON Doc.DocId=Personal.PictureFile 
      WHERE User.UserID='${UserID}' AND Personal.DeleteStatus='N'  AND Personal.EnableStatus='Y' AND Personal.ResignStatus='N'`;
  console.log(sql);
  const detail = await sequelize.query(sql , { type: QueryTypes.SELECT });
  console.log(detail);
  if(detail==null || detail[0]==null) {
    return { error_msg : 'Not found data'}
  }
  const log = {
    Provider : 'API',
    Method : 'GET',
    UserId : currentUser.UserID || '',
    LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
    Token : currentUser.token || '',
    URL : 'personal/detailByUser',
    Data: sql,
    UserName: currentUser.Username || ''
  };
  const insert = await insertSQL('provider_log', log, fields_log);console.log(insert);
  return await getFullDetail(detail);
};

const getFullDetail = async (detail) => {

  if(detail[0]!=null) {
    if(detail[0].WelfareStatus=='Y') detail[0].WelfareStatusName = 'ได้รับสวัสดิการสิทธิ์ สช.';
    else if(detail[0].WelfareStatus=='GOV') detail[0].WelfareStatusName = 'สิทธิราชการ';
    else if(detail[0].WelfareStatus=='SSO') detail[0].WelfareStatusName = 'สิทธิประกันสังคม';
    else if(detail[0].WelfareStatus=='OTHER') detail[0].WelfareStatusName = 'สิทธิอื่นๆ';
  }

  const sqlEducation = `SELECT Edu.* ,Deg.Degree,Ins.Institute,Lev.Level 
      FROM tblpersonal_education AS Edu 
      LEFT JOIN tblpersonal_edu_degree AS Deg ON Deg.DegreeId=Edu.DegreeId 
      LEFT JOIN tblpersonal_edu_institute AS Ins ON Ins.InstituteId=Edu.InstituteId 
      LEFT JOIN tblpersonal_edu_level AS Lev ON Lev.LevelId=Edu.LevelId 
      WHERE Edu.PersonalId=${detail[0].PersonalId} AND Edu.DeleteStatus='N'
      ORDER BY Edu.UpdateDate DESC`;
      console.log(sqlEducation);
  const education = await sequelize.query(sqlEducation , { type: QueryTypes.SELECT });

  const sqlPosition = `SELECT Per.* ,Po.Position,PosType.PosTypeName
    FROM tblpersonal_position AS Per
    LEFT JOIN tblposition AS Po ON Po.PositionId=Per.PositionId
    LEFT JOIN tblposition_type AS PosType ON PosType.PosTypeId=Per.PosTypeId
    WHERE PersonalId=${detail[0].PersonalId} AND Per.DeleteStatus='N' 
    ORDER BY Per.UpdateDate DESC`;
  const position = await sequelize.query(sqlPosition , { type: QueryTypes.SELECT });

  const sqlParent = `SELECT Fam.* ,ReligionName, PrefixName, IF(Fam.RelationCode=1,'บิดา','มารดา') AS ParentType
    FROM tblpersonal_family AS Fam 
    LEFT JOIN tblpersonal_religion AS Religion ON Religion.ReligionId=Fam.ReligionId  
    LEFT JOIN tblpersonal_prefix AS Prefix ON Prefix.PrefixId=Fam.PrefixId 
    WHERE PersonalId=${detail[0].PersonalId} AND Fam.DeleteStatus='N' AND Fam.RelationCode IN (1,2) 
    ORDER BY Fam.UpdateDate DESC`;
    console.log(sqlParent);
  const parents = await sequelize.query(sqlParent , { type: QueryTypes.SELECT });

  const sqlChild = `SELECT Fam.* ,ReligionName, PrefixName, 'บุตร/ธิดา' AS ParentType
    FROM tblpersonal_family AS Fam 
    LEFT JOIN tblpersonal_religion AS Religion ON Religion.ReligionId=Fam.ReligionId  
    LEFT JOIN tblpersonal_prefix AS Prefix ON Prefix.PrefixId=Fam.PrefixId 
    WHERE PersonalId=${detail[0].PersonalId} AND Fam.DeleteStatus='N' AND Fam.RelationCode=4 
    ORDER BY Fam.UpdateDate DESC`;
    console.log(sqlChild);
  const childs = await sequelize.query(sqlChild , { type: QueryTypes.SELECT });

  const sqlSpouse = `SELECT Fam.* ,ReligionName, PrefixName, 'คู่สมรส' AS ParentType 
    FROM tblpersonal_family AS Fam 
    LEFT JOIN tblpersonal_religion AS Religion ON Religion.ReligionId=Fam.ReligionId  
    LEFT JOIN tblpersonal_prefix AS Prefix ON Prefix.PrefixId=Fam.PrefixId 
    WHERE PersonalId=${detail[0].PersonalId} AND Fam.DeleteStatus='N' AND Fam.RelationCode=3 
    ORDER BY Fam.UpdateDate  DESC`;
    console.log(sqlSpouse);
  const spouses = await sequelize.query(sqlSpouse , { type: QueryTypes.SELECT });

  const sqlPayment = `SELECT owner.* ,BankName, BranchName
    FROM tblmou_owner_bank AS owner 
    LEFT JOIN tblmou_bank AS bank ON bank.BankCode = owner.BankCode
    LEFT JOIN tblmou_bank_branch AS branch ON owner.BranchCode = branch.BranchCode
    WHERE PartnerCode='${detail[0].PersonalCode}' AND owner.DeleteStatus='N' 
    ORDER BY owner.UpdateDate DESC`;
    console.log(sqlPayment);
  const payment = await sequelize.query(sqlPayment , { type: QueryTypes.SELECT });

  const organization = await this.getOrgByPersonalId(detail[0].PersonalId);

  if(detail && detail[0].BirthDate=='0000-00-00') detail[0].BirthDate = null;
  return {...detail[0], education ,position , parents, childs, payment, spouses, organization }
}

exports.create = async (data, transaction) => {
  const created = await address_type.create(
    data, transaction);
  return created
};

exports.update = async (id, data, transaction) => {
  const updated = await address_type.update(
    data,
    { where : { id : id } }, transaction);
  const detail  = await this.detail(id);
  return detail
};

exports.delete = async (id, transaction) => {
  const deleted = await address_type.destroy({
     where :  { id : id } , 
  }, transaction);
  return deleted
};

exports.getPosition = async () => {
  let sql = `SELECT * FROM tblposition 
    WHERE EnableStatus = 'Y' AND  DeleteStatus = 'N' ORDER BY Position ASC`;
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return data;
};

exports.getPrefix = async () => {
  let sql = `SELECT * FROM tblpersonal_prefix 
    WHERE EnableStatus = 'Y' AND  StatusPrefix = 'TH' ORDER BY PrefixId ASC`;
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return data;
};

exports.getPersonalType = async () => {
  let sql = `SELECT * FROM tblpersonal_type 
    WHERE EnableStatus = 'Y' AND  DeleteStatus = 'N' ORDER BY PerTypeCode ASC`;
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return data;
};

exports.getPersonalMate = async () => {
  let sql = `SELECT * FROM tblpersonal_mate 
    WHERE DeleteStatus = 'N' ORDER BY MateId ASC`;
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return data;
};

exports.getBank = async () => {
  let sql = `SELECT BankId, BankCode, BankName, BankCodeId, ShortName, PrefixBranch, FirstPositionBranch, LastPositionBranch
    FROM tblmou_bank 
    WHERE DeleteStatus = 'N' ORDER BY BankName ASC`;
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  const data = []
  
  for await(let item of list ) {
    let sql = `SELECT BranchId,BranchCode,BankId,BankCode,BranchName,BranchAddress 
    FROM tblmou_bank_branch 
    WHERE DeleteStatus = 'N' ORDER BY BranchCode ASC`;
    item.Branch = await sequelize.query(sql , { type: QueryTypes.SELECT });

    data.push(item)
  }
  return data;
};

exports.getYear = async (DocCode) => {
  console.log(DocCode);
  let sql = `SELECT BgtYear FROM tblbudget_init_year 
    WHERE CloseStatus = 'N' ORDER BY BgtYear DESC`;
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return data;
};

exports.getRound = async (BgtYear) => {
  console.log(BgtYear);
  let sql = `SELECT RoundId,Year,Round,RoundCode,StartDate,EndDate,CurrentUse 
    FROM tblstructure_operation_round  
    WHERE  StartDate <= NOW()  and  EnableStatus="Y"  and  DeleteStatus="N"  and  Year="${BgtYear}"  
    ORDER BY StartDate DESC`;
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return data;
};

exports.getCurrentRound = async () => {
  let sql = `SELECT RoundId,Year,Round,RoundCode,StartDate,EndDate,CurrentUse 
    FROM tblstructure_operation_round  
    WHERE  StartDate <= NOW() and  EnableStatus="Y" and  DeleteStatus="N"    
    ORDER BY StartDate DESC LIMIT 1`;
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
  //console.log(data);
  return data && data.length>0 ? data[0] : null;
};

exports.getOrganization = async (BgtYear,RoundId,isOperation, isPersonal, currentUser) => {
 
    let sql = `SELECT ManagerId,ManagerId AS RefId,ManagerParent AS RefParent,ParentType,
      ManagerName,'M' AS Type ,CONCAT(p.FirstName,' ',p.LastName) FullName,m.ApproveLeave,
      p.PersonalId,p.FirstName,p.LastName,p.Email,p.PersonalCodeReal,p.Mobile,p.IDCard,p.BirthDate,
      po.Position
      FROM tblstructure_manager AS m
      LEFT JOIN tblpersonal AS p ON p.PersonalId=m.PersonalId
      LEFT JOIN (
        SELECT PersonalId, MAX(PositionId) AS PositionId
        FROM tblpersonal_position
        GROUP BY PersonalId 
      ) AS pp ON pp.PersonalId = p.PersonalId
      LEFT JOIN tblposition AS po ON pp.PositionId=po.PositionId
      WHERE  m.DeleteStatus="N" AND isReal=1 AND ManagerParent=0 AND ParentType='R'
      AND ManagerRound=${RoundId}
      ORDER BY ManagerId DESC`;
    console.log(sql);
    var result = [];
    const list = await sequelize.query(sql , { type: QueryTypes.SELECT });
    if(list.length>0){
      for await(const item of list ) {
        console.log('->',item);
        const child = await this.childsOrg(RoundId,item.ManagerId,'Root',isOperation, isPersonal);
        if(child.length>0){
          item.childs = child;
        }
        result.push(item);
      }
    }
    if(currentUser){
      const log = {
        Provider : 'API',
        Method : 'GET',
        UserId : currentUser.UserID || '',
        LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
        Token : currentUser.token || '',
        URL : 'personal/list_org',
        Data: sql,
        UserName: currentUser.Username || ''
      };
      const insert = await insertSQL('provider_log', log, fields_log);
    }
    //const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
    return result;
};


exports.childsOrg = async (RoundId,ParentId,ParentType,isOperation=0, isPersonal=0) => {
  var result = [];
  var sqlOrg;
  var sqlManager;
  var sqlPersonal;

  let whereType =  `isReal=1`
  if(isOperation==1)  whereType = ` isOperation=1 `
 
  sqlOrg = `SELECT OrgId,OrgId AS RefId,OrgParent AS RefParent,ParentType ,OrgName,OrgShortName,'O' AS Type
    FROM tblstructure_operation   
    WHERE  DeleteStatus="N"  AND ${whereType} AND OrgParent=${ParentId} AND OrgRound=${RoundId}`;
  sqlManager = `SELECT ManagerId,ManagerId AS RefId,ManagerParent AS RefParent,ParentType,
    ManagerName,'M' AS Type ,CONCAT(p.FirstName,' ',p.LastName) FullName,m.ApproveLeave,
    p.PersonalId,p.FirstName,p.LastName,p.Email,p.PersonalCodeReal,p.Mobile,p.IDCard,p.BirthDate,
    po.Position
    FROM tblstructure_manager AS m  
    LEFT JOIN tblpersonal AS p ON p.PersonalId=m.PersonalId
    LEFT JOIN (
      SELECT PersonalId, MAX(PositionId) AS PositionId
      FROM tblpersonal_position
      GROUP BY PersonalId 
    ) AS pp ON pp.PersonalId = p.PersonalId
    LEFT JOIN tblposition AS po ON pp.PositionId=po.PositionId
    WHERE  m.DeleteStatus="N"  AND ${whereType} AND ManagerParent=${ParentId} AND ManagerRound=${RoundId}`;
  sqlPersonal = `SELECT s.PersonalId AS RefId,s.PersonalParent AS RefParent,ParentType,CONCAT(p.FirstName,' ',p.LastName) FullName ,'P' AS Type,s.ApproveLeave,
    p.PersonalId,p.FirstName,p.LastName,p.Email,p.PersonalCodeReal,p.Mobile,p.IDCard,p.BirthDate,,po.Position
    FROM tblstructure_operation_personal AS s  
    LEFT JOIN tblpersonal AS p ON p.PersonalId=s.PersonalId
    LEFT JOIN (
      SELECT PersonalId, MAX(PositionId) AS PositionId
      FROM tblpersonal_position
      GROUP BY PersonalId 
    ) AS pp ON pp.PersonalId = p.PersonalId
    LEFT JOIN tblposition AS po ON pp.PositionId=po.PositionId
    WHERE  s.DeleteStatus="N"  AND ${whereType} AND s.PersonalParent=${ParentId} AND s.PersonalRound=${RoundId}`;
  
  //console.log('->  ',isOperation, isPersonal);
  //console.log('-> sqlManager ',sqlManager);
  const listManager = sqlManager ? await sequelize.query(sqlManager , { type: QueryTypes.SELECT }) : null;
  const listOrg = sqlOrg ? await sequelize.query(sqlOrg , { type: QueryTypes.SELECT }) : null;
  let listPersonal = []
  if(isPersonal==1) listPersonal = sqlPersonal ? await sequelize.query(sqlPersonal , { type: QueryTypes.SELECT }) : null;
  var list = []
  if(listOrg) list = [...list , ...listOrg]
  if(listManager) list = [...list , ...listManager]
  if(isPersonal==1 && listPersonal) list = [...list , ...listPersonal]

  if(list.length>0){
    for await(const item of list ) {
      //console.log('-->> item',item);
      const child = await this.childsOrg(RoundId,item.RefId,item.ParentType,isOperation,isPersonal);
      //console.log('-->> child',child);
      let childs = [];
      if(child.length>0){
        for await(const item_child of child ) {
          if(item_child.OrgId && item_child.Type=='O' ) {
            const { count } = await this.listPersonalOrg({OrgId : item_child.OrgId, RoundId});
            item_child.count_personal = count && count[0] ? count[0].count : 0;
            childs.push(item_child)
          } else {
            item_child.count_personal = 0;
            childs.push(item_child)
          }
        }
        item.childs = child;
      }
      result.push(item);
    }
  }
  return result;
}

exports.listPersonalOrg = async (query) => {
  try{
    const currentRound = await this.getCurrentRound();
    //console.log(currentRound);
    var whereSearch = '';
    if(query.search && query.search) {
      whereSearch += ` AND ( FirstName LIKE '%${query.search}%' OR LastName LIKE '%${query.search}%' OR IDCard LIKE '%${query.search}%' ) `;
    }
    let sql = `FROM tblstructure_operation_personal op 
      LEFT JOIN tblpersonal p ON p.PersonalId=op.PersonalId 
      LEFT JOIN tblstructure_operation org ON op.PersonalParent=org.OrgId
      LEFT JOIN tblpersonal_position AS pp ON pp.PersonalId=p.PersonalId AND pp.EnableStatus='Y' AND pp.DeleteStatus='N'
      LEFT JOIN tblposition AS po ON po.PositionId=pp.PositionId
      WHERE op.EnableStatus="Y"  AND  op.DeleteStatus="N"  AND p.EnableStatus="Y"  AND  p.DeleteStatus="N"  AND op.isReal=1  AND op.ParentType='O' ${whereSearch}
      `;

    if(query.OrgId){
      sql += ` AND PersonalParent=${query.OrgId} `;
    } 
    if(query.PositionId){
      sql += ` AND pp.PositionId=${query.PositionId} `;
    } 
    if(query.RoundId){
      sql += ` AND PersonalRound=${query.RoundId} `;
    } else {
      sql += ` AND PersonalRound=${currentRound.RoundId} `;
    }
    sql += ` GROUP BY p.PersonalId `
    
    if(query.page && query.limit) {
      sql += ` LIMIT ${((query.page-1)*query.limit)} , ${query.limit}`;
    }
    let select = `SELECT p.PersonalId,p.FirstName,p.LastName,IDCard,BirthDate,Mobile,Email,
    OrgIdRef, OrgName,op.OrganizeCode,OrgShortName,address_long,p.PersonalCode,'P' AS Type,
    PersonalRound,p.Address,p.Address2,po.PositionId,po.Position,op.ApproveLeave `;
    console.log(select+sql);
    let results = await sequelize.query(select+sql , { type: QueryTypes.SELECT });
    let count = await sequelize.query('SELECT COUNT(*) as count '+ sql, { type: QueryTypes.SELECT });
    let list = await results.sort(dynamicSort("FirstName"));

    if(query.currentUser){
      const log = {
        Provider : 'API',
        Method : 'POST',
        UserId : query.currentUser.UserID || '',
        LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
        Token : query.currentUser.token || '',
        URL : 'personal/list_personal_org',
        Data: select+sql,
        UserName: query.currentUser.Username || ''
      };
      const insert = await insertSQL('provider_log', log, fields_log);
    }

    return { list , count }
  } catch(e) {
    console.log(e);
  }
};

exports.listPersonalOrgChild = async (query) => {
  try{
    let ParentId;
    let ParentType;
    if(query.OrgId) {
      const org = query.OrgId.split('_')
      ParentId = org[1]
      ParentType = org[0]
    }
    const currentRound = await this.getCurrentRound();
    //console.log(currentRound);
    var whereSearch = '';
    if(query.search && query.search) {
      whereSearch += ` AND ( FirstName LIKE '%${query.search}%' OR LastName LIKE '%${query.search}%' OR IDCard LIKE '%${query.search}%' ) `;
    }
    if(ParentType=='M'){
      let sql = `SELECT ManagerId,ManagerId AS RefId,ManagerParent AS RefParent,ParentType,
        ManagerName,'M' AS Type ,CONCAT(p.FirstName,' ',p.LastName) FullName,m.ApproveLeave,
        p.PersonalId,p.FirstName,p.LastName,p.Email,p.PersonalCodeReal,p.Mobile,p.IDCard,p.BirthDate,
        po.Position
        FROM tblstructure_manager AS m  
        LEFT JOIN tblpersonal AS p ON p.PersonalId=m.PersonalId
        LEFT JOIN (
          SELECT PersonalId, MAX(PositionId) AS PositionId
          FROM tblpersonal_position
          GROUP BY PersonalId 
        ) AS pp ON pp.PersonalId = p.PersonalId
        LEFT JOIN tblposition AS po ON pp.PositionId=po.PositionId
        WHERE  m.DeleteStatus="N"  ${whereSearch} AND ManagerId=${ParentId} AND ManagerRound=${query.RoundId}`;
        console.log(sql);
        let results = await sequelize.query(sql , { type: QueryTypes.SELECT });
        let list = await results.sort(dynamicSort("FirstName"));
        return list
    } else {
      let sql = `FROM tblstructure_operation_personal op 
        LEFT JOIN tblpersonal p ON p.PersonalId=op.PersonalId 
        LEFT JOIN tblstructure_operation org ON op.PersonalParent=org.OrgId
        LEFT JOIN tblpersonal_position AS pp ON pp.PersonalId=p.PersonalId AND pp.EnableStatus='Y' AND pp.DeleteStatus='N'
        LEFT JOIN tblposition AS po ON po.PositionId=pp.PositionId
        WHERE op.EnableStatus="Y"  AND  op.DeleteStatus="N"  AND p.EnableStatus="Y"  AND  p.DeleteStatus="N"  AND op.isReal=1  AND op.ParentType='O' ${whereSearch} `;

      if(ParentId){
        sql += ` AND op.PersonalParent=${ParentId} `;
        sql += ` AND op.ParentType='${ParentType}' `;
      } 
      if(query.PositionId){
        sql += ` AND pp.PositionId=${query.PositionId} `;
      } 
      if(query.RoundId){
        sql += ` AND PersonalRound=${query.RoundId} `;
      } else {
        sql += ` AND PersonalRound=${currentRound.RoundId} `;
      }
      sql += ` GROUP BY p.PersonalId `;
      
      if(query.page && query.limit) {
        sql += ` LIMIT ${((query.page-1)*query.limit)} , ${query.limit}`;
      }
      let select = `SELECT p.PersonalId,p.FirstName,p.LastName,IDCard,BirthDate,Mobile,Email,org.OrgId,OrgIdRef, OrgName,
      op.OrganizeCode,OrgShortName,address_long,p.PersonalCode,'P' AS Type,
      PersonalRound,p.Address,p.Address2,po.PositionId,po.Position,op.ApproveLeave `;
      console.log(select+sql);
      let results = await sequelize.query(select+sql , { type: QueryTypes.SELECT });
      ///let count = await sequelize.query('SELECT COUNT(*) as count '+ sql, { type: QueryTypes.SELECT });
      if(ParentType=='O'){
        const child = await this.childsOrg(query.RoundId,ParentId,ParentType,false,false);
        console.log('child',child);
        if(child && child.length>0){
          for await(const item_child of child ) {
            const person = await this.listPersonalOrgChild({...query,OrgId : item_child.ParentType+'_'+item_child.RefId, RoundId : query.RoundId});
            console.log('person',person);
            results = [...results,...person]
          }
          console.log('results',results);
          return results
        } 
      }
      let list = await results.sort(dynamicSort("FirstName"));
      return list
    }
    
  } catch(e) {
    console.log(e);
  }
};
exports.listPersonalOrgAll = async (query) => {
  const list =  await this.listPersonalOrgChild(query)
  return { list , count : list.length || 0 }
}

exports.getOrgByPersonalId = async (PersonalId) => {

  const currentRound = await this.getCurrentRound();
  
  let sql = `SELECT  org.OrgId, org.OrgIdRef, org.OrgRoundRef, org.OrgRoundCode, org.OrgYear,org.OrgRound,org.OrgParent,
    org.OrgName,org.OrgShortName,org.isOperation,org.isReal
    FROM tblstructure_operation_personal per
    LEFT JOIN tblstructure_operation org ON per.PersonalParent=org.OrgId
    WHERE  per.DeleteStatus="N" AND per.isReal=1 
    AND per.PersonalRound=${currentRound.RoundId} AND per.PersonalId=${PersonalId}`;
  console.log(sql);
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return list.length>0 ? list : {}
};

exports.getAbsence = async (query) => {
  let sql = `SELECT  AbsenceId,ab.AbsenceTypeId,ab.AbsenceTypeCode,AbsenceTopic,ApproveStatus,
    AbsenceStartDate,AbsenceStartPeriod,AbsenceEndDate,AbsenceEndPeriod,TotalDay,
    PersonalCode,PersonalId,type.AbsenceTypeName,type.AbsenceTypeShortName
    FROM tblabsence ab
    LEFT JOIN tblabsence_type type ON type.AbsenceTypeId=ab.AbsenceTypeId
    WHERE  ab.DeleteStatus="N" AND ab.EnableStatus="Y"  AND ab.PersonalID=${query.PersonalID}
    AND ('${moment(query.date).format('YYYY-MM-DD')}' BETWEEN ab.AbsenceStartDate AND ab.AbsenceEndDate)`;
  
  const detail = await sequelize.query(sql , { type: QueryTypes.SELECT });
  console.log(sql,detail);
  if(query.currentUser){
    const log = {
      Provider : 'API',
      Method : 'POST',
      UserId : query.currentUser.UserID || '',
      LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
      Token : query.currentUser.token || '',
      URL : 'personal/absence',
      Data: sql,
      UserName: query.currentUser.Username || ''
    };
    const insert = await insertSQL('provider_log', log, fields_log);
  }
  return  detail && detail.length ?  {isAbsence :true ,...detail[0]}  : {isAbsence : false}
};


function dynamicSort(property) {
  var sortOrder = 1;
  if(property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
  }
  return function (a,b) {
      /* next line works with strings and numbers, 
       * and you may want to customize it to your needs
       */
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
  }
}