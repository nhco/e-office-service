// config.js
require('dotenv').config();
//process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const { DB_HOST, DB_USER, DB_PWD,DB_NAME, DB_PORT, DB_SSL  } = process.env;

module.exports = {
  port : process.env.PORT,
  api: { prefix: '/api' },
  auth_url : process.env.AUTH_EXT_URL,
  auth_backend_url : process.env.AUTH_INT_URL,
  noti_url : process.env.NOTI_URL,
  jwtSecret: process.env.JWT_SECRET,
  jwtAlgorithm: process.env.JWT_ALGO,
  development : {
    "username": DB_USER,
    "password": DB_PWD,
    "database": DB_NAME,
    "host": DB_HOST,
    "port": DB_PORT,
    "dialect": "mysql",
    logging: false,
    ssl : DB_SSL === "TRUE" ? true : false,
    dialectOptions : {
      ssl : DB_SSL === "TRUE" ? true : false,
    },
  },
  "test": {
    "username": DB_USER,
    "password": DB_PWD,
    "database": DB_NAME,
    "host": DB_HOST,
    "port": DB_PORT,
    "dialect": "mysql",
    logging: false,
    ssl : DB_SSL === "TRUE" ? true : false,
    dialectOptions : {
      ssl : DB_SSL === "TRUE" ? true : false,
    },
  },
  "production": {
    "username": DB_USER,
    "password": DB_PWD,
    "database": DB_NAME,
    "host": DB_HOST,
    "port": DB_PORT,
    "dialect": "mysql",
    logging: false,
    ssl : DB_SSL === "TRUE" ? true : false,
    dialectOptions : {
      ssl : DB_SSL === "TRUE" ? true : false,
    }
  }
};