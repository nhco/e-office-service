const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require(__dirname + '/config');
const routes = require(__dirname + '/routes');
const db = require(__dirname + '/models');
const fs = require('fs');
const logging = require('./middlewares/logging');

async function startServer() {
    const app = express();
    db.sequelize.sync();
    app.use(
        cors({
            origin: '*',
        })
    );
    
    //app.options('*', cors());
    

    /*app.use((req, res, next) => {
        res.append('Access-Control-Allow-Origin', '*');
        res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        //res.append('Access-Control-Allow-Headers: *');
        res.append('Access-Control-Allow-Headers', 'Content-Type');
        next();
    });*/
    
    
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(bodyParser.raw());
    //authen
    // app.use(authorization.auth);
    app.use(logging.logResponseBody);

    console.log(fs.existsSync('public/files/pdf'));
    if (!fs.existsSync('public/files')) fs.mkdirSync('public/files', { recursive: true });
    if (!fs.existsSync('public/files/pdf')) fs.mkdirSync('public/files/pdf', { recursive: true });
    if (!fs.existsSync('public/files/signature')) fs.mkdirSync('public/files/signature', { recursive: true });
    if (!fs.existsSync('public/files/excel')) fs.mkdirSync('public/files/excel', { recursive: true });

    app.use('/', routes());

    app.get('/check_status', (req, res) => { res.status(200).send({ status : 'online' }) })

    app.listen(config.port, () => {
        console.log(`Server listening on port: ${config.port}`);
    }).on('error', (err) => {
        console.log(err);
        process.exit(1);
    });
}

startServer();