const nodemailer = require('nodemailer');
let transporter = nodemailer.createTransport({
      host: process.env.EMAIL_SMTP,
      port: process.env.EMAIL_PORT,
      auth: {
          user: process.env.EMAIL_USER,
          pass: process.env.EMAIL_PASS,
      }
})

const sendEmail = async ( params ) => {
  //message = { from, to, subject, text }
  let [ success, msg, data ] = [ true, '', null ];
  try {
    data = await transporter.sendMail(params);
    success = true;
  } catch (error) {
    success = false;
    data = error;
    msg = error.response;
  }
  return { success, msg, data };
};

module.exports = {
  sendEmail
}