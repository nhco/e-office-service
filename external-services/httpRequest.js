const axios = require('axios').default;
const https = require('https');

module.exports = config => {
  const httpsAgent = new https.Agent({
    rejectUnauthorized: false,
  });

  const $http = axios.create({ httpsAgent, ...config });

  return {
    get (uri, params={}, options={}) {
      return $http.get( uri, { ...options, params } );
    },
    post (uri, data, options={}) {
      return $http.post( uri, data, {...options} );
    },
    put (uri, data, options={}) {
      return $http.put( uri, data, {...options} );
    },
    delete (uri, params={}, options={}) {
      return $http.delete( uri, { ...options, params } );
    },
  }


};