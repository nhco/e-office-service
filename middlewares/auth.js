const jwt = require("jsonwebtoken");
const config = require('../config');
const axios = require('axios');

exports.getAuth = async (req, res, next) => {
  try {
    //next();
    const token = req.headers.authorization.split(' ')[1];
    //const token_decoded = jwt.verify(token, process.env.TOKEN_KEY);
    console.log(token);
    jwt.verify(token, process.env.TOKEN_KEY, (err, decoded) => {
      if (err) {
        console.log('err',err)
        return res.status(401).send({
          code : 101,
          isSuccess : false,
          message: "Unauthorized!"
        });
      }
      console.log('decoded',decoded);
      req.currentUser = decoded;//decoded.id;
      req.currentUser.token = token;
      next();
    });
  } catch (err){
    console.log('err',err)
    res.status(401).send({ 
      code : 101,
      isSuccess : false,
      message: 'Invalid token!' })
  }
};

/*const getTokenFromHeader = (req) => {
  if (
    (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Token')
    || (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer')
  ) {
    return req.headers.authorization.split(' ')[1];
  }
  return null;
};

const isAuth = jwt({
  secret: config.jwtSecret, // The _secret_ to sign the JWTs
  algorithms: ['HS256', config.jwtAlgorithm], // JWT Algorithm
  userProperty: 'token', // Use req.token to store the JWT
  getToken: getTokenFromHeader, // How to extract the JWT from the request
});*/

