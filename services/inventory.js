const { Op, QueryTypes } = require('sequelize');
const { sequelize } = require('../models/index')
const { insertSQL, updateSQL, deleteSQL } = require('../services/tool')
const moment = require('moment')
const db = require("../models");
const fields_log = [
  'Provider',
  'CategoryName',
  'Method',
  'UserId',
  'LogDate',
  'Token',
  'URL',
  'Data',
  'UserName',
];

exports.listFP001 = async (DocCode, currentUser) => {
  console.log(DocCode);
  let sql = `SELECT t1.*,
    t4.ProcureId,t4.ProcureCode,t4.CostId,t4.AmountItem,
    t6.ContractId,t6.ContractNo,t6.CostIdRef,t6.AmountItem,t1.SumCostRelate,
    t3.CostExtId,t2.PrjDetailId,t1.CostItemCode,t5.SumCost SumMatCost,t7.CostName,
    doc.ApproveDate,
    doc.ApproveBy 
  FROM tblintra_eform_formal_mat_cost  as t5 
  LEFT JOIN tblfinance_form_hold_cost as t1 ON t1.CostInt_ExtId=t5.CostInt_ExtId
  LEFT JOIN stock_doc_purchase_item as t4 ON t4.CostId=t5.CostId 
  LEFT JOIN stock_doc_con_item as t6 ON t6.CostIdRef=t5.CostId
  LEFT JOIN tblbudget_init_cost_item t7 ON t7.CostItemCode=t1.CostItemCode 
  LEFT JOIN tblbudget_project_activity as t2 ON t2.PrjActCode=t1.PrjActCode
  LEFT JOIN tblbudget_project_activity_cost_external as t3 ON t3.CostExtId=t1.CostInt_ExtId
	LEFT JOIN tblfinance_doccode AS doc ON doc.DocCode = t1.DocCode
  WHERE t1.DocCode='${DocCode}' 
  Group By t1.CostInt_ExtId Order By PrjActCode,DetailCost asc`;
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
  console.log('sql',sql);

  const list = await Promise.all(
    
    data.map( async item =>  {
      item.sumPay = 0;
      item.sumAmountItem = 0;
      item.Procure = [];
      if(item.ProcureId){
        const sqlProcure = `SELECT t2.ProcureId,t2.ProcureCode
            FROM stock_doc_purchase_item t2 
            LEFT JOIN stock_doc_purchase as t1  on t1.ProcureId = t2.ProcureId 
            WHERE t1.DocCode='${DocCode}'  AND t2.DeleteStatus = 'N' 
            AND t2.CostItemCode = '${item.CostItemCode}' AND t2.CostId = '${item.CostId}'
            AND t2.EnableStatus='Y' AND t2.DeleteStatus='N' 
            AND t1.EnableStatus='Y' AND t1.DeleteStatus='N' 
            GROUP BY t1.ProcureId`;

        const dataProcure = await sequelize.query(sqlProcure , { type: QueryTypes.SELECT });
        item.Procure = [...item.Procure,...dataProcure]
        for await (const procure of dataProcure){
          let sql = `SELECT SUM(TotalCost) AS total
            FROM tblfinance_form_pay 
            WHERE ProcureCode='${procure.ProcureCode}' AND PrjActCode='${item.PrjActCode}'` 

          const TotalCost = await sequelize.query(sql , { type: QueryTypes.SELECT });
          //console.log('TotalCost',TotalCost);
          item.sumPay += TotalCost.length ? Number(TotalCost[0].total) : 0
          
          let condition = ` FROM stock_doc_purchase_item t2 
              LEFT JOIN stock_doc_purchase as t1  on t1.ProcureId = t2.ProcureId 
              WHERE t1.ProcureId='${procure.ProcureId}' AND t1.DocCode='${DocCode}' AND t2.DeleteStatus = 'N' 
              AND t2.CostItemCode = '${item.CostItemCode}' AND t2.CostId = '${item.CostId}'
              AND t2.EnableStatus='Y' AND t2.DeleteStatus='N' 
              GROUP BY t1.ProcureId`;
          const sqlTax = `SELECT t1.Tax ${condition}`;
          const tax = await sequelize.query(sqlTax , { type: QueryTypes.SELECT });
          const taxValue = tax && tax.length ? Number(tax[0].Tax) : 0
          const sqlSum = `SELECT SUM(t2.PreTaxItem) total ${condition}`;
          const sum = await sequelize.query(sqlSum , { type: QueryTypes.SELECT });
          const sumValue = sum && sum.length ? Number(sum[0].total) : 0
          item.sumAmountItem += (sumValue *( taxValue/100))+sumValue;
        }
      }
      item.Contract = [];
      if(item.ContractId){
        const sqlContract = `SELECT t2.ContractId,t2.ContractNo
            FROM stock_doc_con_item t2 
            LEFT JOIN stock_doc_con_contract as t1  on t1.ContractId = t2.ContractId 
            WHERE  t1.DocCode='${DocCode}'  AND t2.DeleteStatus = 'N' 
            AND t2.CostItemCode = '${item.CostItemCode}' AND t2.CostIdRef = '${item.CostIdRef}'
            AND t2.EnableStatus='Y' AND t2.DeleteStatus='N' 
            AND t1.EnableStatus='Y' AND t1.DeleteStatus='N' 
            GROUP BY t1.ContractId`;
        const dataContract = await sequelize.query(sqlContract , { type: QueryTypes.SELECT });
        item.Contract = [...item.Contract,...dataContract]

        for await (const contract of dataContract){
          let sql = `SELECT SUM(TotalCost) AS total
            FROM tblfinance_form_pay 
            WHERE ContractNo='${contract.ContractNo}' AND PrjActCode='${item.PrjActCode}'` 

          const TotalCost = await sequelize.query(sql , { type: QueryTypes.SELECT });
          item.sumPay += TotalCost.length ? Number(TotalCost[0].total) : 0

          let sqlAmount = ` SELECT SUM(t2.AmountItem) AS total
              FROM stock_doc_con_item t2 
              LEFT JOIN stock_doc_con_contract as t1  on t1.ContractId = t2.ContractId 
              WHERE t1.ContractId='${contract.ContractId}' AND t1.DocCode='${DocCode}' AND t2.DeleteStatus = 'N' 
              AND t2.CostItemCode = '${item.CostItemCode}' AND t2.CostIdRef = '${item.CostIdRef}'
              AND t2.EnableStatus='Y' AND t2.DeleteStatus='N' 
              GROUP BY t1.ContractId`;
          const sum = await sequelize.query(sqlAmount , { type: QueryTypes.SELECT });
          item.sumAmountItem += sum && sum.length ? Number(sum[0].total) : 0
        }
      }
      if(item.SumPay>item.sumAmountItem){
        item.SumCostRelateTotal = Number(item.SumMatCost)-Number(item.SumPay);
      } else {
        item.SumCostRelateTotal = Number(item.SumMatCost)-Number(item.sumAmountItem);//-(item.SumCostRelate);
      }
      if(item.DocStatusId==8 || item.DocStatusId==15){
        item.SumCostRelateTotal = 0;
      } 
      item.SumChainRelate = Number(item.sumAmountItem-item.SumPay) > 0 ? Number(item.sumAmountItem)-Number(item.SumPay) : 0;
      item.SumChainOverPay = Number(item.SumPay)>0 && Number(item.SumPay)>Number(item.sumAmountItem) ? Number(item.SumPay)-Number(item.sumAmountItem) : 0;
      
      return item
    })
    
  )
  if(currentUser){
    const log = {
      Provider : 'API',
      Method : 'GET',
      UserId : currentUser.UserID || '',
      LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
      Token : currentUser.token || '',
      URL : 'inventory/fp001/list',
      Data: sql,
      UserName: currentUser.Username || ''
    };
    const insert = await insertSQL('provider_log', log, fields_log);
  }
  return list;
};
