const { Router } = require('express');
const { getAuth } = require('../../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const inventoryService = require('../../services/inventory');
const route = Router();

module.exports = (app) => {

    app.use('/erp/inventory', route);
    app.use(errors());

    route.get('/fp001/list', getAuth, async function (req, res) {
        console.log('inventory detail');
        const detail = await inventoryService.listFP001(req.query.DocCode, req.currentUser);
        res.status(200).json({code : 0, isSuccess : true , data : detail})
    })


};