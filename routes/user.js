const { Router } = require('express');
const middlewares = require('../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const userService = require('../services/user');
const route = Router();

module.exports = (app) => {

    app.use('/user', route);
    app.use(errors());

    route.get('/', function (req, res) {
        console.log('user');
        res.send('Hello World!')
    })

    route.post('/login', 
        celebrate({
            [Segments.BODY]: Joi.object({
                username: Joi.string().required(),
                password: Joi.string().required(),
            })
        }),
        async function (req, res) {
            console.log('user list');
            const list = await userService.login(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.post('/list', async function (req, res) {
        console.log('user list');
        const list = await userService.list(req.body);
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

     route.get('/detail/:UserID', async function (req, res) {
        console.log('user detail');
        const detail = await userService.detail(req.params.UserID);
        res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.post('/oauth2', 
        celebrate({
            [Segments.BODY]: Joi.object({
                code: Joi.string().required(),
                redirect_uri: Joi.string().required(),
                client_id: Joi.string().allow(null),
                client_secret: Joi.string().allow(null),
            })
        }),
        async function (req, res) {
            console.log('user list');
            const data = await userService.oauth2(req , res);
            console.log('#oauth2 data',data);
            res.status(200).json({data : data})
    })
    
    route.post('/create', async function (req, res) {
        console.log('user create');
        const created = await userService.create(req.body);
        res.status(200).json({code : 0, isSuccess : true , data : created})
    })

    route.post('/update', async function (req, res) {
        console.log('user update');
        const updated = await userService.update(req.body);
        res.status(200).json({code : 0, isSuccess : true , data : updated})
    })
};