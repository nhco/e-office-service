const { Op, QueryTypes } = require('sequelize');
const { sequelize } = require('../models/index')
const { insertSQL, updateSQL, deleteSQL } = require('../services/tool')
const db = require("../models");
const moment = require("moment");
const fields_log = [
  'Provider',
  'CategoryName',
  'Method',
  'UserId',
  'LogDate',
  'Token',
  'URL',
  'Data',
  'UserName',
];

exports.version_plan = async (query, currentUser) => {

  try{
    let sqlCurrent = `SELECT t1.SCTypeId, t1.ScreenLevel FROM  tblbudget_init_year_org as t1 WHERE t1.BgtYear='${query.BgtYear}'`
    const curProcess = await sequelize.query(sqlCurrent , { type: QueryTypes.SELECT });

    let sql = `SELECT t1.ScreenId, t1.ScreenLevel ,t1.ScreenName,t1.SCTypeId,t2.SCTypeName,t1.Allot 
      FROM  tblbudget_init_screen_item as t1 
      LEFT JOIN tblbudget_init_screen_type as t2 on t2.SCTypeId=t1.SCTypeId 
      WHERE t1.BgtYear='${query.BgtYear}' AND t1.EnableStatus='Y' AND t1.DeleteStatus<>'Y'
      ORDER BY t1.ScreenLevel ASC`;

    console.log(sql);
    let data = [];
    const list = await sequelize.query(sql , { type: QueryTypes.SELECT });
    for await(const item of list ){
      item.flag = 0;
      if(curProcess[0] && curProcess[0].SCTypeId == item.SCTypeId  && item.ScreenLevel == curProcess[0].ScreenLevel ){
        item.flag = 1; 
      }
      const TotalPrjExt = await getTotalPrjExt(query.BgtYear, item.SCTypeId, item.ScreenLevel)
      const TotalPrjInt = await getTotalPrjInt(query.BgtYear, item.SCTypeId, item.ScreenLevel)
      item.TotalPrj = TotalPrjExt+TotalPrjInt;
      item.TotalAllot = await getTotalAllot(query.BgtYear, item.SCTypeId, item.ScreenLevel);
      data.push(item)
      //$TotalPrj = $get->getTotalPrj($_REQUEST['BgtYear'],$_REQUEST["OrganizeCode"],0,0,0,0,$SCTypeId,$ScreenLevel);
      //$TotalAllot = $get->getTotalAllotPrj($_REQUEST['BgtYear'],$_REQUEST["OrganizeCode"],$ScreenLevel,$SCTypeId);
    }
    if(currentUser){
      const log = {
        Provider : 'API',
        Method : 'GET',
        UserId : currentUser.UserID || '',
        LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
        Token : query.token || '',
        URL : 'budget/version_plan',
        Data: sql,
        UserName: currentUser.Username || ''
      };
      const insert = await insertSQL('provider_log', log, fields_log);
    }
    return data;
  } catch (err) {
    console.log(err);
  }
};

exports.list_group = async (query, currentUser) => {
  let sql = `SELECT  * 
      FROM  tblbudget_init_plan_item_group AS t1 
      WHERE  t1.EnableStatus='Y' 
      order by t1.LPlanGroupId asc`;
  console.log(sql);
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });
  if(currentUser){
    const log = {
      Provider : 'API',
      Method : 'GET',
      UserId : currentUser.UserID || '',
      LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
      Token : currentUser.token || '',
      URL : 'budget/list_group',
      Data: sql,
      UserName: currentUser.Username || ''
    };
    const insert = await insertSQL('provider_log', log, fields_log);
  }
  return list;
};

exports.list_plan_main = async (query, currentUser) => {
  let sql = `SELECT  * 
      FROM  tblbudget_longterm AS t1 
      WHERE  t1.EnableStatus='Y' AND t1.DeleteStatus='N'
      order by t1.PLongYear ASC `;
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });
  let data = [];
  for await(let item of list ){
    let sql = `SELECT  * 
      FROM  tblbudget_longterm_plan AS t1 
			left join tblbudget_longterm as t2 on t2.PLongCode=t1.PlongCode
      WHERE t1.PLongCode=${item.PLongCode}
      ORDER by t1.LPlanCode ASC `;
    item.Plan = await sequelize.query(sql , { type: QueryTypes.SELECT });
    data.push(item)
  }
  if(currentUser){
    const log = {
      Provider : 'API',
      Method : 'GET',
      UserId : currentUser.UserID || '',
      LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
      Token : currentUser.token || '',
      URL : 'budget/list_plan_main',
      Data: sql,
      UserName: currentUser.Username || ''
    };
    const insert = await insertSQL('provider_log', log, fields_log);
  }
  return data;
};

exports.list_plan = async (query, currentUser) => {
  let sql = `SELECT  * 
      FROM  tblbudget_init_plan_item AS t1 
      WHERE t1.BgtYear='${query.BgtYear}' AND t1.PGroupId='12'
      AND t1.EnableStatus='Y' AND t1.DeleteStatus='N'
      order by t1.PItemCode asc`;
  console.log(sql);
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  let data = [];
  for await(const item of list ){
    data.push(item)
  }
  if(currentUser){
    const log = {
      Provider : 'API',
      Method : 'GET',
      UserId : currentUser.UserID || '',
      LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
      Token : currentUser.token || '',
      URL : 'budget/list_plan',
      Data: sql,
      UserName: currentUser.Username || ''
    };
    const insert = await insertSQL('provider_log', log, fields_log);
  }
  return data;
};

exports.list_cost = async (query, currentUser) => {
  try{
    //SOURCE
    let sqlSource = `SELECT *,SourceExName AS SourceName
      FROM tblbudget_init_source_external  
      WHERE DeleteStatus='N' AND EnableStatus='Y' 
      ORDER BY SourceExId ASC`;
    const listSource = await sequelize.query(sqlSource , { type: QueryTypes.SELECT });

    let data = [];
    for await(let source of listSource){
      source.SumCost = 0;

      //ACTIVEITY
      let sqlActivity = `SELECT t1.PrjActId,t1.PrjActCode,t1.PrjActName,
        t1.OrganizeCode,t1.StartDate,t1.EndDate,t1.PrjDetailId,PercentMass,t2.TypeActName ,
        t4.BgtYear,t4.PrjId,t4.PrjCode,t4.PrjName,t4.PItemCode, t5.PItemId,t5.PItemName
        FROM tblbudget_project_activity AS t1 
        LEFT JOIN tblbudget_init_type_activity t2 ON t2.TypeActCode=t1.TypeActCode
        LEFT JOIN tblbudget_project_detail t3 ON t3.PrjDetailId=t1.PrjDetailId
        LEFT JOIN tblbudget_project AS t4 ON t4.PrjId = t3.PrjId 
        LEFT JOIN tblbudget_init_plan_item t5 ON t5.PItemCode=t4.PItemCode AND t5.PGroupId=12
        WHERE t3.PrjId='${query.PrjId}' AND t3.SCTypeId='${query.SCTypeId}' AND t3.ScreenLevel='${query.ScreenLevel}'
        GROUP BY t1.PrjActId,t1.PrjActCode,t1.PrjActName,t1.OrganizeCode,t1.StartDate,t1.EndDate,t1.PrjDetailId,PercentMass ,t2.TypeActName
        ORDER BY t1.PrjActCode`;
        //console.log('sqlActivity',sqlActivity)
      const listActivity = await sequelize.query(sqlActivity , { type: QueryTypes.SELECT });
      source.Activity = []
      for await(let activity of listActivity ){
        const TotalPrjExt = await getTotalPrjExt(query.BgtYear, null, null, null, null, activity.PrjDetailId, activity.PrjActId,source.SourceExId);
        
        if(TotalPrjExt>0){
          activity.SumCost = TotalPrjExt;
          source.SumCost += Number(TotalPrjExt);

          //COST
          let sqlCost = `SELECT CostTypeId,CostTypeName,Ordering 
                FROM tblbudget_init_cost_type 
                WHERE EnableStatus='Y' AND DeleteStatus!='Y'
                order by Ordering`;
          const listCost = await sequelize.query(sqlCost , { type: QueryTypes.SELECT });
          activity.Cost = []
          for await(let cost of listCost ){

              const BGLevel1 = await getCostItem(cost.CostTypeId)
              cost.Level1 = []
              cost.Month = [];
              for await(let level1 of BGLevel1 ){
                  //if(TotalPrjExt>0){
                    level1.SumCost = 0;
                    if(level1.HasChild=='Y'){
                      level1.Level2 = []
                      level1.Month = [];
                      const BGLevel2 = await getCostItem(cost.CostTypeId,2, level1.CostItemCode)
                      for await(let level2 of BGLevel2 ){
                          level2.SumCost = 0;
                          if(level2.HasChild=='Y'){
                            level2.Level3 = []
                            level2.Month = [];
                            const BGLevel3 = await getCostItem(cost.CostTypeId,3, level2.CostItemCode)
                            for await(let level3 of BGLevel3 ){
                              const TotalPrjExt = await getTotalPrjExtWithMonth(query.BgtYear, null, null, null, null, activity.PrjDetailId, activity.PrjActId,source.SourceExId, level3.CostItemCode, cost.ParentCode, null, 3, cost.HasChild);
                                level3.SumCost = TotalPrjExt.SumCost;
                                level3.Month = TotalPrjExt.Month;
                                console.log('level3.Month',level3.Month)
                                level2.Month = sumBudgets(level2.Month,level3.Month)
                                level2.SumCost += TotalPrjExt.SumCost
                                level2.Level3.push(level3) 
                            }
                            level1.Month = sumBudgets(level1.Month,level2.Month)
                          } else {
                            const TotalPrjExt = await getTotalPrjExtWithMonth(query.BgtYear, null, null, null, null, activity.PrjDetailId, activity.PrjActId,source.SourceExId, level2.CostItemCode, cost.ParentCode, null, 2, cost.HasChild);
                            level2.SumCost = TotalPrjExt.SumCost;
                            level2.Month = TotalPrjExt.Month;
                            level1.Month = sumBudgets(level1.Month,level2.Month)
                          }
                          level1.Level2.push(level2) 
                          level1.SumCost += level2.SumCost
                          //cost.Month = sumBudgets(cost.Month,level1.Month)
                      }
                    } else {
                      const TotalPrjExt = await getTotalPrjExtWithMonth(query.BgtYear, null, null, null, null, activity.PrjDetailId, activity.PrjActId,source.SourceExId, level1.CostItemCode, cost.ParentCode, null, 1, cost.HasChild);
                      level1.SumCost = TotalPrjExt.SumCost;
                      level1.Month = TotalPrjExt.Month;
                      
                    }
                //}
                cost.Level1.push(level1) 
                cost.Month = sumBudgets(cost.Month,level1.Month)
            }
            activity.Cost.push(cost)
          }
          source.Activity.push(activity)
        }
      }
      if(source.Activity.length>0) data.push(source)
      // if(currentUser){
      //   const log = {
      //     Provider : 'API',
      //     Method : 'GET',
      //     UserId : currentUser.UserID || '',
      //     LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
      //     Token : query.token || '',
      //     URL : 'budget/list_cost',
      //     Data: sqlActivity,
      //     UserName: currentUser.Username || ''
      //   };
      //   const insert = await insertSQL('provider_log', log, fields_log);
      // }
    }
    if(currentUser){
      const log = {
        Provider : 'API',
        Method : 'GET',
        UserId : currentUser.UserID || '',
        LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
        Token : currentUser.token || '',
        URL : 'budget/list_cost',
        Data: sqlSource,
        UserName: currentUser.Username || ''
      };
      const insert = await insertSQL('provider_log', log, fields_log);
    }
    return data;
  } catch (err) {
    console.log(err)
    return err
  }
};

const getCostItem = async (CostTypeId = 0, LevelId = 1, ParentCode = 0, HasChild = 0, CostItemCode = 0) =>{
  let where = []
  where.push(`EnableStatus='Y'`)
  where.push(`DeleteStatus!='Y'`)
  where.push(`CostTypeId='${CostTypeId}'`)
  where.push(`LevelId='${LevelId}'`)
  if (ParentCode) {
    where.push(`ParentCode='${ParentCode}'`)
  }
  let sqlCost = `SELECT CostItemId, CostItemCode, CostName, LevelId, ParentCode, HasChild, CostTypeId 
      FROM tblbudget_init_cost_item 
      WHERE ${where.join(' AND ')}`;

   return await sequelize.query(sqlCost , { type: QueryTypes.SELECT });
}

const getTotalAllot = async(BgtYear,SCTypeId,ScreenLevel,PrjDetailId) => {
  let where = []
  where.push(`t1.BgtYear='${BgtYear}'`)
  where.push(`t1.SCTypeId='${SCTypeId}'`)
  where.push(`t1.ScreenLevel='${ScreenLevel}'`)
  if(PrjDetailId )where.push(`t1.PrjDetailId='${PrjDetailId}'`);
  const sqlTotalInt = `SELECT sum(BGInternal) total
    FROM tblbudget_allot AS t1
    WHERE ${where.join(' AND ')}`;
  const BGTotal = await sequelize.query(sqlTotalInt , { type: QueryTypes.SELECT });
  const sqlAllot = `SELECT sum(BGExternal) total
    FROM tblbudget_allot_external AS t1
    WHERE ${where.join(' AND ')}`;
  const BGAllot = await sequelize.query(sqlAllot , { type: QueryTypes.SELECT });
  return  Number(BGTotal[0].total || 0)+Number(BGAllot[0].BGAllot || 0)
}

const getTotalPrjExt = async(BgtYear,SCTypeId,ScreenLevel,PItemCode,PrjId,PrjDetailId,PrjActId,SourceExId, CostItemCode, ParentCode, CostTypeId, LevelId, HasChild) => {
  let where = []
  if(BgtYear) where.push(`t1.BgtYear='${BgtYear}'`)
  if(SCTypeId) where.push(`t2.SCTypeId='${SCTypeId}'`)
  if(ScreenLevel) where.push(`t2.ScreenLevel='${ScreenLevel}'`);
  if(PItemCode )where.push(`t1.PItemCode='${PItemCode}'`);
  if(PrjId )where.push(`t1.PrjId='${PrjId}'`);
  if(PrjDetailId )where.push(`t2.PrjDetailId='${PrjDetailId}'`);
  if(PrjActId )where.push(`t3.PrjActId='${PrjActId}'`);
  if(SourceExId )where.push(`t4.SourceExId='${SourceExId}'`);
  if(LevelId==2 && HasChild == "Y"){
    const sqlCostItemCode = `SELECT CostItemCode 
      FROM tblbudget_init_cost_item 
      WHERE ParentCode='${CostItemCode}'
      OR ParentCode IN ( 
        SELECT CostItemCode 
        FROM tblbudget_init_cost_item 
        ParentCode='${CostItemCode}'
      ) `;
    const CostItem = await sequelize.query(sqlCostItemCode , { type: QueryTypes.SELECT });
    CostItemCode = CostItem[0] ? CostItem[0].CostItemCode : null
  }
  if (CostItemCode) {
    where.push(`t4.CostItemCode='${CostItemCode}'`);
  }
  if (CostTypeId) {
    where.push(`t4.CostTypeId='${CostTypeId}'`);
  }
  const sqlExt4 = `SELECT  sum(t4.SumCost)as total ,t4.CostExtId
      FROM 	tblbudget_project AS t1 
      Inner Join tblbudget_project_detail AS t2 ON t2.PrjId = t1.PrjId 
      Inner Join tblbudget_project_activity AS t3 ON t3.PrjDetailId = t2.PrjDetailId 
      Inner Join tblbudget_project_activity_cost_external AS t4 ON t4.PrjActId = t3.PrjActId 
      Inner Join tblbudget_init_plan_item AS t5 ON t1.PItemCode = t5.PItemCode 
      Inner Join tblbudget_init_cost_item AS t6 ON t4.CostItemCode = t6.CostItemCode 
      Inner Join tblbudget_init_cost_type AS t7 ON t6.CostTypeId = t7.CostTypeId 
      Inner Join tblbudget_init_source_external AS t8 ON t8.SourceExId = t4.SourceExId 
      WHERE ${where.join(' AND ')}
      GROUP BY t4.CostExtId`;
  //console.log('sqlExt4',sqlExt4);
  const BGExt4 = await sequelize.query(sqlExt4 , { type: QueryTypes.SELECT });

  return Number(BGExt4.length ? BGExt4[0].total : 0)
}


const getTotalPrjExtWithMonth = async(BgtYear,SCTypeId,ScreenLevel,PItemCode,PrjId,PrjDetailId,PrjActId,SourceExId, CostItemCode, ParentCode, CostTypeId, LevelId, HasChild) => {
  let where = []
  if(BgtYear) where.push(`t1.BgtYear='${BgtYear}'`)
  if(SCTypeId) where.push(`t2.SCTypeId='${SCTypeId}'`)
  if(ScreenLevel) where.push(`t2.ScreenLevel='${ScreenLevel}'`);
  if(PItemCode )where.push(`t1.PItemCode='${PItemCode}'`);
  if(PrjId )where.push(`t1.PrjId='${PrjId}'`);
  if(PrjDetailId )where.push(`t2.PrjDetailId='${PrjDetailId}'`);
  if(PrjActId )where.push(`t3.PrjActId='${PrjActId}'`);
  if(SourceExId )where.push(`t4.SourceExId='${SourceExId}'`);
  if(LevelId==2 && HasChild == "Y"){
    const sqlCostItemCode = `SELECT CostItemCode 
      FROM tblbudget_init_cost_item 
      WHERE ParentCode='${CostItemCode}'
      OR ParentCode IN ( 
        SELECT CostItemCode 
        FROM tblbudget_init_cost_item 
        ParentCode='${CostItemCode}'
      ) `;
    const CostItem = await sequelize.query(sqlCostItemCode , { type: QueryTypes.SELECT });
    CostItemCode = CostItem[0] ? CostItem[0].CostItemCode : null
  }
  if (CostItemCode) {
    where.push(`t4.CostItemCode='${CostItemCode}'`);
  }
  if (CostTypeId) {
    where.push(`t4.CostTypeId='${CostTypeId}'`);
  }
  const sqlExt4 = `SELECT  sum(t4.SumCost)as total ,t4.CostExtId
      FROM 	tblbudget_project AS t1 
      Inner Join tblbudget_project_detail AS t2 ON t2.PrjId = t1.PrjId 
      Inner Join tblbudget_project_activity AS t3 ON t3.PrjDetailId = t2.PrjDetailId 
      Inner Join tblbudget_project_activity_cost_external AS t4 ON t4.PrjActId = t3.PrjActId 
      Inner Join tblbudget_init_plan_item AS t5 ON t1.PItemCode = t5.PItemCode 
      Inner Join tblbudget_init_cost_item AS t6 ON t4.CostItemCode = t6.CostItemCode 
      Inner Join tblbudget_init_cost_type AS t7 ON t6.CostTypeId = t7.CostTypeId 
      Inner Join tblbudget_init_source_external AS t8 ON t8.SourceExId = t4.SourceExId 
      WHERE ${where.join(' AND ')}
      GROUP BY t4.CostExtId`;
  //console.log('sqlExt4',sqlExt4);
  const BGExt4 = await sequelize.query(sqlExt4 , { type: QueryTypes.SELECT });
  
  const sqlMonth = `SELECT  t4.CostExtId
    FROM 	tblbudget_project AS t1 
    Inner Join tblbudget_project_detail AS t2 ON t2.PrjId = t1.PrjId 
    Inner Join tblbudget_project_activity AS t3 ON t3.PrjDetailId = t2.PrjDetailId 
    Inner Join tblbudget_project_activity_cost_external AS t4 ON t4.PrjActId = t3.PrjActId 
    Inner Join tblbudget_init_plan_item AS t5 ON t1.PItemCode = t5.PItemCode 
    Inner Join tblbudget_init_cost_item AS t6 ON t4.CostItemCode = t6.CostItemCode 
    Inner Join tblbudget_init_cost_type AS t7 ON t6.CostTypeId = t7.CostTypeId 
    Inner Join tblbudget_init_source_external AS t8 ON t8.SourceExId = t4.SourceExId 
    WHERE ${where.join(' AND ')}`;
  //console.log('sqlMonth',sqlMonth);
  const BGMonth = await sequelize.query(sqlMonth , { type: QueryTypes.SELECT });
  //console.log('BGMonth',BGMonth);
  var Months = []
  if(BGMonth){
    for await(const month of BGMonth){
      const sqlExt5 = `SELECT  MonthNo, Budget
          FROM 	tblbudget_project_activity_cost_external_month AS t1 
          WHERE CostExtId=${month.CostExtId}`;
          //console.log('sqlExt5',sqlExt5);
      const BGExt5 = await sequelize.query(sqlExt5 , { type: QueryTypes.SELECT });
      //console.log('BGExt5',BGExt5);
      Months = sumBudgets(Months, BGExt5)
    }
  }

  return { SumCost : Number(BGExt4.length ? BGExt4[0].total : 0) , Month : Months}
}


const getTotalPrjInt = async(BgtYear,SCTypeId,ScreenLevel,PItemCode,PrjId,PrjDetailId,PrjActId,SourceExId, CostItemCode, ParentCode, CostTypeId, LevelId, HasChild) => {
  let where = []
  if(BgtYear) where.push(`t1.BgtYear='${BgtYear}'`)
  if(SCTypeId) where.push(`t2.SCTypeId='${SCTypeId}'`)
  if(ScreenLevel) where.push(`t2.ScreenLevel='${ScreenLevel}'`);
  if(PItemCode )where.push(`t1.PItemCode='${PItemCode}'`);
  if(PrjId )where.push(`t1.PrjId='${PrjId}'`);
  if(PrjDetailId )where.push(`t2.PrjDetailId='${PrjDetailId}'`);
  if(PrjActId )where.push(`t3.PrjActId='${PrjActId}'`);

  const sqlInt4 =`SELECT sum(t4.SumCost)as total 
				FROM 	tblbudget_project AS t1 
				Inner Join tblbudget_project_detail AS t2 ON t2.PrjId = t1.PrjId 
				Inner Join tblbudget_project_activity AS t3 ON t3.PrjDetailId = t2.PrjDetailId 
				Inner Join tblbudget_project_activity_cost_internal AS t4 ON t4.PrjActId = t3.PrjActId 
				Inner Join tblbudget_init_plan_item AS t5 ON t1.PItemCode = t5.PItemCode 
				Inner Join tblbudget_init_cost_item AS t6 ON t4.CostItemCode = t6.CostItemCode 
				Inner Join tblbudget_init_cost_type AS t7 ON t6.CostTypeId = t7.CostTypeId 
				WHERE ${where.join(' AND ')}`;
  //console.log('sqlInt4',sqlInt4);
  const BGInt4 = await sequelize.query(sqlInt4 , { type: QueryTypes.SELECT });

  return Number(BGInt4[0].total || 0)
}

const get_organizeFullName = async (BgtYear, PrjId) => {
  let where = [];
  where.push("t2.EnableStatus='Y'");
  where.push("t2.DeleteStatus='N'");
  where.push(`t1.PrjId='${PrjId}'`);

  let sql = `SELECT t2.OrgName, t2.OrgShortName,t2.OrganizeCode,t2.OrgId
  FROM tblbudget_project t1
  INNER JOIN tblstructure_operation t2 ON  t1.OrgId = t2.OrgId
  
  WHERE ${where.join(" AND ")}`;
  const org = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return org && org[0] ? org[0] : null
};

const getTaskPerson = async (BgtYear, PrjId) => {
  let where = [];
  where.push(`PP.PrjId='${PrjId}'`);
  let sql = `SELECT P.PersonalCode,
					CONCAT(PR.PrefixName,
					P.FirstName,' ',
					P.LastName) as Name,
					PP.ResultStatus,
					PP.PersonId
					FROM
					tblbudget_project_person AS PP
					Inner Join tblpersonal AS P ON PP.PersonalCode = P.PersonalCode
					Inner Join tblpersonal_prefix AS PR ON PR.PrefixId = P.PrefixId 
  WHERE ${where.join(" AND ")}`;
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.list_project = async (query, currentUser) => {
  let sql = `SELECT 
      t1.StatusId, 
      t1.PrjDetailId, 
      t1.StartDate,
      t1.EndDate,
      t2.PrjId, 
      t2.PrjCode, 
      t2.PrjName, 
      t2.PItemCode, 
      t3.StatusName, 
      t3.TextColor, 
      t3.Icon ,
      t2.PrjBudget,
      t2.PrjFeature,
      t2.PrjMethods,
      t2.PrjSpecial,
      t2.OldPrjId,
      t4.PrjCode AS OldPrjCode,
      t4.PrjName AS OldPrjName
      FROM 
      tblbudget_project_detail AS t1 
      Inner Join tblbudget_project AS t2 ON t1.PrjId = t2.PrjId 
      inner Join tblbudget_init_status as t3 on t3.StatusId = t1.StatusId 
      Inner Join tblbudget_project AS t4 ON t2.OldPrjId = t4.PrjId 
      WHERE t2.BgtYear='${query.BgtYear}' AND t1.SCTypeId='${query.SCTypeId}' AND t1.ScreenLevel='${query.ScreenLevel}'
      order by t1.PrjDetailId desc`;
  console.log(sql);
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  let data = [];
  for await(const item of list ){
    const TotalPrjExt = await getTotalPrjExt(query.BgtYear, query.SCTypeId, query.ScreenLevel, item.PItemCode,item.PrjId,item.PrjDetailId);
    const TotalPrjInt = await getTotalPrjInt(query.BgtYear, query.SCTypeId, query.ScreenLevel, item.PItemCode,item.PrjId,item.PrjDetailId);
    item.TotalPrj = TotalPrjExt+TotalPrjInt
    item.TotalAllot = await getTotalAllot(query.BgtYear, query.SCTypeId, query.ScreenLevel,item.PrjDetailId);
    item.Organization = await get_organizeFullName(query.BgtYear,item.PrjId);
    item.TaskPerson = await getTaskPerson(query.BgtYear,item.PrjId);
    data.push(item)
  }
  if(currentUser){
    const log = {
      Provider : 'API',
      Method : 'GET',
      UserId : currentUser.UserID || '',
      LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
      Token : query.token || '',
      URL : 'budget/list_project',
      Data: sql,
      UserName: currentUser.Username || ''
    };
    const insert = await insertSQL('provider_log', log, fields_log);
  }
  return data;
};

exports.list_activity = async (query, currentUser) => {
  try{
    let sql = `SELECT t1.*,t2.TypeActName
        FROM tblbudget_project_activity AS t1 
        LEFT JOIN tblbudget_init_type_activity t2 ON t2.TypeActCode=t1.TypeActCode
        WHERE t1.PrjDetailId='${query.PrjDetailId}'  
        ORDER BY t1.PrjActCode`;
    //console.log(sql);
    const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

    let data = [];
    for await(const item of list ){
      const TotalPrjInt = await getTotalPrjInt(query.BgtYear, null, null, null, null, item.PrjDetailId, item.PrjActId);
      const TotalPrjExt = await getTotalPrjExt(query.BgtYear, null, null, null, null, item.PrjDetailId, item.PrjActId);
      item.SumCost = TotalPrjInt+TotalPrjExt;
      data.push(item)
    }
    if(currentUser){
      const log = {
        Provider : 'API',
        Method : 'GET',
        UserId : currentUser.UserID || '',
        LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
        Token : currentUser.token || '',
        URL : 'budget/list_activity',
        Data: sql,
        UserName: currentUser.Username || ''
      };
      const insert = await insertSQL('provider_log', log, fields_log);
    }
    return data;
  } catch (err) {
    console.log(err);
  }
};

exports.list_policy = async (query) => {
  let sql = `SELECT *
    FROM tblbudget_init_plan_item AS pItem 
    LEFT JOIN tblbudget_init_plan_group AS pGroup ON pGroup.PGroupId=pItem.PGroupId 
    WHERE pItem.DeleteStatus='N' AND pItem.EnableStatus='Y' 
      AND pItem.PGroupId=${query.PGGroupId}
      AND pItem.BgtYear='${query.BgtYear}'
    ORDER BY PItemId ASC`;
  console.log(sql);
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  return list;
};

exports.detail_policy = async (query) => {
  let sql = `SELECT *
    FROM tblbudget_init_plan_item AS pItem 
    LEFT JOIN tblbudget_init_plan_group AS pGroup ON pGroup.PGroupId=pItem.PGroupId 
    WHERE pItem.DeleteStatus='N' AND pItem.EnableStatus='Y' 
      AND pItem.PItemCode='${query.PItemCode}'
      AND pItem.BgtYear='${query.BgtYear}'
    ORDER BY PItemId ASC`;
  console.log(sql);
  const detail = await sequelize.query(sql , { type: QueryTypes.SELECT });

  if(detail==null || detail[0]==null) {
    return { error_msg : 'Not found data'}
  }
  //PurposeItem
  let sqlPurposeItem = `SELECT * FROM tblbudget_init_plan_item_purpose 
    WHERE PItemCode='${query.PItemCode}' ORDER BY PurposeCode ASC`;
  console.log(sqlPurposeItem);
  const listPurposeItem = await sequelize.query(sqlPurposeItem , { type: QueryTypes.SELECT });

  detail[0].purpose = listPurposeItem;
  return detail;
};

exports.group_policy = async (query) => {
  let sqlGroupItem = `SELECT *
  FROM tblbudget_init_plan_item AS pItem 
  WHERE pItem.DeleteStatus='N' AND pItem.EnableStatus='Y' 
    AND pItem.PGroupId='${query.PGroupId}'
    AND pItem.BgtYear='${query.BgtYear}'
  ORDER BY PItemId ASC`;
  console.log(sqlGroupItem);
  const listGroupItem = await sequelize.query(sqlGroupItem , { type: QueryTypes.SELECT });

  return listGroupItem;
};

exports.getItemPurposes = async query => {
  try {
    const {purposeCodes} = query;
    if (!purposeCodes) return [];
    if (!purposeCodes.length) return [];

    const result = await sequelize.query(
      `
      SELECT *
      FROM tblbudget_init_plan_item_purpose
      WHERE PurposeCode IN (${purposeCodes.map( code => `'${code}'`).join(',')})
      ORDER BY PurposeCode ASC`,
      {
        type: QueryTypes.SELECT,
        logging: console.log
      }
    )
    return result;
  } catch (error) {
    console.error(error);
    throw error;
  }
}
exports.getListPolicyByPItemCodes = async query => {
  try {
    const {PItemCodes} = query;
    if (!PItemCodes) return [];
    if (!PItemCodes.length) return [];

    const result = await sequelize.query(
      `
      SELECT *
      FROM tblbudget_init_plan_item
      WHERE 
        DeleteStatus='N' AND 
        PItemCode IN (${PItemCodes.map( code => `'${code}'`).join(',')})
      ORDER BY PItemId ASC`,
      {
        type: QueryTypes.SELECT,
        logging: console.log
      }
    )
    return result;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

async function getCostTypes() {
  return await sequelize.query(
    `SELECT CostTypeId, CostTypeName, Ordering FROM tblbudget_init_cost_type WHERE EnableStatus='Y' AND DeleteStatus='N'`,
    {
      type: QueryTypes.SELECT,
      logging: console.log,
    }
  );
}

async function getCostItemRecordSet(CostTypeId, LevelId = 1, ParentCode = null) {
  var where = [];
  where.push("DeleteStatus='N'")
  where.push(`CostTypeId='${CostTypeId}'`);
  where.push(`LevelId='${LevelId}'`);
  if(ParentCode){
    where.push(`ParentCode='${ParentCode}'`);
  }
  return await sequelize.query(`SELECT CostItemId, CostItemCode, CostName, LevelId, ParentCode, HasChild, CostTypeId 
     FROM tblbudget_init_cost_item WHERE ${where.join(' AND ')}`,
     {
       type: QueryTypes.SELECT,
       //logging: console.log
     }
   )
}

async function populateLevel3Items(costTypeId, level2Items) {
  if (!Array.isArray(level2Items)) {
    return ;
  }

  return await Promise.all(level2Items.map(async (level2) => {
    const BGLevel3 = await getCostItemRecordSet(costTypeId, 3, level2.CostItemCode);
    if(Array.isArray(BGLevel3) && BGLevel3.length) level2.items = BGLevel3 ;
    return level2;
  }));
}

async function populateLevel2Items(costTypeId, level1Items) {
  if (!Array.isArray(level1Items)) {
    return ;
  }

  return await Promise.all(level1Items.map(async (level1) => {
    const BGLevel2 = await getCostItemRecordSet(costTypeId, 2, level1.CostItemCode);
    if(BGLevel2) level1.items = await populateLevel3Items(costTypeId, BGLevel2);
    return level1;
  }));
}

exports.getListPay = async (query, currentUser) => {
  try {
    const result = [];
    const costTypes = await getCostTypes();

    for (const costType of costTypes) {
      const BGLevel1 = await getCostItemRecordSet(costType.CostTypeId);
      costType.items = await populateLevel2Items(costType.CostTypeId, BGLevel1);
      result.push(costType);
    }
    if(currentUser){
      const log = {
        Provider : 'API',
        Method : 'GET',
        UserId : currentUser.UserID || '',
        LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
        Token : query.token || '',
        URL : 'budget/list_pay',
        Data: '',
        UserName: currentUser.Username || ''
      };
      const insert = await insertSQL('provider_log', log, fields_log);
    }
    return result;
  } catch (error) {
    console.error(error);
    throw error;
  }
};


const sumBudgets = (arr1, arr2) => {
  const budgetMap = new Map();
  if(!arr1.length) {
    arr1 = [
      { MonthNo: 12, Budget: '0.00' },
      { MonthNo: 11, Budget: '0.00' },
      { MonthNo: 10, Budget: '0.00' },
      { MonthNo: 9, Budget: '0.00' },
      { MonthNo: 8, Budget: '0.00' },
      { MonthNo: 7, Budget: '0.00' },
      { MonthNo: 6, Budget: '0.00' },
      { MonthNo: 5, Budget: '0.00' },
      { MonthNo: 4, Budget: '0.00' },
      { MonthNo: 3, Budget: '0.00' },
      { MonthNo: 2, Budget: '0.00' },
      { MonthNo: 1, Budget: '0.00' }]
  }
  if(!arr2.length) {
    arr2 = [
      { MonthNo: 12, Budget: '0.00' },
      { MonthNo: 11, Budget: '0.00' },
      { MonthNo: 10, Budget: '0.00' },
      { MonthNo: 9, Budget: '0.00' },
      { MonthNo: 8, Budget: '0.00' },
      { MonthNo: 7, Budget: '0.00' },
      { MonthNo: 6, Budget: '0.00' },
      { MonthNo: 5, Budget: '0.00' },
      { MonthNo: 4, Budget: '0.00' },
      { MonthNo: 3, Budget: '0.00' },
      { MonthNo: 2, Budget: '0.00' },
      { MonthNo: 1, Budget: '0.00' }]
  }

  arr1.forEach(({ MonthNo, Budget }) => {
    budgetMap.set(MonthNo, parseFloat(Budget));
  });

  arr2.forEach(({ MonthNo, Budget }) => {
    if (budgetMap.has(MonthNo)) {
      budgetMap.set(MonthNo, budgetMap.get(MonthNo) + parseFloat(Budget));
    } else {
      budgetMap.set(MonthNo, parseFloat(Budget));
    }
  });

  const result = [];
  budgetMap.forEach((Budget, MonthNo) => {
    result.push({ MonthNo, Budget: Budget.toFixed(2) });
  });

  // Sort the result by MonthNo
  result.sort((a, b) => a.MonthNo - b.MonthNo);

  return result;
};