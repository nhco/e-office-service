'use strict';
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  const address_types = sequelize.define("address_type", {
    name: DataTypes.STRING,
    descriptoin: DataTypes.STRING,
    active: DataTypes.BOOLEAN,
  });

  return address_types;
};