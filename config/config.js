require('dotenv').config();

const { DB_HOST, DB_USER, DB_PWD,DB_NAME, DB_PORT, DB_SSL } = process.env;

module.exports = {
  "development" : {
    "username": DB_USER,
    "password": DB_PWD,
    "database": DB_NAME,
    "host": DB_HOST,
    "port": DB_PORT,
    "dialect": "mysql",
    logging: false,
    pool: {
      max: 100,
      min: 0,
      idle: 30000,
      acquire: 60000
    },
    ssl : DB_SSL === "TRUE" ? true : false,
    dialectOptions : {
      ssl : DB_SSL === "TRUE" ? true : false,
    }
  },
  "test": {
    "username": DB_USER,
    "password": DB_PWD,
    "database": DB_NAME,
    "host": DB_HOST,
    "port": DB_PORT,
    "dialect": "mysql",
    logging: false,
    pool: {
      max: 100,
      min: 0,
      idle: 30000,
      acquire: 60000
    },
    ssl : DB_SSL === "TRUE" ? true : false,
    dialectOptions : {
      ssl : DB_SSL === "TRUE" ? true : false,
    }
  },
  "production": {
    "username": DB_USER,
    "password": DB_PWD,
    "database": DB_NAME,
    "host": DB_HOST,
    "port": DB_PORT,
    "dialect": "mysql",
    "logging": false,
    "pool": {
      max: 100,
      min: 0,
      idle: 30000,
      acquire: 60000
    },
    ssl : DB_SSL === "TRUE" ? true : false,
    dialectOptions : {
      ssl : DB_SSL === "TRUE" ? true : false,
    }
    /*"dialectOptions": {
      ssl: {
        key: cKey,
        cert: cCert,
        ca: cCA
      }
    }*/
  }
};