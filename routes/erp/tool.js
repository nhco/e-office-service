const { Router } = require('express');
const { getAuth } = require('../../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const toolService = require('../../services/tool');
const route = Router();

module.exports = (app) => {

    app.use('/erp/tool', route);
    app.use(errors());

    route.post('/list', getAuth,
        celebrate({
            [Segments.BODY]: Joi.object({
                page: Joi.number().allow(null),
                limit: Joi.number().allow(null),
                search: Joi.string().allow(null,""),
                sort: Joi.string().allow(null,""),
                order: Joi.string().allow(null,""),
            })
        }),
        async function (req, res) {
            console.log('tool list');
            const list = await toolService.list(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : list.list , total : list.count[0].count })
    })

     route.get('/detail/:CategoryId', getAuth, async function (req, res) {
        console.log('tool detail');
        const detail = await toolService.detail(req.params.CategoryId);
        res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.post('/sync', getAuth, async function (req, res) {
        console.log('tool detail');
        const detail = await toolService.sync(req.body);
        res.status(200).json({code : 0, isSuccess : true , data : detail})
    })
    
};