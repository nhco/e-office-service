const { Router } = require('express');
const middlewares = require('../../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const userService = require('../../services/user');
const route = Router();

module.exports = (app) => {

    app.use('/erp/user', route);
    //app.use(errors());

    route.post('/login', 
        celebrate({
            [Segments.BODY]: Joi.object({
                username: Joi.string().required(),
                password: Joi.string().required(),
            }),
            // [Segments.HEADERS]: Joi.object({
            //     customer_key: Joi.string().required(),
            //     secret_key: Joi.string().required(),
            // }) 
        }),
        async function (req, res) {
            try{
                console.log('user list');
                console.log('req.headers',req.headers);
                const list = await userService.loginProvider(req.body,req.headers);
                res.status(200).json({code : 0, isSuccess : true , data : list})
            } catch (err) {
                console.log('err',err)
                res.status(400).json({code : 0, isSuccess : false , error : err})
            }
    })

    route.post('/list', async function (req, res) {
        console.log('user list');
        const list = await userService.list(req.body);
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })


     route.get('/detail/:UserID', async function (req, res) {
        console.log('user detail');
        const detail = await userService.detail(req.params.UserID);
        res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.post('/oauth2', 
        celebrate({
            [Segments.BODY]: Joi.object({
                code: Joi.string().required(),
                redirect_uri: Joi.string().required(),
                client_id: Joi.string().allow(null),
                client_secret: Joi.string().allow(null),
            })
        }),
        async function (req, res) {
            console.log('user list');
            const data = await userService.oauth2(req , res);
            console.log('#oauth2 data',data);
            res.status(200).json({data : data})
    })
};