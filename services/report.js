const { Op, QueryTypes } = require('sequelize');
const { sequelize } = require('../models/index')
const db = require("../models");

exports.list_source = async (query) => {
  let sql = `SELECT *,SourceExName AS SourceName
    FROM tblbudget_init_source_external  
    WHERE DeleteStatus='N' AND EnableStatus='Y' 
    ORDER BY SourceExId ASC`;
  //console.log(sql);
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  return list;
};

exports.list_groupPlan = async (query) => {
  let sql = `SELECT *
    FROM tblbudget_init_plan_item_group  
    WHERE  EnableStatus='Y' 
    ORDER BY Ordering`;
  //console.log(sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.list_planItem = async (query, LPlanGroupId) => {
  let where = [];
  where.push("t1.PGroupId='12'");
  where.push("t1.EnableStatus='Y'");
  where.push("t1.DeleteStatus<>'Y'");
  if(LPlanGroupId)where.push(`t1.LPlanGroupId = ${LPlanGroupId}`);
  where.push("t2.EnableStatus='Y'");
  where.push("t2.DeleteStatus<>'Y'");
  where.push(`t1.BgtYear='${query.BgtYear}'`);
  let sql = `SELECT distinct(t1.PItemCode), t1.LPlanCode, t1.PItemName ,t1.BgtYear,t1.PGroupId, t1.PlanType
    FROM tblbudget_init_plan_item AS t1 
    INNER JOIN tblbudget_longterm_plan_project t5 on t1.LPlanCode = t5.LPlanCode 
    INNER join tblbudget_project AS t2 on t2.PItemCode=t1.PItemCode 
    INNER join tblbudget_project_detail AS t3 on t3.PrjId=t2.PrjId 
    INNER JOIN tblbudget_project_activity t4 ON t3.PrjDetailId = t4.PrjDetailId 
    WHERE ${where.join(" AND ")} 
    ORDER by t1.PItemCode`;
  //console.log(sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_organizeFullName = async (query, PrjCode) => {
  let where = [];
  where.push("t2.EnableStatus='Y'");
  where.push("t2.DeleteStatus='N'");
  //where.push(`t3.Year='${query.BgtYear}'`);
  where.push(`t1.PrjCode='${PrjCode}'`);

  let sql = `SELECT t2.OrgName, t2.OrgShortName,t2.OrganizeCode,t2.OrgId
  FROM tblbudget_project t1
  INNER JOIN tblstructure_operation t2 ON  t1.OrgId = t2.OrgId
  
  WHERE ${where.join(" AND ")}`;
  //console.log(sql);
  //INNER JOIN tblstructure_operation_round t3 ON t2.OrgRound = t3.RoundId
  const org = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return org && org[0] ? org[0] : null
};

exports.get_organizeFP002 = async (query, DocCode) => {
  let where = [];
  where.push("t2.EnableStatus='Y'");
  where.push("t2.DeleteStatus='N'");
  //where.push(`t3.Year='${query.BgtYear}'`);
  where.push(`t1.DocCode='${DocCode}'`);

  let sql = `SELECT t2.OrgName, t2.OrgShortName,t2.OrganizeCode,t2.OrgId
  FROM tblfinance_doccode t1
  INNER JOIN tblstructure_operation t2 ON  REPLACE(t1.RQOrganizeId,"O_","") = t2.OrgId
  
  WHERE ${where.join(" AND ")}`;
  console.log(sql);
  //INNER JOIN tblstructure_operation_round t3 ON t2.OrgRound = t3.RoundId
  const org = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return org && org[0] ? org[0] : null
};

exports.get_organizeByAct = async (query, PrjActCode) => {
  let where = [];
  where.push("t2.EnableStatus='Y'");
  where.push("t2.DeleteStatus='N'");
  //where.push(`t3.Year='${query.BgtYear}'`);
  where.push(`act.PrjActCode='${PrjActCode}'`);

  let sql = `SELECT t2.OrgName, t2.OrgShortName,t2.OrganizeCode,t2.OrgId
  FROM tblbudget_project_activity AS act
  LEFT JOIN tblbudget_project_detail AS de ON act.PrjDetailId=de.PrjDetailId
  LEFT JOIN tblbudget_project t1 ON t1.PrjCode=de.PrjCode
  LEFT JOIN tblstructure_operation t2 ON  t1.OrgId = t2.OrgId
  WHERE ${where.join(" AND ")}
  GROUP BY t2.OrgName`;
  //console.log(sql);
  //INNER JOIN tblstructure_operation_round t3 ON t2.OrgRound = t3.RoundId
  const org = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return org && org[0] ? org[0] : null
};

exports.get_mouContractIncome = async (query,PItemCode, PrjActCode, SourceExId) => {
  let where = [];
  where.push(` bfi.DeleteStatus='N'
        AND p.DeleteStatus='N'
        AND p.ActiveStatus='Y'
        AND fd.UseStatus='Y'
        AND bpd.ActiveStatus='Y' `)
  if(PrjActCode) where.push(`bfi.PrjActCode = '${PrjActCode}'`);
  if(SourceExId) where.push(`bfi.SourceExId = '${SourceExId}'`);
  let sql = `SELECT p.ContractNo
    , fd.DocCode, fd.PItemCode, fd.PrjActCode
    , fd.PrjDetailId AS PrjDetailIdOrig, fd.PrjId AS PrjIdOrig
    , bpa.PrjActId
    , bpd.PrjCode, bpd.PrjDetailId, bpd.PrjId
    , blp.LPlanCode, blp.LPrjCode
    , bfi.RunNoCode, bfi.CreateDate, bfi.PayDate, bfi.IncomeValue, bfi.IncomeDetail
    , ac.PV, ac.ActionDate
    FROM tblbudget_finance_income bfi
    INNER JOIN tblmou_project p ON p.ContractNo=bfi.ReferCode
    INNER JOIN tblmou_project_doccode pd ON p.MouCode=pd.MouCode
    INNER JOIN tblfinance_doccode fd ON pd.DocCode=fd.DocCode AND fd.DocStatusId NOT IN (8,13)
    LEFT JOIN tblbudget_project_activity bpa ON bpa.PrjActCode=fd.PrjActCode
    LEFT JOIN tblbudget_project_detail bpd ON bpd.PrjDetailId=bpa.PrjDetailId
    INNER JOIN tblbudget_project bp ON bp.PrjId=bpd.PrjId
    LEFT JOIN tblbudget_longterm_plan_project blp ON blp.LPrjCode=bp.LPrjCode
    LEFT JOIN ac_action ac ON ac.IncomeId=bfi.IncomeId AND ac.DeleteStatus='N'
    WHERE ${where.join(' AND ')}`;
  //console.log(sql);
  const result = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return result[0] && result[0].IncomeValue ? parseFloat(result[0] && result[0].IncomeValue) : 0 ;
};

exports.get_sumBGPlan = async (query, PItemCode , LPrjCode , PrjDetailId , PrjActCode, SourceExId, OrgId ) => {
  let where = [];
  where.push("t2.ActiveStatus='Y' AND t1.EnableStatus='Y' AND t1.DeleteStatus='N'");
  where.push(`t1.BgtYear='${query.BgtYear}'`);
  //if(PItemCode) where.push(`t1.PItemCode = '${PItemCode}'`);
  //if(PrjDetailId) where.push(`t2.PrjDetailId = ${PrjDetailId}`);
  if(PrjActCode) where.push(`t3.PrjActCode = '${PrjActCode}'`);
  //if(LPrjCode) where.push(`t5.LPrjCode = '${LPrjCode}'`);
  if(SourceExId) where.push(`t4.SourceExId = '${SourceExId}'`);
  if(OrgId) where.push(`t1.OrgId='${OrgId}'`);

  let sql = `SELECT sum(t8.Budget)AS total 
    FROM tblbudget_project_activity_cost_external_month AS t8 
    LEFT JOIN tblbudget_project_activity_cost_external AS t4 ON t4.CostExtId = t8.CostExtId 
    LEFT JOIN tblbudget_project_activity AS t3 ON t3.PrjActId = t4.PrjActId 
    LEFT JOIN tblbudget_project_detail AS t2 ON t2.PrjDetailId = t3.PrjDetailId 
    LEFT JOIN tblbudget_project AS t1 ON t1.PrjId = t2.PrjId 
    LEFT JOIN tblbudget_longterm_plan_project t5 ON t1.LPrjCode = t5.LPrjCode
    WHERE ${where.join(" AND ")} `;
  //console.log(sql);
  const result = await sequelize.query(sql , { type: QueryTypes.SELECT });
  //console.log('get_sumBGPlan',result);
  return result[0] && result[0].total ? parseFloat(result[0] && result[0].total) : 0 ;
};

exports.get_sumBGTferIn = async (query, PItemCode , LPrjCode , PrjDetailId , PrjActCode, SourceExId, OrgId ) => {
  let where = [];
  where.push("t2.ActiveStatus='Y' AND t1.EnableStatus='Y' AND t1.DeleteStatus='N'");
  where.push(`t1.BgtYear='${query.BgtYear}'`);
  where.push("t8.DocStatusId NOT IN (8,13,99)");
  //if(PItemCode) where.push(`t1.PItemCode = '${PItemCode}'`);
  //if(PrjDetailId) where.push(`t2.PrjDetailId = ${PrjDetailId}`);
  if(PrjActCode) where.push(`t8.PrjActCodeTo = '${PrjActCode}'`);
  //if(LPrjCode) where.push(`t5.LPrjCode = '${LPrjCode}'`);
  if(SourceExId) where.push(`t8.SourceExId = '${SourceExId}'`);
  if(OrgId) where.push(`t1.OrgId='${OrgId}'`);

  let sql = `SELECT SUM(t8.SumBGTfer) AS total 
    FROM tblfinance_bg_transfer AS t8 
    INNER JOIN tblbudget_project_activity AS t3 ON t3.PrjActId = t8.PrjActId and t3.PrjActCode = t8.PrjActCodeTo 
    LEFT JOIN tblbudget_project_detail AS t2 ON t2.PrjDetailId = t3.PrjDetailId 
    LEFT JOIN tblbudget_project AS t1 ON t1.PrjId = t2.PrjId 
    LEFT JOIN tblbudget_longterm_plan_project t5 ON t1.LPrjCode = t5.LPrjCode
    WHERE ${where.join(" AND ")} `;
  //console.log(sql);
  const result = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return result[0] && result[0].total ? parseFloat(result[0] && result[0].total) : 0 ;
};

exports.get_sumBGTferOut = async (query, PItemCode , LPrjCode , PrjDetailId , PrjActCode, SourceExId,OrgId  ) => {
  let where = [];
  where.push("t2.ActiveStatus='Y' AND t1.EnableStatus='Y' AND t1.DeleteStatus='N'");
  where.push(`t1.BgtYear='${query.BgtYear}'`);
  where.push("t8.DocStatusId NOT IN (8,13,99)");
  //if(PItemCode) where.push(`t1.PItemCode = '${PItemCode}'`);
  //if(PrjDetailId) where.push(`t2.PrjDetailId = ${PrjDetailId}`);
  if(PrjActCode) where.push(`t8.PrjActCodeFrom = '${PrjActCode}'`);
  //if(LPrjCode) where.push(`t5.LPrjCode = '${LPrjCode}'`);
  if(SourceExId) where.push(`t8.SourceExId = '${SourceExId}'`);
  if(OrgId) where.push(`t1.OrgId='${OrgId}'`);

  let sql = `SELECT SUM(t8.SumBGTfer) AS total 
    FROM tblfinance_bg_transfer as t8 
    INNER JOIN tblbudget_project_activity AS t3 ON t3.PrjActId = t8.PrjActId and  t3.PrjActCode = t8.PrjActCodeFrom 
    LEFT JOIN tblbudget_project_detail AS t2 ON t2.PrjDetailId = t3.PrjDetailId 
    LEFT JOIN tblbudget_project AS t1 ON t1.PrjId = t2.PrjId 
    LEFT JOIN tblbudget_longterm_plan_project t5 ON t1.LPrjCode = t5.LPrjCode
    WHERE ${where.join(" AND ")} `;
  //console.log(sql);
  const result = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return result[0] && result[0].total ? parseFloat(result[0] && result[0].total) : 0 ;
};

exports.get_sumBGOther = async (query, PItemCode , LPrjCode , PrjDetailId , PrjActCode, SourceExId,OrgId ) => {
  let where = [];
  
  where.push("t2.ActiveStatus='Y' AND t10.EnableStatus='Y' AND t10.DeleteStatus='N'");
  where.push(`t10.BgtYear='${query.BgtYear}'`);
  where.push("t8.DocStatusId NOT IN (8,13,99)");
  //if(PItemCode) where.push(`t10.PItemCode = '${PItemCode}'`);
  //if(PrjDetailId) where.push(`t3.PrjDetailId = ${PrjDetailId}`);
  if(PrjActCode) where.push(`t8.PrjActCode = '${PrjActCode}'`);
  //if(LPrjCode) where.push(`t5.LPrjCode = '${LPrjCode}'`);
  if(SourceExId) where.push(`t8.SourceExId = '${SourceExId}'`);
  if(OrgId) where.push(`t10.OrgId='${OrgId}'`);

  let sql = `SELECT sum(t9.OtherSumCost) AS total
    FROM tblfinance_form_paybr as t8 
    Inner Join tblfinance_form_paybr_cost_other AS t9 ON t8.DocCode = t9.DocCode 
    Inner Join tblbudget_project_activity AS t3 ON t3.PrjActCode = t8.PrjActCode 
    Inner Join tblbudget_project_detail AS t2 ON t2.PrjDetailId = t3.PrjDetailId 
    Inner Join tblbudget_init_cost_item AS t6 ON t6.CostItemCode = t9.CostItemCode 
    Inner Join tblbudget_init_cost_type AS t7 ON t7.CostTypeId = t6.CostTypeId 
    left Join tblbudget_project AS t10 ON t10.PrjId = t2.PrjId 
    LEFT JOIN tblbudget_longterm_plan_project t5 ON t10.LPrjCode = t5.LPrjCode
    WHERE ${where.join(" AND ")} `;
  //console.log(sql);
  const result = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return result[0] && result[0].total ? parseFloat(result[0] && result[0].total) : 0 ;
};


exports.get_sumBGPay = async (query, PItemCode , LPrjCode , PrjDetailId, PrjActCode, SourceExId, OrgId ) => {
  let where = [];
  //where.push("t8.CancelStatusxx='N'");
  where.push("t2.ActiveStatus='Y' AND t1.EnableStatus='Y' AND t1.DeleteStatus='N'");
  where.push(`t1.BgtYear='${query.BgtYear}'`);
  where.push("t9.DocStatusId NOT IN (8,13)");
  //if(PItemCode) where.push(`t10.PItemCode = '${PItemCode}'`);
  //if(PrjDetailId) where.push(`t3.PrjDetailId = ${PrjDetailId}`);
  if(PrjActCode) where.push(`t8.PrjActCode = '${PrjActCode}'`);
  //if(LPrjCode) where.push(`t5.LPrjCode = '${LPrjCode}'`);
  if(SourceExId) where.push(`t8.SourceExId = '${SourceExId}'`);
  if(OrgId) where.push(`t1.OrgId='${OrgId}'`);
  let sql = `SELECT
      SUM(t8.SumBGPay) AS total
    FROM 
    tblfinance_bg_pay AS t8
    LEFT JOIN tblfinance_doccode AS t9 ON t9.DocCode = t8.DocCode
    LEFT JOIN tblbudget_project_activity AS t3 ON t3.PrjActCode = t8.PrjActCode
    LEFT JOIN tblbudget_project_detail AS t2 ON t2.PrjDetailId = t3.PrjDetailId
    LEFT JOIN tblbudget_project AS t1 ON t1.PrjId = t2.PrjId
    LEFT JOIN tblbudget_longterm_plan_project t5 ON t1.LPrjCode = t5.LPrjCode
  
    WHERE ${where.join(" AND ")}`;
  if(PrjActCode=='66P02B04') console.log(sql);
  const result = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return result[0] && result[0].total ? parseFloat(result[0] && result[0].total) : 0 ;
};

exports.get_sumBGHold = async (query, PItemCode , LPrjCode , PrjDetailId , PrjActCode, SourceExId ,OrgId) => {
  let where = [];
  where.push("t2.ActiveStatus='Y' AND t1.EnableStatus='Y' AND t1.DeleteStatus='N'");
  where.push(`t1.BgtYear='${query.BgtYear}'`);
  where.push("t9.DocStatusId NOT IN (8,13)");
  //if(PItemCode) where.push(`t1.PItemCode = '${PItemCode}'`);
  //if(PrjDetailId) where.push(`t2.PrjDetailId = ${PrjDetailId}`);
  if(PrjActCode) where.push(`t8.PrjActCode = '${PrjActCode}'`);
  //if(LPrjCode) where.push(`t1.LPrjCode = '${LPrjCode}'`);
  //if(SourceExId) where.push(`t4.SourceExId = '${SourceExId}'`);
  if(OrgId) where.push(`t1.OrgId='${OrgId}'`);

  const condition = `
    LEFT JOIN tblfinance_doccode AS t9 ON t9.DocCode = t8.DocCode
    LEFT JOIN tblbudget_project_activity AS t3 ON t3.PrjActCode = t8.PrjActCode
    LEFT JOIN tblbudget_project_detail AS t2 ON t2.PrjDetailId = t3.PrjDetailId
    LEFT JOIN tblbudget_project AS t1 ON t1.PrjId = t2.PrjId
    WHERE ${where.join(" AND ")}`;

  let sql = `SELECT SUM( SumBGHold ) as sum  
    FROM tblfinance_bg_hold AS t8 
    ${condition} AND t8.FormCode <> 'FP001' AND t9.SourceExId = '${SourceExId}'`;
  //console.log(sql);
  const BGHold1 = await sequelize.query(sql , { type: QueryTypes.SELECT });
  //console.log('BGHold1',BGHold1);

  let sql2 = `SELECT SUM( t4.SumCost ) as sum 
    FROM tblfinance_form_hold_cost AS t8 
    LEFT JOIN tblbudget_project_activity_cost_external as t4 ON t4.CostExtId=t8.CostInt_ExtId
    ${condition} AND t4.SourceExId = '${SourceExId}'`;
  //console.log(sql2);
  const BGHold2 = await sequelize.query(sql2 , { type: QueryTypes.SELECT });
  //console.log('BGHold2',BGHold2);

  return  parseFloat(BGHold1[0].sum || 0) + parseFloat(BGHold2[0].sum || 0);
};

exports.list_BGChain = async (query, PItemCode , LPrjCode , PrjDetailId , PrjActId ) => {
  let where = [];
  where.push("t2.ActiveStatus='Y'");
  where.push(`t1.BgtYear='${query.BgtYear}'`);
  if(PItemCode) where.push(`t1.PItemCode = '${PItemCode}'`);

  let sql = `select t3.PrjActId, t3.PrjActCode, t2.PrjDetailId, t5.LPlanCode,	t5.LPrjCode 
            from tblbudget_project_activity as t3 
            left Join tblbudget_project_detail AS t2 ON t2.PrjDetailId = t3.PrjDetailId 
            left Join tblbudget_project AS t1 ON t1.PrjId = t2.PrjId 
            left join tblbudget_longterm_plan_project t5 on t1.LPrjCode = t5.LPrjCode
            WHERE ${where.join(" AND ")}
            order by t1.BgtYear desc, t2.PrjDetailId, t3.PrjActId`;
  //console.log(sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.sumBGChain = async (query, PItemCode , LPrjCode , PrjDetailId , PrjActId, $DocCode ) => {
  let where = [];
  where.push("t2.ActiveStatus='Y'");
  where.push(`t1.BgtYear='${query.BgtYear}'`);
  if(PItemCode) where.push(`t1.PItemCode = '${PItemCode}'`);

  let sql = `select t3.PrjActId, t3.PrjActCode, t2.PrjDetailId, t5.LPlanCode,	t5.LPrjCode 
            from tblbudget_project_activity as t3 
            left Join tblbudget_project_detail AS t2 ON t2.PrjDetailId = t3.PrjDetailId 
            left Join tblbudget_project AS t1 ON t1.PrjId = t2.PrjId 
            left join tblbudget_longterm_plan_project t5 on t1.LPrjCode = t5.LPrjCode
            WHERE ${where.join(" AND ")}
            order by t1.BgtYear desc, t2.PrjDetailId, t3.PrjActId`;
  //console.log(sql);
  const result = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return result[0] && result[0].total ? parseFloat(result[0] && result[0].total) : 0 ;
};

exports.get_sumBGChain = async (query, PItemCode , LPrjCode , PrjDetailId , PrjActCode, SourceExId, OrgId ) => {

  /*const listBGChain = await this.list_BGChain(query, planItem.PItemCode);
  let result = 0;
  for await( const item of listBGChain) {
    
  }*/
  let where = [];
  where.push("t2.ActiveStatus='Y' AND t1.EnableStatus='Y' AND t1.DeleteStatus='N'");
  //where.push(`t1.BgtYear='${query.BgtYear}'`);

  //where.push(`t9.DocDate BETWEEN '${parseInt(query.BgtYear)-544}-10-01' AND '${parseInt(query.BgtYear)-543}-09-30'`);
  //where.push("(t9.DocStatusId NOT IN (8,13) or t8.ProcureCode !='' or t8.ContractNo!='')");
  //if(PItemCode) where.push(`t10.PItemCode = '${PItemCode}'`);
  if(PrjDetailId) where.push(`t2.PrjDetailId = ${PrjDetailId}`);
  if(PrjActCode) where.push(`t8.PrjActCode = '${PrjActCode}'`);
  //if(LPrjCode) where.push(`t5.LPrjCode = '${LPrjCode}'`);
  if(SourceExId) where.push(`t13.SourceExId = '${SourceExId}'`);
  if(SourceExId) where.push(`(t9.BgtYear='${query.BgtYear}' OR t8.BgtYear='${query.BgtYear}')`);
  if(OrgId) where.push(`t1.OrgId='${OrgId}'`);

  let sql = `SELECT sum(t8.SumBGChain)as total 
          FROM 
          tblfinance_bg_chain as t8 
          left Join tblfinance_doccode AS t9 ON t9.DocCode = t8.DocCode 
          left Join tblbudget_project_activity AS t3 ON t3.PrjActCode = t8.PrjActCode 
          LEFT JOIN (
              SELECT
                  PrjActId,
                  SourceExId,
                  ActivityPlan
              FROM
                  tblbudget_project_activity_cost_external 
                  GROUP BY PrjActId
              
          ) AS t13 ON t13.PrjActId = t3.PrjActId
          left Join tblbudget_project_detail AS t2 ON t2.PrjDetailId = t3.PrjDetailId 
          left Join tblbudget_project AS t1 ON t1.PrjId = t2.PrjId AND t8.SumBGChain>0
            WHERE ${where.join(" AND ")} `;
  
  if(PrjActCode=='66P02B04') console.log(sql);
  const result = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return result[0] && result[0].total && Number(result[0].total)>0 ? parseFloat(result[0].total) : 0 ;

};

exports.get_project5Year = async (query, LPlanCode ) => {
  let where = [];
  where.push("t2.EnableStatus='Y'");
  where.push("t2.DeleteStatus='N'");
  where.push(`t1.LPlanCode='${LPlanCode}'`);

  let sql = `SELECT distinct(t1.LPrjCode), LPrjName
        FROM tblbudget_longterm_plan_project t1
        INNER JOIN tblbudget_project t2 on t1.LPrjCode = t2.LPrjCode
        INNER JOIN tblbudget_project_detail t3 on t2.PrjCode = t3.PrjCode
        INNER JOIN tblbudget_project_activity t4 on t3.PrjDetailId = t4.PrjDetailId
            WHERE ${where.join(" AND ")}
            ORDER BY t2.PrjCode`;
  //console.log(sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_projectRecordSet = async (query, PItemCode , LPrjCode ) => {
  let where = [];
  where.push("t1.EnableStatus='Y'");
  where.push("t1.DeleteStatus='N'");
  where.push("t2.ActiveStatus='Y'");
  if(LPrjCode) where.push(`t1.LPrjCode='${LPrjCode}'`);
  if(PItemCode) where.push(`t1.PItemCode='${PItemCode}'`);

  let sql = `SELECT distinct(t1.PrjCode), t1.PrjName, t2.PrjDetailId , t1.PrjId 
            FROM tblbudget_project t1 
            INNER JOIN tblbudget_project_detail t2 on t1.PrjId = t2.PrjId
            INNER JOIN tblbudget_project_activity t3 on t2.PrjDetailId = t3.PrjDetailId
            WHERE ${where.join(" AND ")}
            ORDER BY t1.PrjCode`;
  console.log('#get_projectRecordSet ',sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_actRecordSet = async (query, PrjCode ) => {
  let where = [];
  where.push("t1.ActiveStatus='Y'");
  where.push(`t1.PrjCode='${PrjCode}'`);

  let sql = `SELECT t2.PrjActId, t2.PrjActCode, t2.PrjActName, t1.PrjDetailId 
            FROM tblbudget_project_activity as t2 
            LEFT JOIN tblbudget_project_detail t1 on t1.PrjDetailId = t2.PrjDetailId AND t1.PrjDetailId = (
              SELECT MAX( PrjDetailId )
              FROM tblbudget_project_detail
              WHERE PrjCode = '${PrjCode}' )
            WHERE ${where.join(" AND ")}
            ORDER BY t2.PrjActCode ASC`;
  console.log('#get_actRecordSet',sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_Purchase = async (query, PrjActCode ) => {
  let where = [];
  where.push("t1.EnableStatus='Y'");
  where.push("t1.DeleteStatus='N'");
  where.push(`t1.BgtYear='${query.BgtYear}'`);
  if(PrjActCode) where.push(`t1.PrjActCode='${PrjActCode}'`);

  let sql = `SELECT t1.ProcureID,t1.SourceExIdCheckIn,DocId,t1.FormCode,t1.DocDate,t1.DocCode,t1.CloseStatus,
          t1.ProcureCode,t1.Topic,t1.TotalBGChain,t1.SourceType,t1.BgtYear,t1.OrganizeCode,t1.PrjActCode,
          t1.ProcureDate,t1.ProcureTopic,t1.PartnerCode,t1.SendDate,StartWork,FinishWork,
          t1.ProcureTotalAmount,t1.Tax,t1.Discount,t1.StatusName,t1.ApproveDate,t1.ValueInPlan,doc.DocStatusId,
          IF(t1.FormCode='FP002',docSource.SourceExName,source.SourceExName) AS SourceName,
          (
            SELECT SUM(AmountItem) FROM stock_doc_purchase_item AS item
            WHERE EnableStatus='Y' AND DeleteStatus='N' AND item.ProcureId=t1.ProcureId
          ) AS Budget
            FROM stock_doc_purchase t1 
            LEFT JOIN tblfinance_doccode AS doc ON doc.DocCode=t1.DocCode
            LEFT JOIN tblbudget_init_source_external AS docSource ON doc.SourceExId=docSource.SourceExId
            LEFT JOIN tblbudget_init_source_external AS source ON t1.SourceExIdCheckIn=source.SourceExId
            WHERE ${where.join(" AND ")}
            ORDER BY t1.PrjActCode ASC`;
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_ContractType = async (query, PrjActCode, InvenTypeId ) => {
  let where = [];
  where.push("t1.EnableStatus='Y'");
  where.push("t1.DeleteStatus='N'");
  if(InvenTypeId)where.push(`t1.InvenTypeId='${InvenTypeId}'`);
  where.push(`t1.BgtYear='${query.BgtYear}'`);
  if(PrjActCode) where.push(`(t1.PrjActCodeCheckIn='${PrjActCode}' OR t1.PrjActCode='${PrjActCode}')`);

  let sql = `SELECT t1.ContractId,t1.SourceExIdCheckIn,t1.DocId,t1.FormCode,t1.DocDate,t1.DocCode,
            t1.ContractNo,t1.Topic,t1.TotalBGChain,t1.SourceType,t1.BgtYear,t1.OrganizeCode,t1.PrjActCode,
            t1.ContractDate,t1.ContractDate,t1.PartnerCode,doc.DocStatusId,
            t1.ContractTotalAmount,t1.StatusName,t1.ApproveDate,t1.ValueInPlan,
            t1.ContractTopic,t1.CloseStatus,
            IF(t1.FormCode='FP002',docSource.SourceExName,source.SourceExName) AS SourceName,
            (
              SELECT SUM(ContractTimeMoney) FROM stock_doc_con_contracttime
              WHERE EnableStatus='Y' AND DeleteStatus='N' AND ContractId=t1.ContractId
            ) AS Budget,
            (
              SELECT SUM(Budget) FROM tblfinance_doccode AS doc
              WHERE doc.EnableStatus='Y' AND t1.DeleteStatus='N' AND doc.CodeReferId=t1.ContractId
            ) AS BGPayment, t1.PrjActCodeCheckIn
              FROM stock_doc_con_contract t1 
              LEFT JOIN tblfinance_doccode AS doc ON doc.DocCode=t1.DocCode
              LEFT JOIN tblbudget_init_source_external AS docSource ON doc.SourceExId=docSource.SourceExId
              LEFT JOIN tblbudget_init_source_external AS source ON t1.SourceExIdCheckIn=source.SourceExId
            WHERE ${where.join(" AND ")}
            ORDER BY t1.PrjActCode ASC`;
            /*, sour.SourceCode	, sour.SourceExName AS SourceName	
            LEFT JOIN tblbudget_project_activity AS act ON act.PrjActCode=t1.PrjActCode
            LEFT JOIN  tblbudget_project_activity_cost_external AS cost ON act.PrjActId=cost.PrjActId
            LEFT JOIN tblbudget_init_source_external AS sour ON sour.SourceExId=cost.SourceExId*/
  if(PrjActCode=='67P05E02')console.log('#get_ContractType',sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_Mou = async (query, PrjActCode, ContractTypeId ) => {
  let result = [];
  try{
    let where = [];
    where.push("t7.ActiveStatus='Y'");
    where.push("t7.CancelStatus='N'");
    where.push("t1.DeleteStatus='N'");
    where.push("t7.DeleteStatus='N'");
    //where.push("PGroupId=12");
    where.push(`t7.BgtYear='${query.BgtYear}'`);
    where.push(`t4.PrjActCode='${PrjActCode}'`);

    let sql = `SELECT t1.PItemName, t2.PrjName, t3.PrjDetailId, t4.PrjActId, t4.PrjActName, t4.PrjActCode, 
              t5.DocDate, t5.FormCode, t5.DocCode, t5.DocCodeRefer , t7.* ,
              IF(t7.CloseStatus='Y' OR t7.CancelStatus='Y','สิ้นสุดโครงการ','อยู่ระหว่างดำเนินการ') StatusName,
              t7.ContractNo MouNo,
              t8.OrgId,t8.OrgIdRef,t8.OrgName,t8.OrganizeCode AS OrganizationCode,t8.OrgShortName,
              fd.DocStatusId,fd.Budget AS BGHold,s.SourceExName AS SourceName
              FROM tblbudget_init_plan_item t1
              LEFT JOIN tblbudget_project t2 ON t1.PItemCode = t2.PItemCode
              LEFT JOIN tblbudget_project_detail t3 ON t2.PrjCode = t3.PrjCode
              LEFT JOIN tblbudget_project_activity t4 ON t3.PrjDetailId = t4.PrjDetailId
              LEFT JOIN tblfinance_doccode t5 ON t2.Prjid = t5.Prjid AND t3.PrjDetailId = t5.PrjDetailId AND t4.PrjActCode = t5.PrjActCode
              LEFT JOIN tblfinance_doccode fd ON t5.DocCodeRefer=fd.DocCode 
              LEFT JOIN tblmou_project_doccode t6 ON t5.DocCode = t6.DocCode
              LEFT JOIN tblmou_project t7 ON t6.MouCode = t7.MouCode
              LEFT JOIN tblbudget_init_source_external AS s ON t5.SourceExId=s.SourceExId
              LEFT JOIN tblstructure_operation t8 ON REPLACE(t5.RQOrganizeId,'O_','') = t8.OrgId
              WHERE ${where.join(" AND ")}
              ORDER BY t7.ContractNo ASC`;
    //console.log(sql);
    const list = await sequelize.query(sql , { type: QueryTypes.SELECT });
    
    for( const item of list) {
      let task = await this.get_MouTask(query, item.MouId)
      let receipt = await this.get_MouReceipt(query, item.ContractNo)
      //let chain = await this.get_MouChain(query, item.ContractNo)//FC002 (tblmou_project_doccode+tblfinance_bg_chain)
      if(receipt){
        task.Payment = Number(task.Payment)-Number(receipt.IncomeValue)
        task.Total = Number(task.Total)+Number(receipt.IncomeValue)
       //console.log('get_Mou',item.ContractNo,task.Payment,receipt.IncomeValue)
      }
      result.push({...item,...task})
    }
  } catch (e) {
    console.log(e);
  }
  return result;
};

exports.get_MouTask = async (query, MouId ) => {
  let where = [];
  where.push("task.DeleteStatus='N'");
  where.push(`task.MouId='${MouId}'`);

  let sql = `SELECT SUM(BGRepay) BGRepay, SUM(BGOperate) BGOperate, SUM(TaskBudget) Budget,
            SUM(
              (SELECT tfc.Budget 
              FROM  tblfinance_doccode tfc
              WHERE tfc.DocStatusId not in (8,13) AND tfc.FormCode='FC003'
              AND tfc.DocCode IS NOT NULL AND tfc.TaskId=task.TaskId)
            ) Payment
            FROM tblmou_project_task task 
            LEFT JOIN tblmou_project mou on mou.MouId=task.MouId
            WHERE ${where.join(" AND ")}`;
  //console.log(sql);
  const task = await sequelize.query(sql , { type: QueryTypes.SELECT });
  if(task && task.length>0){
    task[0].Total = parseFloat(task[0].Budget)-parseFloat(task[0].Payment);
    return task[0]
  } else return null
};
exports.get_MouReceipt = async (query, ContractNo ) => {
  let sql = `SELECT p.ContractNo
    , fd.DocCode, fd.PItemCode, fd.PrjActCode, fd.PrjDetailId
    , bpa.PrjActId
    , bfi.RunNoCode, bfi.CreateDate, bfi.PayDate, bfi.IncomeValue, bfi.IncomeDetail
    , ac.PV, ac.ActionDate
    FROM tblbudget_finance_income bfi
    INNER JOIN tblmou_project p ON p.ContractNo=bfi.ReferCode
    INNER JOIN tblmou_project_doccode pd ON p.MouCode=pd.MouCode
    INNER JOIN tblfinance_doccode fd ON pd.DocCode=fd.DocCode AND fd.DocStatusId NOT IN (8,13)
    INNER JOIN tblbudget_project_activity bpa ON bpa.PrjActCode=fd.PrjActCode AND bpa.PrjDetailId=fd.PrjDetailId
    LEFT JOIN ac_action ac ON ac.IncomeId=bfi.IncomeId AND ac.DeleteStatus='N'
    WHERE
      bfi.DeleteStatus='N'
      AND p.DeleteStatus='N'
      AND p.ActiveStatus='Y'
      AND fd.UseStatus='Y'
      AND p.ContractNo='${ContractNo}'`
    const receipt = await sequelize.query(sql , { type: QueryTypes.SELECT });
    if(receipt && receipt.length>0){
      return receipt[0]
    } else return null
    
}

exports.get_bookBankGroup = async () => {
  let where = [];
  where.push("t1.DeleteStatus='N'");
  where.push("t1.EnableStatus='Y'");
  //where.push("t1.SourceExId=1");

  let sql = `SELECT t1.BookbankGroupName,t1.BookbankGroupId,t1.SourceExId,t1.PrjCode
            FROM tblbudget_finance_bookbank_group t1 
            WHERE ${where.join(" AND ")}
            ORDER BY BookbankGroupId ASC`;
  console.log(sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_acChartCode = async (query, type, AcSeriesId ) => {
  let where = [];
  where.push("t1.DeleteStatus='N'");
  if(type=='parent') where.push(` (ParentId IS NULL OR ParentId = 0)`);
  if(AcSeriesId) where.push(`AcSeriesId=${AcSeriesId}`);

  let sql = `SELECT t1.*
            FROM ac_chart t1 
            WHERE ${where.join(" AND ")}
            ORDER BY tqAcChartCode ASC`;
  //console.log(sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_acDetail = async (query ) => {
  let where = [];
  where.push("d.DeleteStatus='N'");
  //where.push(`YYear='${query.BgtYear}'`);
  where.push(`ActionDate>'${parseInt(query.BgtYear)-544}-10-01' AND ActionDate<'${parseInt(query.BgtYear)-543}-09-30'`);
  
  let sql = `SELECT d.*, a.*,s.SourceExName	,s.SourceCode,
            IF(pd.AcChartCode,pd.AcChartCode,pc.AcChartCode) AS AcChartCodeParent,
            IF(cd.AcChartId,cd.AcChartId,cc.AcChartId) AS AcChartId,
            IF(cd.PartnerCode,cd.PartnerCode,cc.AcChartId) AS PartnerCode,
            IF(cd.AcSeriesId,cd.AcChartId,cc.AcSeriesId) AS AcSeriesId,
            IF(cd.ThaiName,cd.ThaiName,cc.ThaiName) AS ThaiName,
            IF(cd.EngName,cd.EngName,cc.EngName) AS EngName,
            IF(cd.AcType,cd.AcType,cc.AcType) AS AcType,
            IF(cd.AcGroupId,cd.AcGroupId,cc.AcGroupId) AS AcGroupId,
            IF(cd.AcChartCode,cd.AcChartCode,cc.AcChartCode) AS AcChartCode,
            IF(cd.ParentId,cd.ParentId,cc.ParentId) AS ParentId,
            IF(cd.Deep,cd.Deep,cc.Deep) AS Deep
            FROM ac_action_detail  AS d
            LEFT JOIN ac_action AS a ON a.AcActionId=d.AcActionId
            LEFT JOIN tblbudget_init_source_external AS s ON s.SourceExId=a.SourceExId
            LEFT JOIN ac_chart AS cd ON d.DrId=cd.AcChartId AND cd.EnableStatus='Y' AND cd.DeleteStatus='N'
            LEFT JOIN ac_chart AS pd ON pd.AcChartId=cd.ParentId AND pd.EnableStatus='Y' AND pd.DeleteStatus='N'
            LEFT JOIN ac_chart AS cc ON d.CrId=cc.AcChartId AND cc.EnableStatus='Y' AND cc.DeleteStatus='N'
            LEFT JOIN ac_chart AS pc ON pc.AcChartId=cc.ParentId AND pc.EnableStatus='Y' AND cc.DeleteStatus='N'
            WHERE ${where.join(" AND ")}`;
  //console.log(sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_acChart = async ( query , PrjCode) => {
  let where = [];
  where.push("Ad.DeleteStatus='N'");
  where.push("Ad.EnableStatus='Y'");
  where.push(`Aa.ActionDate BETWEEN '${parseInt(query.BgtYear)-544}-10-01' AND '${parseInt(query.BgtYear)-543}-09-30'`);
  let whereChart = [];
  whereChart.push("Ch.DeleteStatus='N'");
  if(PrjCode || PrjCode=='')where.push(`Aa.SourceExId='${PrjCode}'`);
  //whereChart.push("Ch.EnableStatus='Y'");
  if(query.ParentId || query.ParentId==0) whereChart.push(`Ch.ParentId=${query.ParentId}`);

  let sql = `SELECT AcChartId, ThaiName, AcChartCode,AcGroupId, ParentId,Deep,
            ( SELECT SUM(CrValue) 
            FROM ac_action_detail AS Ad 
              LEFT JOIN ac_action AS Aa ON Aa.AcActionId=Ad.AcActionId 
              WHERE Ad.CrId=Ch.AcChartId AND ${where.join(" AND ")} ) AS Credit ,
            ( SELECT SUM(DrValue) FROM ac_action_detail AS Ad 
              LEFT JOIN ac_action AS Aa ON Aa.AcActionId=Ad.AcActionId 
              WHERE Ad.DrId=Ch.AcChartId AND ${where.join(" AND ")} ) AS Debit 
            FROM ac_chart AS Ch 
            WHERE ${whereChart.join(" AND ")}
            ORDER BY Ch.AcChartId ASC`
  //if(PrjCode!='')console.log(sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_sumAcDetail = async ( query, AcChartId, PrjCode ) => {
  let where = [];
  where.push("t2.DeleteStatus='N'");
  //if(AcChartId && type=='debit')where.push(`t1.DrId=${AcChartId}`);
  //if(AcChartId && type=='credit')where.push(`t1.CrId=${AcChartId}`);

  where.push("PV IS NOT NULL");
  if(AcChartId )where.push(`( t1.DrId =${AcChartId} OR t1.CrId=${AcChartId})`);
  if(PrjCode || PrjCode=='')where.push(`t2.SourceExId='${PrjCode}'`);
  where.push(`t2.ActionDate BETWEEN '${parseInt(query.BgtYear)-544}-10-01' AND '${parseInt(query.BgtYear)-543}-09-30'`);

  let sql = ` SELECT SUM(t1.DrValue - t1.CrValue) AS total 
          FROM (ac_action_detail AS t1
            INNER JOIN ac_action AS t2 ON t1.AcActionId = t2.AcActionId)
            WHERE ${where.join(" AND ")}`
  //if(AcChartId==617)console.log(sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_sumBroughtForward = async ( query, AcChartId, PrjCode ) => {
  let where = [];
  where.push("t2.DeleteStatus='N'");
  //if(AcChartId && type=='debit')where.push(`t1.DrId=${AcChartId}`);
  //if(AcChartId && type=='credit')where.push(`t1.CrId=${AcChartId}`);

  where.push("PV IS NOT NULL");
  //where.push(`t2.SourceExId=${query.SourceExId}`);
  where.push(`t2.ActionDate < '${parseInt(query.BgtYear)-544}-10-01'`);
  if(AcChartId )where.push(`( t1.DrId =${AcChartId} OR t1.CrId=${AcChartId})`);
  if(PrjCode || PrjCode=='')where.push(`t2.SourceExId='${PrjCode}'`);

  let sql = ` SELECT SUM(t1.DrValue - t1.CrValue) AS total 
          FROM (ac_action_detail AS t1
            INNER JOIN ac_action AS t2 ON t1.AcActionId = t2.AcActionId)
            WHERE ${where.join(" AND ")}`
  //if(AcChartId==617)console.log(sql);
  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

/////////////////////////// LIST REPORT /////////////////////////

exports.list_reportD1 = async (query) => {

  let result = [];
  const groupPlans = await this.list_groupPlan(query);
  const list_source = await this.list_source(null)
  for await (let group of groupPlans) {
    group.LPlanGroupOrdering = group.Ordering
    //Plan
    const listPlanItem = await this.list_planItem(query, group.LPlanGroupId);
    for await (let planItem of listPlanItem) {
       
      planItem.BGPlan = 0;//await this.get_sumBGPlan(query, planItem.PItemCode);
      planItem.BGTferIn = 0;//await this.get_sumBGTferIn(query, planItem.PItemCode);
      planItem.BGTferOut = 0;//await this.get_sumBGTferOut(query, planItem.PItemCode);
      planItem.BGHold = 0;//await this.get_sumBGHold(query, planItem.PItemCode);
      planItem.BGOther = 0;//await this.get_sumBGOther(query, planItem.PItemCode);
      planItem.BGPlanTfer = 0;//planItem.BGPlan + planItem.BGTferIn - planItem.BGTferOut;
      planItem.BGPay = 0;
      planItem.BGChain = 0;
      planItem.BGPayChain = 0;
      planItem.BGTotal = 0;

      record = {...group,...planItem};
      result.push(record);
      
      //Project 5 Year
      const project5Year = await this.get_project5Year(query, planItem.LPlanCode);
      //console.log('project5Year',project5Year);
      for await(let project5YItem of project5Year) {
        project5YItem.BGPlan = 0;//await this.get_sumBGPlan(query, planItem.PItemCode, project5YItem.LPrjCode);
        project5YItem.BGTferIn = 0;//await this.get_sumBGTferIn(query, planItem.PItemCode, project5YItem.LPrjCode);
        project5YItem.BGTferOut = 0;//await this.get_sumBGTferOut(query, planItem.PItemCode, project5YItem.LPrjCode);
        project5YItem.BGHold = 0;//await this.get_sumBGHold(query, planItem.PItemCode, project5YItem.LPrjCode);
        project5YItem.BGOther = 0;//await this.get_sumBGOther(query, planItem.PItemCode, project5YItem.LPrjCode);
        project5YItem.BGPlanTfer = 0;//project5YItem.BGPlan + project5YItem.BGTferIn - project5YItem.BGTferOut;
        project5YItem.BGPay = 0;
        project5YItem.BGChain = 0;
        project5YItem.BGPayChain = 0;
        project5YItem.BGTotal = 0;
        
        record = {...group,...planItem,...project5YItem };
        result.push(record);

        //Project
        const projectRecordSet = await this.get_projectRecordSet(query, planItem.PItemCode,project5YItem.LPrjCode);
        //console.log('projectRecordSet',projectRecordSet);
        for await(let projectRecItem of projectRecordSet) {
          const orgData = await this.get_organizeFullName(query, projectRecItem.PrjCode);
          projectRecItem.BGPlan = 0;//await this.get_sumBGPlan(query, planItem.PItemCode, project5YItem.LPrjCode, projectRecItem.PrjDetailId);
          projectRecItem.BGTferIn = 0;//await this.get_sumBGTferIn(query, planItem.PItemCode, project5YItem.LPrjCode, projectRecItem.PrjDetailId);
          projectRecItem.BGTferOut = 0;//await this.get_sumBGTferOut(query, planItem.PItemCode, project5YItem.LPrjCode, projectRecItem.PrjDetailId);
          projectRecItem.BGHold = 0;//await this.get_sumBGHold(query, planItem.PItemCode, project5YItem.LPrjCode, projectRecItem.PrjDetailId);
          projectRecItem.BGOther = 0;//await this.get_sumBGOther(query, planItem.PItemCode, project5YItem.LPrjCode, projectRecItem.PrjDetailId);
          projectRecItem.BGPlanTfer = 0;//projectRecItem.BGPlan + projectRecItem.BGTferIn - projectRecItem.BGTferOut;
          projectRecItem.BGPay = 0;
          projectRecItem.BGChain = 0;
          projectRecItem.BGPayChain = 0;
          projectRecItem.BGTotal = 0;
          
          record = {...group,...planItem,...project5YItem,...projectRecItem,...orgData };
          result.push(record);

          //Activity
          const actRecordSet = await this.get_actRecordSet(query, projectRecItem.PrjCode);
          //console.log('orgData',orgData);
          for await(let actRecordSetItem of actRecordSet) {
            for await (const source of list_source) {
              actRecordSetItem.BGPlan = await this.get_sumBGPlan(query, planItem.PItemCode, 0, projectRecItem.PrjDetailId, actRecordSetItem.PrjActCode, source.SourceExId, orgData.OrgId);
              actRecordSetItem.BGTferIn = await this.get_sumBGTferIn(query, planItem.PItemCode, 0, projectRecItem.PrjDetailId, actRecordSetItem.PrjActCode, source.SourceExId, orgData.OrgId);
              actRecordSetItem.BGTferOut = await this.get_sumBGTferOut(query, planItem.PItemCode, 0, projectRecItem.PrjDetailId, actRecordSetItem.PrjActCode, source.SourceExId, orgData.OrgId);
              actRecordSetItem.BGHold = await this.get_sumBGHold(query, planItem.PItemCode, 0, projectRecItem.PrjDetailId, actRecordSetItem.PrjActCode, source.SourceExId, orgData.OrgId);
              actRecordSetItem.BGOther = await this.get_sumBGOther(query, planItem.PItemCode, 0, projectRecItem.PrjDetailId, actRecordSetItem.PrjActCode, source.SourceExId, orgData.OrgId);
              actRecordSetItem.BGPlanTfer = actRecordSetItem.BGPlan + actRecordSetItem.BGTferIn - actRecordSetItem.BGTferOut;
              //เอาแค่ตารางหลักพอ
              actRecordSetItem.BGPay = await this.get_sumBGPay(query, planItem.PItemCode, 0, projectRecItem.PrjDetailId, actRecordSetItem.PrjActCode, source.SourceExId, orgData.OrgId);
              actRecordSetItem.ContractIncome = await this.get_mouContractIncome(query,planItem.PItemCode, actRecordSetItem.PrjActCode, source.SourceExId)
              actRecordSetItem.BGPay += Number(actRecordSetItem.BGOther) - Number(actRecordSetItem.ContractIncome)
              actRecordSetItem.BGChain = await this.get_sumBGChain(query, planItem.PItemCode, 0, projectRecItem.PrjDetailId, actRecordSetItem.PrjActCode, source.SourceExId, orgData.OrgId);
              
              if(actRecordSetItem.PrjActCode=='66P02B04'){
                console.log('BGOther',actRecordSetItem.BGOther);
                console.log('ContractIncome',actRecordSetItem.ContractIncome);
                console.log('BGChain',actRecordSetItem.BGChain);
                console.log('BGPay',actRecordSetItem.BGPay);
              }
              //
              actRecordSetItem.BGPayChain =  Number(actRecordSetItem.BGChain) + Number(actRecordSetItem.BGPay);
              actRecordSetItem.BGTotal = 0;
             
              record = {...group, ...planItem, ...project5YItem, ...projectRecItem, ...actRecordSetItem,...source,...orgData };
              result.push(record);
            }
          }
        }
      }
    }
  }

  return result;
};


exports.list_reportD3 = async (query) => {

  let result = [];
  //Plan
  const listPlanItem = await this.list_planItem(query);
  let purchaseAmount = 0
  let contractAmount1 = 0;
  let contractAmount2 = 0;
  //console.log('listPlanItem',listPlanItem);
  for await (let planItem of listPlanItem) {
      
    planItem.Amount = 0;
    planItem.Payment = 0;
    planItem.BGTotal = 0;

    record = {...planItem};
    result.push(record);
    
    //Project 5 Year
    // const project5Year = await this.get_project5Year(query, planItem.LPlanCode);
    // //console.log('project5Year',project5Year);
    // for await(let project5YItem of project5Year) {
    //   project5YItem.Amount = 0;
    //   project5YItem.Payment = 0;
    //   project5YItem.Total = 0;
      
    //   record = {...planItem,...project5YItem };
    //   result.push(record);

      //Project
      const projectRecordSet = await this.get_projectRecordSet(query, planItem.PItemCode);
      console.log('projectRecordSet',projectRecordSet);
      for await(let projectRecItem of projectRecordSet) {
        let orgData = await this.get_organizeFullName(query, projectRecItem.PrjCode);
        projectRecItem.Amount = 0;
        projectRecItem.Payment = 0;
        projectRecItem.Total = 0;
        
        record = {...planItem,...projectRecItem,...orgData };
        result.push(record);

        //Activity
        const actRecordSet = await this.get_actRecordSet(query, projectRecItem.PrjCode);
        //console.log('actRecordSet',actRecordSet);
        for await(let actRecordSetItem of actRecordSet) {
            actRecordSetItem.Amount = 0;
            actRecordSetItem.Payment = 0;
            actRecordSetItem.Total = 0;
          
            record = { ...planItem, ...projectRecItem, ...actRecordSetItem,...orgData };
            result.push(record);

            const purchaseList = await this.get_Purchase(query, actRecordSetItem.PrjActCode);
            //console.log('purchaseList',purchaseList);
           
            for await(let item of purchaseList) {
                //if(item.FormCode=='FP001')  item.Amount = parseFloat(item.ProcureTotalAmount);
                //else item.Amount = parseFloat(item.ValueInPlan);
                item.BGChain = parseFloat(item.ProcureTotalAmount || 0)
                if(item.FormCode=='FP001') {
                  item.BGChain = parseFloat(item.ValueInPlan || 0)
                  item.Topic = item.ProcureTopic
                }
                if(item.FormCode=='FP002') {
                  //ดึงสำนักจากอีกที่นึง
                  //orgData = await this.get_organizeFP002(query, item.DocCode);
                  console.log('contractList',orgData);
                } 
                item.Amount = parseFloat(item.ProcureTotalAmount || 0);
                purchaseAmount += item.Amount;
                item.Payment = parseFloat(item.Budget || 0);
                item.Total = (Number(item.Amount || 0)-Number(item.Payment||0));
                item.TotalAmount = parseFloat(item.ProcureTotalAmount || 0);
                item.Type = 'Purchase';
                item.TypeName = 'ใบสั่งซื้อ';
                
                record = { ...planItem, ...projectRecItem, ...actRecordSetItem, ...item,...orgData };
                result.push(record);
            }

            for await(let i of [1,2]){
              let contractList = await this.get_ContractType(query, actRecordSetItem.PrjActCode, i);
              //console.log('contractList',contractList);
              for await(let item of contractList) {
                  item.BGChain = parseFloat(item.ContractTotalAmount || 0)
                  if(item.FormCode=='FP001') {
                    item.PrjActCode = item.PrjActCodeCheckIn
                    item.BGChain = parseFloat(item.ValueInPlan || 0)
                    item.Topic = item.ContractTopic
                  } 

                  if(item.FormCode=='FP002') {
                    //ดึงสำนักจากอีกที่นึง
                    //orgData = await this.get_organizeFP002(query, item.DocCode);
                    console.log('contractList',orgData);
                  } 

                  item.Amount = parseFloat(item.Budget || 0);
                  
                  item.Payment = parseFloat(item.BGPayment || 0);
                  item.Total = (Number(item.Amount || 0)-Number(item.Payment || 0));
                  item.TotalAmount = parseFloat(item.ContractTotalAmount || 0);
                  if(i==1){
                    item.Type = 'Contract';
                    item.TypeName = 'สัญญา';
                    contractAmount1 += item.Amount
                  } else {
                    item.Type = 'Employment';
                    item.TypeName = 'ใบสั่งจ้าง';
                    contractAmount2 += item.Amount
                  }
                  
                  record = { ...planItem, ...projectRecItem, ...actRecordSetItem, ...item,...orgData };
                  result.push(record);
              }
            }
            //FP004 (tblfinance_bg_pay)
            // const fp004List = await this.get_FP004(query, actRecordSetItem.PrjActCode);
            // for await(let item of fp004List) {
            //   item.Payment = parseFloat(item.BGPayment || 0);
            //   record = { ...planItem, ...project5YItem, ...projectRecItem, ...actRecordSetItem, ...item,...orgData };
            //   result.push(record);
            // }
        }
      }
    //}
  }
  console.log('contractAmount สัญญา',contractAmount1);
  console.log('contractAmount ใบสั่งจ้าง',contractAmount2);
  console.log('purchaseAmount',purchaseAmount);

  return result;
};

exports.list_reportD5 = async (query) => {

  let result = [];
  //Plan
  const listPlanItem = await this.list_planItem(query);
  //console.log('listPlanItem',listPlanItem);
  for await (let planItem of listPlanItem) {
      
    planItem.Budget = 0;
    planItem.BGRepay = 0;
    planItem.BgOperate = 0;
    planItem.Payment = 0;
    planItem.Total = 0;

    record = {...planItem};
    result.push(record);
    
    //Project 5 Year
    const project5Year = await this.get_project5Year(query, planItem.LPlanCode);
    //console.log('project5Year',project5Year);
    for await(let project5YItem of project5Year) {
      project5YItem.Budget = 0;
      project5YItem.BGRepay = 0;
      project5YItem.BgOperate = 0;
      project5YItem.Payment = 0;
      project5YItem.Total = 0;
      
      record = {...planItem,...project5YItem };
      result.push(record);

      //Project
      const projectRecordSet = await this.get_projectRecordSet(query, planItem.PItemCode,project5YItem.LPrjCode);
      //console.log('projectRecordSet',projectRecordSet);
      for await(let projectRecItem of projectRecordSet) {
        const orgData = await this.get_organizeFullName(query, projectRecItem.PrjCode);
        projectRecItem.Budget = 0;
        projectRecItem.BGRepay = 0;
        projectRecItem.BgOperate = 0;
        projectRecItem.Payment = 0;
        projectRecItem.Total = 0;
        
        record = {...planItem,...project5YItem,...projectRecItem,...orgData };
        result.push(record);

        //Activity
        const actRecordSet = await this.get_actRecordSet(query, projectRecItem.PrjCode);
        //console.log('actRecordSet',actRecordSet);
        for await(let actRecordSetItem of actRecordSet) {
            actRecordSetItem.Budget = 0;
            actRecordSetItem.BGRepay = 0;
            actRecordSetItem.BgOperate = 0;
            projectRecItem.Payment = 0;
            projectRecItem.Total = 0;
          
            record = { ...planItem, ...project5YItem, ...projectRecItem, ...actRecordSetItem,...orgData };
            result.push(record);

            const mouList = await this.get_Mou(query, actRecordSetItem.PrjActCode);
            //console.log('purchaseList',purchaseList);
            for await(let item of mouList) {
                record = { ...planItem, ...project5YItem, ...projectRecItem, ...actRecordSetItem, ...item,...orgData };
                result.push(record);
            }
        }
      }
    }
  }

  return result;
};

exports.list_reportD7 = async (query) => {

  let result = [];
  query.ParentId = 0;
  console.log('query',query);

  const bookBankGroup = await this.get_bookBankGroup();
  for await (let bookBank of bookBankGroup) {
    console.log('bookBank',bookBank);
    const listParent = await this.get_acChart({ ParentId : 0},bookBank.PrjCode);

    for await (let level0 of listParent) {
      query.ParentId = level0.AcChartId
      level0.cumCredit = 0
      level0.cumDebit = 0
      //level0.peCredit = 0
      //level0.peDebit = 0
      level0.bbCredit = 0
      level0.bbDebit = 0


      const listLevel1 = await this.get_acChart(query,bookBank.PrjCode);
      for await (let level1 of listLevel1) {
        query.ParentId = level1.AcChartId
        level1.cumCredit = 0
        level1.cumDebit = 0
        //level1.peCredit = 0
        //level1.peDebit = 0
        level1.bbCredit = 0
        level1.bbDebit = 0

        let total = await this.get_sumAcDetail(query, level1.AcChartId,bookBank.PrjCode)
        let brightForward = await this.get_sumBroughtForward(query, level1.AcChartId,bookBank.PrjCode)
        let periodCD = total[0].total || 0;
        let brightForwardCD = brightForward[0].total || 0;
        
        const listLevel2 = await this.get_acChart(query,bookBank.PrjCode);
        if(listLevel2 && listLevel2.length>0){
          for await (let level2 of listLevel2) {
            query.ParentId = level2.AcChartId
            level2.cumCredit = 0
            level2.cumDebit = 0
            //level2.peCredit = 0
            //level2.peDebit = 0
            level2.bbCredit = 0
            level2.bbDebit = 0

            let total = await this.get_sumAcDetail(query, level2.AcChartId,bookBank.PrjCode)
            let brightForward = await this.get_sumBroughtForward(query, level2.AcChartId,bookBank.PrjCode)
            let periodCD = total[0].total || 0;
            let brightForwardCD = brightForward[0].total || 0;

            const listLevel3 = await this.get_acChart(query,bookBank.PrjCode);
            if(listLevel3 && listLevel3.length>0){
              for await (let level3 of listLevel3) {
                query.ParentId = level3.AcChartId
                level3.cumCredit = 0
                level3.cumDebit = 0
                //level3.peCredit = 0
                //level3.peDebit = 0
                level3.bbCredit = 0
                level3.bbDebit = 0

                let total = await this.get_sumAcDetail(query, level3.AcChartId,bookBank.PrjCode)
                let brightForward = await this.get_sumBroughtForward(query, level3.AcChartId,bookBank.PrjCode)
                let periodCD = total[0].total || 0;
                let brightForwardCD = brightForward[0].total || 0;

                const listLevel4 = await this.get_acChart(query,bookBank.PrjCode);
                if(listLevel4 && listLevel4.length>0){
                  for await (let level4 of listLevel4) {
                  
                    //console.log('#Level 4 level4',level4);
                    //console.log('#Level 4 level4.Credit',level4.Credit);
                    //console.log('#Level 4 level4.Debit',level4.Debit);
      
                    level4.Credit = Number(level4.Credit || 0)
                    level4.Debit = Number(level4.Debit || 0)
      
                    let total = await this.get_sumAcDetail(query, level4.AcChartId,bookBank.PrjCode)
                    let brightForward = await this.get_sumBroughtForward(query, level4.AcChartId,bookBank.PrjCode)
                    //if(total){
                      //console.log('#Level 4 periodCD',periodCD);
                      let periodCD = total[0].total || 0;
                      let brightForwardCD = brightForward[0].total || 0;
      
                      const inD = periodCD > 0 ? periodCD : 0;
                      const inC = periodCD <= 0 ? 0 - periodCD : 0;
                      level4.peCredit = Number(Math.abs(level4.Credit || 0))
                      level4.peDebit = Number(level4.Debit || 0)
                     
                      if(brightForwardCD<0){
                        //level4.cumCredit = Number(Math.abs(periodCD || 0))
                        //level4.cumDebit = 0 
                        level4.bbCredit = Number(Math.abs(brightForwardCD || 0))
                        level4.bbDebit = 0 
                      } else {
                        //level4.cumCredit = 0
                        //level4.cumDebit = Number(Math.abs(periodCD))
                        level4.bbCredit = 0 
                        level4.bbDebit = Number(brightForwardCD || 0)
                      }
                      const sumCum = level4.bbDebit+level4.peDebit-level4.bbCredit-level4.peCredit
                      if(sumCum>0) {
                        level4.cumDebit = Number(Math.abs(sumCum.toFixed(2)))
                        level4.cumCredit = 0
                      } else {
                        level4.cumDebit = 0
                        level4.cumCredit = Number(Math.abs(sumCum.toFixed(2)))
                      }
                      //console.log('#Level 4 inD',inD);
                      //console.log('#Level 4 inC',inC);
      
                      level4 = {...level4,...bookBank}
                      result.push(level4)
                      level3.Credit +=  Number(level4.Credit || 0)
                      level3.Debit +=  Number(level4.Debit || 0)
                      //level3.cumCredit +=  Number(level4.cumCredit || 0)
                      //level3.cumDebit +=  Number(level4.cumDebit || 0)
                      //level3.peCredit +=  Number(level4.peCredit || 0)
                      //level3.peDebit +=  Number(level4.peDebit || 0)
                      level3.bbCredit +=  Number(level4.bbCredit || 0)
                      level3.bbDebit +=  Number(level4.bbDebit || 0)
                    //}
                  }
                } else {
                  if(periodCD<0){
                    level3.peCredit = Number(Math.abs(periodCD || 0))
                    level3.peDebit = 0 
                  } else {
                    level3.peCredit = 0 
                    level3.peDebit = Number(periodCD || 0)
                  }
                  if(brightForwardCD<0){
                    level3.bbCredit = Number(Math.abs(brightForwardCD || 0))
                    level3.bbDebit = 0 
                  } else {
                    level3.bbCredit = 0 
                    level3.bbDebit = Number(brightForwardCD || 0)
                  }
                }
                
                level3.Credit = Number(level3.Credit || 0)
                level3.Debit = Number(level3.Debit || 0)
                level3.peCredit =  Number(level3.Credit || 0)
                level3.peDebit =  Number(level3.Debit || 0)
                const sumCum = level3.peDebit+level3.bbDebit-level3.peCredit-level3.bbCredit
                if(sumCum>0) {
                  level3.cumDebit = Number(Math.abs(sumCum.toFixed(2)))
                  level3.cumCredit = 0
                } else {
                  level3.cumDebit = 0
                  level3.cumCredit = Number(Math.abs(sumCum.toFixed(2)))
                }
                level3 = {...level3,...bookBank}
                result.push(level3)

                level2.Credit +=  Number(level3.Credit || 0)
                level2.Debit +=  Number(level3.Debit || 0)
                //level2.cumCredit +=  Number(level3.cumCredit || 0)
                //level2.cumDebit +=  Number(level3.cumDebit || 0)
                //level2.peCredit +=  Number(level3.peCredit || 0)
                //level2.peDebit +=  Number(level3.peDebit || 0)
                level2.bbCredit +=  Number(level3.bbCredit || 0)
                level2.bbDebit +=  Number(level3.bbDebit || 0)
              }
            } else {
              if(periodCD<0){
                level2.peCredit = Number(Math.abs(periodCD || 0))
                level2.peDebit = 0 
              } else {
                level2.peCredit = 0 
                level2.peDebit = Number(periodCD || 0)
              }
              if(brightForwardCD<0){
                level2.bbCredit = Number(Math.abs(brightForwardCD || 0))
                level2.bbDebit = 0 
              } else {
                level2.bbCredit = 0 
                level2.bbDebit = Number(brightForwardCD || 0)
              }
            }
            level2.Credit = Number(level2.Credit || 0)
            level2.Debit = Number(level2.Debit || 0)
            level2.peCredit =  Number(level2.Credit || 0)
            level2.peDebit =  Number(level2.Debit || 0)
            const sumCum = level2.peDebit+level2.bbDebit-level2.peCredit-level2.bbCredit
            if(sumCum>0) {
              level2.cumDebit = Number(Math.abs(sumCum.toFixed(2)))
              level2.cumCredit = 0
            } else {
              level2.cumDebit = 0
              level2.cumCredit = Number(Math.abs(sumCum.toFixed(2)))
            }
            level2 = {...level2,...bookBank}
            result.push(level2)
            
            level1.Credit +=  Number(level2.Credit || 0)
            level1.Debit +=  Number(level2.Debit || 0)
            //level1.cumCredit +=  Number(level2.cumCredit || 0)
            //level1.cumDebit +=  Number(level2.cumDebit || 0)
            level1.peCredit +=  Number(level2.peCredit || 0)
            level1.peDebit +=  Number(level2.peDebit || 0)
            level1.bbCredit +=  Number(level2.bbCredit || 0)
            level1.bbDebit +=  Number(level2.bbDebit || 0)
          }
        } else {
          if(periodCD<0){
            level1.peCredit = Number(Math.abs(periodCD || 0))
            level1.peDebit = 0 
          } else {
            level1.peCredit = 0 
            level1.peDebit = Number(periodCD || 0)
          }
          if(brightForwardCD<0){
            level1.bbCredit = Number(Math.abs(brightForwardCD || 0))
            level1.bbDebit = 0 
          } else {
            level1.bbCredit = 0 
            level1.bbDebit = Number(brightForwardCD || 0)
          }
        }
        level1.Credit = Number(level1.Credit || 0)
        level1.Debit = Number(level1.Debit || 0)
        level1.peCredit =  Number(level1.Credit || 0)
        level1.peDebit =  Number(level1.Debit || 0)
        const sumCum = level1.peDebit+level1.bbDebit-level1.peCredit-level1.bbCredit
        if(sumCum>0) {
          level1.cumDebit = Number(Math.abs(sumCum.toFixed(2)))
          level1.cumCredit = 0
        } else {
          level1.cumDebit = 0
          level1.cumCredit = Number(Math.abs(sumCum.toFixed(2)))
        }
        level1 = {...level1,...bookBank}
        result.push(level1)

        level0.Credit += Number(level1.Credit || 0)
        level0.Debit +=  Number(level1.Debit || 0)
        //level0.cumCredit +=  Number(level1.cumCredit || 0)
        //level0.cumDebit +=  Number(level1.cumDebit || 0)
        level0.peCredit +=  Number(level1.peCredit || 0)
        level0.peDebit +=  Number(level1.peDebit || 0)
        level0.bbCredit +=  Number(level1.bbCredit || 0)
        level0.bbDebit +=  Number(level1.bbDebit || 0)
      }

      level0.peCredit =  Number(level0.Credit || 0)
      level0.peDebit =  Number(level0.Debit || 0)
      const sumCum = level0.peDebit+level0.bbDebit-level0.peCredit-level0.bbCredit
      if(sumCum>0) {
        level0.cumDebit = Number(Math.abs(sumCum.toFixed(2)))
        level0.cumCredit = 0
      } else {
        level0.cumDebit = 0
        level0.cumCredit = Number(Math.abs(sumCum.toFixed(2)))
      }
      level0 = {...level0,...bookBank}
      result.push(level0)
    }
  }
  
  return result;
};

/************** REPORT D8 *****************/
exports.list_reportD8 = async (query) => {

  const support_Chain = await this.get_Support_Chain(query);
  const support_Pay = await this.get_Support_Pay(query);
  const support_Income = await this.get_Support_Income(query);

  const saralyPerson_Chain = await this.get_SaralyPerson_Chain(query, true);
  const saralyContractPerson_Chain = await this.get_SaralyContractPerson_Chain(query);
  const saralyContractPerson_Pay = await this.get_SaralyContractPerson_Pay(query);
  const meetingOperation_Chain = await this.get_MeetingOperation_Chain(query);
  const meetingOperation_Pay = await this.get_MeetingOperation_Pay(query);
  const meetingOperation_PayRef = await this.get_MeetingOperation_PayRef(query);
  const other_Chain = await this.get_Other_Chain(query);
  const other_Pay = await this.get_Other_Pay(query);
  const other_PettyCrash = await this.get_Other_PettyCrash(query);
  const other_PettyCrashRef = await this.get_Other_PettyCrashRef(query);
  //const other_FF008 = await this.get_SaralyPerson_Chain(query, false);
  const FP004_Pay = await this.get_FP004_Pay(query);

  const contract_Chain = await this.get_Contract_Chain(query);
  const purchase_Chain = await this.get_Purchase_Chain(query);
  const FP006_pay = await this.get_FP006_Pay(query);
  //const FP004_pay = await this.get_FP004_Pay(query);
  
  return [
    ...support_Chain,
    ...support_Pay,
    ...support_Income,
    ...saralyPerson_Chain,
    ...saralyContractPerson_Chain,
    ...saralyContractPerson_Pay,
    ...meetingOperation_Chain,
    ...meetingOperation_Pay,
    ...meetingOperation_PayRef,
    ...other_Chain,
    ...other_Pay,
    ...other_PettyCrash,
    ...other_PettyCrashRef,
    ...FP004_Pay,
    ...contract_Chain,
    ...purchase_Chain,
    ...FP006_pay,
    //...FP004_pay,
  ]
}

exports.get_SaralyPerson_Chain = async (query ,LoadDataFF008) => {
  let where = [];
  where.push("pay.CancelStatusxx='N'");
  where.push(`pay.BgtYear='${query.BgtYear}'`);
  where.push("pay.FormCode='FF008'");
  if(LoadDataFF008)where.push("doc.LoadDataFF008=1"); 
  else where.push("doc.LoadDataFF008!=1"); 

  let sql = `SELECT
	    SUM(SumBGPay) AS Value,pay.SourceExId,source.SourceExName,source.SourceCode,
      IF(${LoadDataFF008},'SaralyPerson_Chain','Other_FF008') AS Type
    FROM 
      tblfinance_bg_pay AS pay
    LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCode
    LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = pay.SourceExId
    WHERE ${where.join(" AND ")}
    GROUP BY pay.SourceExId,source.SourceExName,source.SourceCode`;    
  //console.log(sql);

  return await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_SaralyContractPerson_Chain = async (query ) => {
  let where = [];
  where.push("(cha.FormCode IS NULL OR cha.FormCode='' AND con.CloseStatus='N')");
  where.push(`cha.BgtYear='${query.BgtYear}'`);
  where.push("(cha.ContractNo_FH002 IS NOT NULL AND cha.ContractNo_FH002!='')");

  let sql = `SELECT
      SUM(SumBGChain) AS Value,doc.SourceExId,source.SourceExName,source.SourceCode,'SaralyContractPerson_Chain' AS Type
    FROM 
      tblfinance_bg_chain AS cha 
    LEFT JOIN tblpersonal_contract con ON con.ContractId = cha.ContractNo_FH002 
    LEFT JOIN tblfinance_doccode doc ON doc.DocCode = cha.DocCodeRefer
    LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = doc.SourceExId
    WHERE ${where.join(" AND ")}
    GROUP BY doc.SourceExId,source.SourceExName,source.SourceCode`;    
      //console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_SaralyContractPerson_Pay = async (query ) => {
  let where = [];
  where.push("doc.FormCode='FH003'");
  where.push(`pay.BgtYear='${query.BgtYear}'`);
  where.push("pay.CancelStatusxx='N'");

  let sql = ` SELECT 
      SUM(SumBGPay) AS Value,pay.SourceExId,source.SourceExName,source.SourceCode,'SaralyContractPerson_Pay' AS Type
    FROM 
      tblfinance_bg_pay AS pay 
    LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCode 
    LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = pay.SourceExId
    WHERE  ${where.join(" AND ")}
    GROUP BY pay.SourceExId,source.SourceExName,source.SourceCode`;    
      //console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_Support_Chain  = async (query ) => {
  let where = [];
  where.push("cha.FormCode IN ('FC002')");
  where.push(`doc.BgtYear='${query.BgtYear}'`);
  where.push("doc.DocStatusId NOT IN (8,9,13) AND mp.CloseStatus!='Y' AND mp.CancelStatus!='Y'  ");

  let sql = `SELECT
      SUM(SumBGChain) AS Value,cha.SourceExId,source.SourceExName,source.SourceCode,'Support_Chain' AS Type
    FROM 
      tblfinance_bg_chain AS cha 
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = cha.DocCode
      LEFT JOIN tblmou_project_doccode mou ON mou.DocCode=doc.DocCode
      LEFT JOIN (
        SELECT MouCode,MAX(VersionNo),ContractNo,tblmou_project.CloseStatus,tblmou_project.CancelStatus
        FROM tblmou_project
        GROUP BY ContractNo
    ) AS mp ON mp.MouCode = mou.MouCode  
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = cha.SourceExId
    WHERE ${where.join(" AND ")}
    GROUP BY cha.SourceExId,source.SourceExName,source.SourceCode`;    
      console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_Support_Pay  = async (query ) => {
  let where = [];
  where.push("pay.FormCode IN ('FC003') ");
  where.push("pay.CancelStatusxx='N' ");
  where.push(`pay.BgtYear='${query.BgtYear}'`);
  where.push("doc.DocStatusId NOT IN (8,13)");

  let sql = `SELECT 
      SUM(SumBGPay) AS Value,pay.SourceExId,source.SourceExName,source.SourceCode,'Support_Pay' AS Type
    FROM 
      tblfinance_bg_pay AS pay  
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCodeRefer  
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = pay.SourceExId
    WHERE ${where.join(" AND ")}
    GROUP BY pay.SourceExId,source.SourceExName,source.SourceCode`;    
      console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_Support_Income = async (query) => {
  let where = [];
  where.push(` bfi.DeleteStatus='N'
        AND p.DeleteStatus='N'
        AND p.ActiveStatus='Y'
        AND fd.UseStatus='Y'
        AND p.BgtYear='${query.BgtYear}'
        AND bpd.ActiveStatus='Y' `)
  let sql = `SELECT SUM(IncomeValue) AS Value,bfi.SourceExId,source.SourceExName,source.SourceCode,'Support_Income' AS Type
    FROM tblbudget_finance_income bfi
    INNER JOIN tblmou_project p ON p.ContractNo=bfi.ReferCode
    INNER JOIN tblmou_project_doccode pd ON p.MouCode=pd.MouCode
    INNER JOIN tblfinance_doccode fd ON pd.DocCode=fd.DocCode AND fd.DocStatusId NOT IN (8,13)
    LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = bfi.SourceExId
    LEFT JOIN tblbudget_project_activity bpa ON bpa.PrjActCode=fd.PrjActCode
    LEFT JOIN tblbudget_project_detail bpd ON bpd.PrjDetailId=bpa.PrjDetailId
    INNER JOIN tblbudget_project bp ON bp.PrjId=bpd.PrjId
    LEFT JOIN tblbudget_longterm_plan_project blp ON blp.LPrjCode=bp.LPrjCode
    LEFT JOIN ac_action ac ON ac.IncomeId=bfi.IncomeId AND ac.DeleteStatus='N'
    WHERE ${where.join(' AND ')}
    GROUP BY bfi.SourceExId,source.SourceExName,source.SourceCode`;    
    console.log(sql);
  return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};


exports.get_Contract_Chain  = async (query ) => {
  let where = [];
  where.push(`doc.BgtYear='${query.BgtYear}'`);
  where.push("doc.DocStatusId NOT IN (8,13) AND (con.CloseStatus!='Y'  OR con.CloseStatus IS NULL)  AND cha.ContractId!=0 ");

  let sql = `SELECT
      SUM(SumBGChain) AS Value,IF(con.FormCode='FP001',con.SourceExIdCheckIn,cha.SourceExId) AS SourceExId,
      source.SourceExName,source.SourceCode,'Contract_Chain' AS Type
      FROM 
        tblfinance_bg_chain AS cha 
        LEFT JOIN stock_doc_con_contract con ON con.ContractId = cha.ContractId 
        LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = IF(con.FormCode='FP001',con.SourceExIdCheckIn,cha.SourceExId)
        LEFT JOIN tblfinance_doccode doc ON doc.DocCode = cha.DocCodeRefer 
      WHERE ${where.join(' AND ')}
      GROUP BY IF(con.FormCode='FP001',con.SourceExIdCheckIn,cha.SourceExId),source.SourceExName,source.SourceCode`;    
      console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_Purchase_Chain  = async (query ) => {
  let where = [];
  where.push(`doc.BgtYear='${query.BgtYear}'`);
  where.push("doc.DocStatusId NOT IN (8,13) AND (pur.CloseStatus!='Y' OR pur.CloseStatus IS NULL ) AND cha.ProcureId!=0  ");

  let sql = `SELECT
    SUM(SumBGChain),IF(pur.FormCode='FP001',pur.SourceExIdCheckIn,cha.SourceExId) AS SourceExId,
    source.SourceExName,source.SourceCode,'Purchase_Chain' AS Type
    FROM 
      tblfinance_bg_chain AS cha 
      LEFT JOIN stock_doc_purchase pur ON pur.ProcureId = cha.ProcureId 
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = IF(pur.FormCode='FP001',pur.SourceExIdCheckIn,cha.SourceExId)
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = cha.DocCodeRefer 
    WHERE ${where.join(' AND ')} 
    GROUP BY IF(pur.FormCode='FP001',pur.SourceExIdCheckIn,cha.SourceExId),source.SourceExName,source.SourceCode `;    
      console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_FP006_Pay  = async (query ) => {
  let where = [];
  where.push("pay.FormCode IN ('FP006') ");
  where.push("pay.CancelStatusxx='N' ");
  where.push(`pay.BgtYear='${query.BgtYear}'`);
  where.push("doc.DocStatusId NOT IN (8,13)");

  let sql = `SELECT 
      SUM(SumBGPay) AS Value,pay.SourceExId,source.SourceExName,source.SourceCode,'FP006_Pay' AS Type
    FROM 
      tblfinance_bg_pay AS pay  
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCode  
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = pay.SourceExId
    WHERE ${where.join(" AND ")}
    GROUP BY pay.SourceExId,source.SourceExName,source.SourceCode`;    
      console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_FP004_Pay  = async (query ) => {
  let where = [];
  where.push("pay.FormCode IN ('FP004') ");
  where.push("pay.CancelStatusxx='N' ");
  where.push(`pay.BgtYear='${query.BgtYear}'`);
  where.push("doc.DocStatusId NOT IN (8,13)");

  let sql = `SELECT 
      SUM(SumBGPay) AS Value,pay.SourceExId,source.SourceExName,source.SourceCode,'FP004_Pay' AS Type
    FROM 
      tblfinance_bg_pay AS pay  
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCode 
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = pay.SourceExId
    WHERE ${where.join(" AND ")}
    GROUP BY pay.SourceExId,source.SourceExName,source.SourceCode`;    
      console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};


exports.get_MeetingOperation_Chain  = async (query ) => {
  let where = [];
  where.push("cha.FormCode IN ('FF005A','FF005B')");
  where.push(`cha.BgtYear='${query.BgtYear}'`);
  where.push("doc.eform_type IN (1,2,4,5) AND  doc.DocStatusId NOT IN (8,9,13)");

  let sql = `SELECT
      SUM(SumBGChain) AS Value,cha.SourceExId,source.SourceExName,source.SourceCode,'MeetingOperation_Chain' AS Type
    FROM 
      tblfinance_bg_chain AS cha 
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = cha.DocCode
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = cha.SourceExId
    WHERE ${where.join(" AND ")}
    GROUP BY cha.SourceExId,source.SourceExName,source.SourceCode`;    
      //console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_MeetingOperation_Pay  = async (query ) => {
  let where = [];
  where.push("pay.FormCode IN ('FF006','FF007') ");
  where.push("doc.FormCode IN ('FF005A','FF005B') ");
  where.push("pay.CancelStatusxx='N' ");
  where.push(`pay.BgtYear='${query.BgtYear}'`);
  where.push("doc.eform_type IN (1,2,4,5) AND  doc.DocStatusId NOT IN (8,9,13)");

  let sql = `SELECT 
      SUM(SumBGPay) AS Value,pay.SourceExId,source.SourceExName,source.SourceCode,'MeetingOperation_Pay' AS Type
    FROM 
      tblfinance_bg_pay AS pay  
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCodeRefer  
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = pay.SourceExId
    WHERE ${where.join(" AND ")}
    GROUP BY pay.SourceExId,source.SourceExName,source.SourceCode`;    
      //console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_MeetingOperation_PayRef  = async (query ) => {
  let where = [];
  where.push("pay.FormCode IN ('FF009ref')  ");
  where.push("pay.CancelStatusxx='N'  ");
  where.push(`pay.BgtYear='${query.BgtYear}'`);
  where.push("doc2.FormCode IN ('FF005A','FF005B') ");
  where.push("doc2.eform_type IN (1,2,4,5)  AND doc.DocStatusId NOT IN (8,9,13)");

  let sql = `SELECT 
      SUM(SumBGPay) AS Value,pay.SourceExId,source.SourceExName,source.SourceCode,'MeetingOperation_Pay' AS Type
    FROM 
      tblfinance_bg_pay AS pay  
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCode
      LEFT JOIN tblfinance_doccode doc2 ON doc.DocCodeRefer = doc2.DocCode 
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = pay.SourceExId
    WHERE ${where.join(" AND ")}
    GROUP BY pay.SourceExId,source.SourceExName,source.SourceCode`;    
      //console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_Other_Chain  = async (query ) => {
  let where = [];
  where.push("cha.FormCode IN ('FF005A','FF005B')  ");
  where.push("doc.eform_type IN (3,6) AND  doc.DocStatusId NOT IN (8,9,13)");
  where.push(`cha.BgtYear='${query.BgtYear}'`);

  let sql = `SELECT
      SUM(SumBGChain) AS Value,cha.SourceExId,source.SourceExName,source.SourceCode,'Other_Chain' AS Type
    FROM 
      tblfinance_bg_chain AS cha 
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = cha.DocCode
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = cha.SourceExId
    WHERE ${where.join(" AND ")}
    GROUP BY cha.SourceExId,source.SourceExName,source.SourceCode`;    
      //console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_Other_Pay  = async (query ) => {
  let where = [];
  where.push("pay.FormCode IN ('FF006','FF007')  ");
  where.push("doc.FormCode IN ('FF005A','FF005B') ");
  where.push("doc.eform_type IN (3,6)  AND doc.DocStatusId NOT IN (8,9,13)");
  where.push("pay.CancelStatusxx='N'");
  where.push(`pay.BgtYear='${query.BgtYear}'`);

  let sql = `SELECT 
      SUM(SumBGPay) AS Value,pay.SourceExId,source.SourceExName,source.SourceCode,'Other_Pay' AS Type
    FROM 
      tblfinance_bg_pay AS pay  
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCodeRefer  
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = pay.SourceExId
    WHERE pay.FormCode IN ('FF006','FF007') 
    AND ${where.join(" AND ")}
    GROUP BY pay.SourceExId,source.SourceExName,source.SourceCode`;    
      //console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_Other_PettyCrash  = async (query ) => {
  let where = [];
  where.push("pay.FormCode IN ('FF009')  ");
  where.push("pay.CancelStatusxx='N'  ");
  where.push(`pay.BgtYear='${query.BgtYear}'`);

  let sql = `SELECT 
      SUM(SumBGPay) AS Value,pay.SourceExId,source.SourceExName,source.SourceCode,'Other_PettyCrash' AS Type
    FROM 
      tblfinance_bg_pay AS pay  
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCode  
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = pay.SourceExId
    WHERE ${where.join(" AND ")}
    GROUP BY pay.SourceExId,source.SourceExName,source.SourceCode`;    
      //console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_Other_PettyCrashRef  = async (query ) => {
  let where = [];
  where.push("pay.FormCode IN ('FF009ref')  ");
  where.push("pay.CancelStatusxx='N'  ");
  where.push(`pay.BgtYear='${query.BgtYear}'`);
  where.push("doc2.FormCode IN ('FF005A','FF005B') ");
  where.push("doc2.eform_type IN (3,6)  AND doc.DocStatusId NOT IN (8,9,13)");

  let sql = `SELECT 
      SUM(SumBGPay) AS Value,pay.SourceExId,source.SourceExName,source.SourceCode,'Other_PettyCrash' AS Type
    FROM 
      tblfinance_bg_pay AS pay  
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCode
      LEFT JOIN tblfinance_doccode doc2 ON doc.DocCodeRefer = doc2.DocCode 
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = pay.SourceExId
    WHERE ${where.join(" AND ")}
    GROUP BY pay.SourceExId,source.SourceExName,source.SourceCode`;    
      //console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};

exports.get_Health_Pay  = async (query ) => {
  let where = [];
  where.push("pay.FormCode IN ('FH005')  ");
  where.push("pay.CancelStatusxx='N'");
  where.push(`pay.BgtYear='${query.BgtYear}'`);
  where.push("doc.DocStatusId NOT IN (8,9,13)");

  let sql = `SELECT 
      SUM(SumBGPay) AS Value,pay.SourceExId,source.SourceExName,source.SourceCode,'Other_Pay' AS Type
    FROM 
      tblfinance_bg_pay AS pay  
      LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCode  
      LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = pay.SourceExId
    WHERE  ${where.join(" AND ")}
    GROUP BY pay.SourceExId,source.SourceExName,source.SourceCode`;    
      //console.log(sql);
    return  await sequelize.query(sql , { type: QueryTypes.SELECT });
};



/************** REPORT D9 *****************/
exports.list_reportD9 = async (query) => {
  const brightForwardSP = await this.get_D9SupportProject(query,1);
  const currentSP = await this.get_D9SupportProject(query,2);
  const BudgetSP = await this.get_D9SupportProject(query,3);
  let listSP = mergeArraysByKey(brightForwardSP,currentSP,'Title')
  listSP = mergeArraysByKey(listSP,BudgetSP,'Title')
  //console.log('listSP',listSP);
  listSP = listSP.filter(item => item.Budget &&  Math.round((Number(item.Budget || 0)-Number(item.sumBrightForward || 0)-Number(item.sumCurrent || 0))*100/100)>0)

  const brightForwardCon = await this.get_D9Contract(query,1);
  const currentCon = await this.get_D9Contract(query,2);
  const BudgetCon = await this.get_D9Contract(query,3);
  let listCon = mergeArraysByKey(brightForwardCon,currentCon,'Code','BgtYear')
  listCon = mergeArraysByKey(listCon,BudgetCon,'Code','BgtYear')
  listCon.map(e=>{
    if(e.Code=='สช.บจ. 66-011') {
      console.log('listCon',e,Math.round((Number(e.Budget || 0)-Number(e.sumBrightForward || 0)-Number(e.sumCurrent || 0))*100/100))
    }
  })
  listCon = listCon.filter(item => item.Budget && Math.round((Number(item.Budget || 0)-Number(item.sumBrightForward || 0)-Number(item.sumCurrent || 0))*100/100)>0)

  const brightForwardPur = await this.get_D9Purchase(query,1);
  const currentPur = await this.get_D9Purchase(query,2);
  const BudgetPur = await this.get_D9Purchase(query,3);
  let listPur = mergeArraysByKey(brightForwardPur,currentPur,'Code','BgtYear')
  listPur = mergeArraysByKey(listPur,BudgetPur,'Code','BgtYear')
  listPur = listPur.filter(item => item.Budget && Math.round((Number(item.Budget || 0)-Number(item.sumBrightForward || 0)-Number(item.sumCurrent || 0))*100/100)>0)

  const brightForwardStaff = await this.get_D9StaffContract(query,1);
  const currentStaff = await this.get_D9StaffContract(query,2);
  const BudgetStaff = await this.get_D9StaffContract(query,3);
  let listStaff = mergeArraysByKey(brightForwardStaff,currentStaff,'Title')
  listStaff = mergeArraysByKey(listStaff,BudgetStaff,'Title')
  listStaff = listStaff.filter(item => item.Budget && Math.round((Number(item.Budget || 0)-Number(item.sumBrightForward || 0)-Number(item.sumCurrent || 0))*100/100)>0)

  const brightForwardMeeting = await this.get_D9Meeting(query,1);
  const currentMeeting = await this.get_D9Meeting(query,2);
  const budgetMeeting = await this.get_D9Meeting(query,3);
  let listMeeting = mergeArraysByKey(brightForwardMeeting,currentMeeting,'Title')
  listMeeting = mergeArraysByKey(listMeeting,budgetMeeting,'Title')
  listMeeting = listMeeting.filter(item => item.Budget && Math.round((Number(item.Budget || 0)-Number(item.sumBrightForward || 0)-Number(item.sumCurrent || 0))*100/100)>0)

  const brightForwardOther = await this.get_D9Other(query,1);
  const currentOther = await this.get_D9Other(query,2);
  const BudgetOther = await this.get_D9Other(query,3);
  let listOther = mergeArraysByKey(brightForwardOther,currentOther,'Title')
  listOther = mergeArraysByKey(listOther,BudgetOther,'Title')
  listOther =listOther.filter(item => item.Budget && Math.round((Number(item.Budget || 0)-Number(item.sumBrightForward || 0)-Number(item.sumCurrent || 0))*100/100)>0)

  return [...listSP,...listCon,...listPur,...listStaff,...listMeeting,...listOther]
}

exports.get_D9SupportProject = async (query, Type ) => {
  let condition1 = '';let field = ''
  if(Type==1)  {
    condition1 = ` AND doc.ApproveDate < '${query.BgtYear-544}-10-01' `
    //field = ' (MouBudget-SUM(t1.SumBGPay)) AS sumBrightForward'
    field = 'SUM(t1.SumBGPay) AS sumBrightForward'
  } else if(Type==2)  {
    condition1 = ` AND doc.ApproveDate >= '${query.BgtYear-544}-10-01' `
    field = ' SUM(t1.SumBGPay) AS sumCurrent'
  } else if(Type==3) {
    //condition1 = ` AND doc.ApproveDate < '${query.BgtYear-544}-10-01' `
    field = 'MouBudget AS Budget '
  }
  let sql = `SELECT ${field},
            mou.MouName AS Title,
            mou.ContractNo AS Code,
            mou.BgtYear,
            doc.ApproveDate,
            'Support' AS Type,
            t1.PrjActCode, t1.DocCode,
            s.SourceExName AS SourceName,
            t8.OrgId,t8.OrgName,t8.OrganizeCode,t8.OrgShortName
            FROM tblfinance_bg_pay t1  
            LEFT JOIN tblmou_project_doccode AS moudoc ON moudoc.DocCode=t1.DocCodeRefer
            LEFT JOIN tblfinance_doccode AS doc ON doc.DocCode=t1.DocCode
             AND doc.FormCode='FC003' AND doc.DocStatusId NOT IN (8,13)
            LEFT JOIN tblfinance_doccode AS doc1 ON doc1.DocCode=t1.DocCodeRefer
            LEFT JOIN tblmou_project AS mou ON mou.MouCode=moudoc.MouCode AND mou.ActiveStatus='Y'
            LEFT JOIN tblbudget_init_source_external AS s ON doc.SourceExId=s.SourceExId 
            LEFT JOIN tblstructure_operation t8 ON REPLACE(doc.RQOrganizeId,'O_','') = t8.OrgId
            WHERE mou.MouId IS NOT NULL AND mou.BgtYear<'${query.BgtYear}' AND mou.BgtYear>'2560'
            AND doc1.DocStatusId NOT IN (8,13) ${condition1}
            AND mou.CloseStatus!='Y' AND mou.CancelStatus!='Y' AND mou.DeleteStatus='N' AND mou.ActiveStatus='Y'
            GROUP BY mou.MouCode,mou.ContractNo,mou.BgtYear
            ORDER BY mou.BgtYear DESC`;//console.log(sql)
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  let listData = await Promise.all(
    list.map(async item => {
      const orgData = await this.get_organizeByAct(query, item.PrjActCode);
      const closeType = await this.get_mouClose(item.MouCode);
      return {...item, ...orgData, closeType : closeType}
    })
  )
  return listData.filter(e => !e.closeType)
};

exports.get_mouClose = async (MouCode) => {
  let sql = `SELECT CloseType
  FROM tblmou_project_close 
  WHERE MouCode='${MouCode}'`;
  const org = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return org && org.length ? true : false
};

exports.get_D9Contract = async (query, Type ) => {
  let result = [];
  let condition1 = '';let field = '';let group = 'ContractNo' 
  if(Type==1)  {
    condition1 = ` AND doc.ApproveDate < '${query.BgtYear-544}-10-01' `
    //field = 'IF(ContractTotalAmount-SUM(t1.SumBGPay)<0,0,ContractTotalAmount-SUM(t1.SumBGPay)) AS sumBrightForward'
    field = 'SUM(t1.SumBGPay) AS sumBrightForward'
    group += `,con.BgtYear `
  } else if(Type==2)  {
    condition1 = ` AND doc.ApproveDate >= '${query.BgtYear-544}-10-01'`
    field = ' SUM(t1.SumBGPay) AS sumCurrent'
    group += `,con.BgtYear `
  } else if(Type==3)  {
    //condition1 = ` AND doc.ApproveDate < '${query.BgtYear-544}-10-01' `
    field = ' ContractTotalAmount AS Budget'
    group += `,con.BgtYear `
  }
  let sql = `SELECT  ${field},
          ContractNo AS Code,
          con.ContractTopic AS Title,
          con.BgtYear,
          doc.ApproveDate,
          'Procurement' AS Type,
          t1.PrjActCode, t1.DocCode,
          s.SourceExName AS SourceName
          FROM tblfinance_bg_pay t1  
          LEFT JOIN tblfinance_doccode AS doc ON doc.DocCode=t1.DocCode
          AND doc.FormCode='FP006' 
          LEFT JOIN stock_doc_con_contract AS con ON ContractNo=doc.CodeRefer  AND doc.BgtYear=con.BgtYear 
          LEFT JOIN tblbudget_init_source_external AS s ON doc.SourceExId=s.SourceExId 
          WHERE con.BgtYear<'${query.BgtYear}' AND con.DeleteStatus!='Y'  AND con.BgtYear>'2560'  ${condition1} 
          AND con.CloseStatus='N'
          GROUP BY ${group} 
          ORDER BY con.BgtYear DESC`;
          console.log('#get_D9Contract sql',sql);
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  return Promise.all(
    list.map(async item => {
      const orgData = await this.get_organizeByAct(query, item.PrjActCode);
      return {...item, ...orgData}
    })
  )
};

exports.get_D9Purchase = async (query, Type ) => {
  let result = [];
  let condition1 = '';let field = '';let group = 'ProcureCode' 
  if(Type==1)  {
    condition1 = ` AND doc.ApproveDate < '${query.BgtYear-544}-10-01' `
    //field = ' IF(ProcureTotalAmount-SUM(t1.SumBGPay)<0,0,ProcureTotalAmount-SUM(t1.SumBGPay)) AS sumBrightForward'
    field = ' ProcureTotalAmount AS Budget, SUM(t1.SumBGPay) AS sumBrightForward'
    group += `,pur.BgtYear `
  } else if(Type==2)  {
    condition1 = ` AND doc.ApproveDate >= '${query.BgtYear-544}-10-01' `
    field = ' SUM(t1.SumBGPay) AS sumCurrent'
    group += `,pur.BgtYear `
  } else if(Type==3)  {
    //condition1 = ` AND doc.ApproveDate < '${query.BgtYear-544}-10-01' `
    field = ' ProcureTotalAmount AS Budget'
    group += `,pur.BgtYear `
  }
  let sql = `SELECT ${field},
          ProcureCode AS Code,
          pur.ProcureTopic AS Title,
          pur.BgtYear,
          doc.ApproveDate,
          'Procurement' AS Type,
          t1.PrjActCode, t1.DocCode,
          s.SourceExName AS SourceName
          FROM tblfinance_bg_pay t1  
          LEFT JOIN tblfinance_doccode AS doc ON doc.DocCode=t1.DocCode
          AND doc.FormCode='FP006' 
          LEFT JOIN stock_doc_purchase AS pur ON ProcureCode=doc.CodeRefer  AND doc.BgtYear=pur.BgtYear 
          LEFT JOIN tblbudget_init_source_external AS s ON doc.SourceExId=s.SourceExId 
          WHERE pur.BgtYear<'${query.BgtYear}' AND pur.DeleteStatus!='Y' AND pur.BgtYear>'2560'  ${condition1}
          AND pur.CloseStatus='N'
          GROUP BY ${group} 
          ORDER BY pur.BgtYear DESC`;
          //console.log('#get_D9Purchase sql',sql);
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  return Promise.all(
    list.map(async item => {
      const orgData = await this.get_organizeByAct(query, item.PrjActCode);
      return {...item, ...orgData}
    })
  )
};

exports.get_D9StaffContract = async (query, Type ) => {
  let condition1 = '';let field = ''
  if(Type==1)  {
    condition1 = ` AND doc.ApproveDate < '${query.BgtYear-544}-10-01'  `
    //field = '((SELECT SUM(SumCost) FROM tblpersonal_contract_cost AS cos WHERE con.ContractId=cos.ContractId )-SUM(SumBGPay)) AS sumBrightForward'
    field = 'SUM(SumBGPay) AS sumBrightForward'
  } else if(Type==2)  {
    condition1 = ` AND doc.ApproveDate >= '${query.BgtYear-544}-10-01'  `
    field = ' SUM(SumBGPay) AS sumCurrent'
  } else if(Type==3)  {
    //condition1 = ` AND doc.ApproveDate < '${query.BgtYear-544}-10-01'  `
    field = ' (SELECT SUM(SumCost) FROM tblpersonal_contract_cost AS cos WHERE con.ContractId=cos.ContractId ) AS Budget'
  }
  let sql = `SELECT  ${field},
          con.ContractNo AS Code,
          CONCAT(con.FirstName,' ',con.LastName) AS Title,
          doc.DocCode,
          doc.ApproveDate,
          pay.PrjActCode,
          con.ContractNo,
          con.BgtYear,
          'StaffContract' AS Type,
          pay.SourceExId,source.SourceExName,source.SourceCode
        FROM 
          tblfinance_bg_pay AS pay  
        LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCode AND doc.FormCode='FH003' AND doc.DocStatusId NOT IN (8,13)
        LEFT JOIN tblfinance_doccode doc1 ON doc1.DocCode=pay.DocCodeRefer 
        LEFT JOIN (
          SELECT ContractNo_FH002,DocCodeRefer
          FROM tblfinance_bg_chain GROUP BY ContractNo_FH002,DocCodeRefer
        )  AS cha ON doc1.DocCode=cha.DocCodeRefer 
        LEFT JOIN tblbudget_init_source_external source ON source.SourceExId = pay.SourceExId
        LEFT JOIN tblpersonal_contract con ON con.ContractId = cha.ContractNo_FH002 
        WHERE con.BgtYear<'${query.BgtYear}'  AND con.BgtYear>'2560' 
        AND pay.CancelStatusxx='N' AND (cha.ContractNo_FH002 IS NOT NULL AND cha.ContractNo_FH002!='') 
        AND con.CloseStatus='N' ${condition1} 
        GROUP BY ContractNo_FH002
        ORDER BY con.ContractNo DESC`;
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  return Promise.all(
    list.map(async item => {
      const orgData = await this.get_organizeByAct(query, item.PrjActCode);
      return {...item, ...orgData}
    })
  )
};

exports.get_D9Meeting= async (query, Type ) => {
  let condition1 = '';let field = ''
  if(Type==1)  {
    condition1 = ` AND doc.ApproveDate < '${query.BgtYear-544}-10-01' `
    //field = '(doc.Budget-SUM(SumBGPay)) AS sumBrightForward'
    field = 'SUM(SumBGPay) AS sumBrightForward'
  } else if(Type==2)  {
    condition1 = ` AND doc.ApproveDate >= '${query.BgtYear-544}-10-01' `
    field = 'SUM(SumBGPay) AS sumCurrent'
  } else if(Type==3)  {
    //condition1 = ` AND doc.ApproveDate < '${query.BgtYear-544}-10-01' `
    field = ' doc.Budget AS Budget'
  }
  let sql = `SELECT ${field},
          doc.Title,
          pay.DocCodeRefer AS Code,
          doc.DocCode,
          doc.ApproveDate,
          pay.PrjActCode,
          pay.BgtYear,
          'Meeting' AS Type,
          pay.SourceExId,s.SourceExName,s.SourceCode
        FROM 
          tblfinance_bg_pay AS pay
          LEFT JOIN tblfinance_form_paybr paybr ON paybr.DocCodeRefer = pay.DocCodeRefer AND paybr.ClearLastNo='Y'
          LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCodeRefer AND doc.DocStatusId NOT IN (8,9,13)
          LEFT JOIN tblbudget_init_source_external s ON s.SourceExId = pay.SourceExId
        WHERE doc.eform_type IN (1,2,4,5) AND pay.FormCode IN ('FF006','FF007')AND doc.FormCode IN ('FF005A','FF005B')
        AND paybr.EFormId IS NULL
        AND pay.CancelStatusxx='N' AND pay.BgtYear<'${query.BgtYear}' AND pay.BgtYear>'2560' ${condition1}
        GROUP BY doc.DocCode
        ORDER BY pay.BgtYear DESC`;
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  return Promise.all(
    list.map(async item => {
      const orgData = await this.get_organizeByAct(query, item.PrjActCode);
      return {...item, ...orgData}
    })
  )
};

exports.get_D9Other= async (query, Type ) => {
  let condition1 = '';let field = ''
  if(Type==1)  {
    condition1 = ` AND doc.ApproveDate < '${query.BgtYear-544}-10-01'  `
    //field = ' (doc.Budget-SUM(SumBGPay)) AS sumBrightForward'
    field = 'SUM(SumBGPay) AS sumBrightForward'
  } else if(Type==2)  {
    condition1 = ` AND doc.ApproveDate >= '${query.BgtYear-544}-10-01' `
    field = ' SUM(SumBGPay) AS sumCurrent'
  } else if(Type==3)  {
    //condition1 = ` AND doc.ApproveDate < '${query.BgtYear-544}-10-01'  `
    field = ' doc.Budget AS Budget'
  }
  let sql = `SELECT  ${field},
            doc.Title,
            pay.DocCodeRefer AS Code,
            doc.DocCode,
            doc.ApproveDate,
            pay.PrjActCode,
            pay.BgtYear,
            'Other' AS Type,
            pay.SourceExId,s.SourceExName,s.SourceCode
          FROM 
            tblfinance_bg_pay AS pay
            LEFT JOIN tblfinance_form_paybr paybr ON paybr.DocCodeRefer = pay.DocCodeRefer AND paybr.ClearLastNo='Y'
            LEFT JOIN tblfinance_doccode doc ON doc.DocCode = pay.DocCodeRefer AND doc.DocStatusId NOT IN (8,9,13)
            LEFT JOIN tblbudget_init_source_external s ON s.SourceExId = pay.SourceExId
          WHERE doc.eform_type IN (3,6) AND pay.FormCode IN ('FF006','FF007') AND doc.FormCode IN ('FF005A','FF005B')
          AND pay.CancelStatusxx='N' AND pay.BgtYear<'${query.BgtYear}' AND pay.BgtYear>'2560'
          AND paybr.EFormId IS NULL ${condition1}
          GROUP BY doc.DocCode
          ORDER BY pay.BgtYear DESC`;
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  return Promise.all(
    list.map(async item => {
      const orgData = await this.get_organizeByAct(query, item.PrjActCode);
      return {...item, ...orgData}
    })
  )
};

/*********** Function ************/
const mergeArraysByKey = (array1, array2, key, key2) =>{
  // Create a new array to store the merged objects
  const mergedArray = [];

  // Iterate over the first array
  array1.forEach(item1 => {
    // Find the matching object in the second array based on the key
    let  matchingItem2;
    if(key2)
      matchingItem2 = array2.find(item2 => item2[key] === item1[key] && item2[key2] === item1[key2]);
    else
      matchingItem2 = array2.find(item2 => item2[key] === item1[key]);

    // If a matching object is found, merge the two objects
    if (matchingItem2) {
      const mergedItem = { ...item1, ...matchingItem2 };
      mergedArray.push(mergedItem);
    } else {
      // If no match is found, add the original object from the first array
      mergedArray.push(item1);
    }
  });

  // Add any remaining objects from the second array that don't have a match in the first array
  array2.forEach(item2 => {
    if(key2){
      if (!mergedArray.some(item => item[key] === item2[key] && item[key2] === item2[key2])) {
        mergedArray.push(item2);
      }
    } else {
      if (!mergedArray.some(item => item[key] === item2[key])) {
        mergedArray.push(item2);
      }
    }
  });

  return mergedArray;
}
