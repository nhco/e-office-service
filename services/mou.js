const { Op, QueryTypes } = require('sequelize');
const { sequelize } = require('../models/index')
const db = require("../models");
const ord = require('ord');
const fs = require('fs');
const path = require('path');
const jwt = require('jsonwebtoken');
// const $personal = require('./personal')
const $partner = require('./partner');
const { generate } = require('randomstring');
//var html_to_pdf = require('html-pdf-node'); 
//var pdf = require('html-pdf');
const { erp_get, erp_post } = require('../services/erp')
const $powerBiMou = require('../external-services/power-bi/mou-project')


const style = `<style>
    body {
      font-family: 'saraban';
      font-size: 14px;
      line-height: 1.6;
    }

    .text-center { text-align: center;}
    .text-right { text-align: right;}

    .form-container {
      margin: 50px;
    }

    .top-label { text-align: right; }

    .frame-label { 
      border: 1px solid #000;
      padding: 10px 0;
      width: 25%;
      text-align: center;
      position: relative;
      top: -30px;
      background-color: #fff !important;
      font-weight: bold;
    }

    .content-title { margin-top: 50px; }

    .content-frame { 
      margin-top: 50px; 
      border: 4px double #000;
      border-radius: lpx;
      padding: 5px 20px;
    }

    .frame-body { text-align: justify; }

    .frame-footer { 
      margin: 50px 0 30px;
      display: flex;
    }
    .frame-footer-col { 
      width: 50%;
      padding: 10px 0;
    }
    .sign-row {
      display: flex;
      margin: 10px 0;
      /* justify-content: center; */
      align-items: flex-end;
    }
    .sign-col-underline {
      border-bottom: 1px dotted #000;
      text-align: center;
      width: 65%;
    }

    .button-row {
      padding: 10px 10px 50px;
      display: flex;
      justify-content: center;
    }
    .button-row > .btn { margin: 0 10px;}

    .title-row { display: flex; }
    .title-row > .title-label { width: 15%; }
    .title-row > .title-value { width: 85%; }

    .tb-work-periods { width: 100%; margin-top: 20px;}
    .tb-work-periods th, .tb-work-periods td { border: 1px solid #000; padding: 5px;}
    .tb-work-periods > thead > tr > th { font-weight: bold; text-align: center; vertical-align: middle; }
    .tb-work-periods > tbody > tr > td:first-child { width: 10%; text-align: center; }
    .tb-work-periods > tbody > tr > td:nth-child(2),
    .tb-work-periods > tbody > tr > td:nth-child(4) { width: 20%; text-align: center; }
    .tb-work-periods > tbody > tr > td:nth-child(3),
    .tb-work-periods > tbody > tr > td:nth-child(5),
    .tb-work-periods > tfoot > tr > th:nth-child(3),
    .tb-work-periods > tfoot > tr > th:nth-child(5) { width: 25%; text-align: right; }
    .tb-work-periods > tfoot > tr > th:first-child,
    .tb-work-periods > tfoot > tr > th:nth-child(2),
    .tb-work-periods > tfoot > tr > th:nth-child(4) { font-weight: bold; text-align: center; }
  </style>`
const templateClose = (data) => {
  return `
    <!DOCTYPE html>
    <html lang="th">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>แบบขออนุมัติปิดโครงการ (รข.003)</title>
        ${style}
      </head>
      <body>

        <div class="section-to-print form-container">

          <div class="top-label">${data.form || ''}</div>

          <div class="content-title">
            <div class="title-row">
              <div class="title-label">โครงการ</div>
              <div class="title-value">${data.mou_name || ''}</div>
            </div>
            <div class="title-row">
              <div class="title-label">เลขที่ข้อตกลง</div>
              <div class="title-value">${data.contract_no || ''}</div>
            </div>
            <div class="title-row">
              <div class="title-label">งวดที่</div>
              <div class="title-value">${data.installment || ''}</div>
            </div>
          </div>
          
          <div class="content-frame">

            <div class="frame-label">ส่วนของ สช.</div>
            
            <div class="frame-body">
              <label>ข้อเสนอต่อการขออนุมัติปิดโครงการ</label>
              <span style="text-justify:auto">มินท์บร็อคโคลีเกสต์เฮาส์ป๋าสแควร์บ๋อยเวิร์คปาสกาลหม่านโถวออสซี่เคอร์ฟิวคีตราชันแอ๊บแบ๊วมัฟฟินแบดงั้นโฟล์ครากหญ้าป๊อกมหาอุปราชาสหรัฐบูมสตรอเบอรีปักขคณนาท็อปบูตฟรังก์คอนแทคโทรจิ๊กโก๋บึมก๊วนวิลล์ไลฟ์ครูเสดเอ็นจีโอแมกกาซีนจตุคามจิ๊กโก๋ไฟลท์ปาสคาลคาวบอยมายาคติ บรรพชนซะศิลปากร ไพลินว้อดก้าชาร์จพรีเซ็นเตอร์ สุริยยาตร์ทัวร์รุสโซออร์แกนิก

    เทควันโดคลับ วิดีโอ คอร์รัปชันแฟ็กซ์ออสซี่อุเทน คอนแทคสต็อกดัมพ์ ออร์แกนวอล์ควอฟเฟิล เป่ายิงฉุบร็อคธุรกรรม อะเอสเพรสโซฮวงจุ้ย ออดิชั่นขั้นตอนคาแรคเตอร์มือถือ ดอกเตอร์เฟรชพาสต้าออดิทอเรียม เทรนด์ฮ็อตเอ๋ มาร์จินโต๊ะจีนอุเทนเบลอแบรนด์ คอนเฟิร์มอิเลียดมหภาคเซ็นเตอร์ อินเตอร์ราสเบอร์รี บุญคุณเอ็นทรานซ์แคนยอนนอมินี วอฟเฟิลวิปจูนกรอบรูป สเตชั่นรองรับว้อดก้าฉลุย

    เซอร์วิสเอ็นเตอร์เทนอึ๋ม อัตลักษณ์ปอดแหก ปิยมิตรแจ็กพอตเซฟตี้ติวไตรมาส ตัวเองฮิปโปจิ๊กโก๋โนติส โปรเจ็คท์ อพาร์ทเมนต์ศึกษาศาสตร์หม่านโถวเซลส์แมน มาร์จินอะโยโย่เพนกวิน แซมบ้าไดเอ็ตออดิชั่นอ่อนด้อย ปิยมิตรทริปนาฏยศาลา ฟาสต์ฟู้ดงั้นคอนเซ็ปต์วอลนัต มายองเนสเปปเปอร์มินต์หม่านโถว วินแคมป์สกายไพลิน คาวบอยล็อตโค้ช แทงกั๊ก ท็อปบูตบ๊อบซีอีโอฟาสต์ฟู้ดพลาซ่า ธรรมาพุทธภูมิ

    ยาวีออดิชั่นมาร์ช สต๊อคสุริยยาตร์ตัวตน เป็นไง คลิปแอดมิสชันโรแมนติกเอฟเฟ็กต์นอร์ท แมคเคอเรล แทคติค ทีวีเลกเชอร์ สโตร์มายองเนสรวมมิตรพรีเมียม ไพลินราชานุญาตเซอร์ไพรส์ อิเลียดชาร์จโมหจริตบูม มายองเนส คอร์สฮองเฮาช็อตปิโตรเคมีซิ่ง ซีเนียร์อริยสงฆ์บรรพชนสังโฆโยโย่ สปาก๋ากั่น โกะเอ็นจีโอฉลุยมิวสิคซะ เทรนด์ยิมเนอะเยอร์บีราไฮเอนด์

    อัลบั้มแฟลช มาร์กจิ๊กซอว์สะเด่าฟีดเกจิ ไมค์ เทรนด์เบบี้มิลค์ ศึกษาศาสตร์นายพรานแทงกั๊กฟีด เทรลเลอร์เซาท์แบคโฮฮองเฮาซีอีโอ โซนเลดี้แคนยอนโปสเตอร์จังโก้ ลิมูซีนแรลลีซัพพลายเลกเชอร์สติ๊กเกอร์ ยูโร รัมไฮเอนด์ตุ๊ด หงวนพิซซ่าปาสกาลครัวซองพุทธภูมิ ซังเต ปิยมิตรเซ็นเซอร์ลอร์ดเปปเปอร์มินต์แรงใจ เดี้ยงสเตย์ยอมรับ กรอบรูปรุสโซ โมหจริตแพทยสภาบอยคอตบัสธรรมา</span>
            </div>

            <div class="frame-footer">

              <div class="frame-footer-col">

                <div class="sign-row">
                  <div class="text-right" style="width: 10%">ลงชื่อ</div>
                  <div class="sign-col-underline" >${data.commenter_signed_img}</div>
                  <div>ผู้ให้ข้อเสนอ</div>
                </div>
                <div class="sign-row">
                  <div class="text-right" style="width: 10%">(</div>
                  <div class="sign-col-underline" >${data.coordinator}</div>
                  <div>)</div>
                </div>
                <div class="text-center" style="width: 80%">ผู้ประสานงาน สช.</div>

              </div>
              
              <div class="frame-footer-col">

                <div class="sign-row">
                  <div class="text-right" style="width: 10%">ลงชื่อ</div>
                  <div class="sign-col-underline" style="width: 80%">${data.director_signed_img}</div>
                </div>
                <div class="sign-row">
                  <div class="text-right" style="width: 10%">(</div>
                  <div class="sign-col-underline" >${data.director}</div>
                  <div>)</div>
                </div>
                <div class="text-center" style="width: 90%">${data.director_position_name}</div>

                <div class="sign-row" style="margin-top: 50px;">
                  <div class="text-right" style="width: 10%">ลงชื่อ</div>
                  <div class="sign-col-underline" style="width: 70%">${data.approver_signed_img}</div>
                  <div>ผู้อนุมัติ</div>
                </div>
                <div class="sign-row">
                  <div class="text-right" style="width: 10%">(</div>
                  <div class="sign-col-underline" >${data.approver}</div>
                  <div>)</div>
                </div>
                <div class="text-center" style="width: 90%">${data.approver_position_name}</div>

              </div>
            
            </div>

          </div>

        </div>
      </body>
    </html>`
}
const templateExtend = (data) => {
  let total_orig = data.row.reduce((sum, row) => sum + parseFloat(row.orig_budget), 0);
  let total_current = data.row.reduce((sum, row) => sum + parseFloat(row.current_budget), 0);

  return `
<!DOCTYPE html>
<html lang="th">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>แบบขออนุมัติขยายเวลา (รข.004)</title>
    ${style}
  </head>
  <body>

    <div class="section-to-print form-container">

      <div class="top-label">แบบ. รข 004</div>

      <div class="content-title">
        <div class="title-row">
          <div class="title-label">โครงการ</div>
          <div class="title-value">${data.mou_name}</div>
        </div>
        <div class="title-row">
          <div class="title-label">เลขที่ข้อตกลง</div>
          <div class="title-value">${data.contract_no}</div>
        </div>
        <div class="title-row">
          <div class="title-label">งวดที่</div>
          <div class="title-value">${data.installment}</div>
        </div>
      </div>
      
      <div class="content-frame">

        <div class="frame-label">ส่วนของ สช.</div>
        
        <div class="frame-body">
          <label>ข้อเสนอต่อการขออนุมัติขยายเวลาโครงการ</label>
          <span>$comments</span>

          <div>
            <table class="tb-work-periods">
              <thead>
                <tr>
                  <th rowspan="2">งวดงาน</th>
                  <th colspan="2">เดิม</th>
                  <th colspan="2">ใหม่</th>
                </tr>
                <tr>
                  <th>วันครบกำหนด</th>
                  <th>จำนวนเงิน (บาท)</th>
                  <th>วันครบกำหนด</th>
                  <th>จำนวนเงิน (บาท)</th>
                </tr>
              </thead>
              <tbody>
                ${data.row.map(row => `
                  <tr>
                    <td>งวดที่ ${row.installment}</td>
                    <td>${row.orig_date}</td>
                    <td>${row.orig_budget}</td>
                    <td>${row.current_date}</td>
                    <td>${row.current_budget}</td>
                  </tr>
                `).join('')}
              </tbody>
              <tfoot>
                <tr>
                  <th>รวม</th>
                  <th>-</th>
                  <th>${total_orig}</th>
                  <th>-</th>
                  <th>${total_current}</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>

        <div class="frame-footer">

          <div class="frame-footer-col">

            <div class="sign-row">
              <div class="text-right" style="width: 10%">ลงชื่อ</div>
              <div class="sign-col-underline" >${data.commenter_signed_img}</div>
              <div>ผู้ให้ข้อเสนอ</div>
            </div>
            <div class="sign-row">
              <div class="text-right" style="width: 10%">(</div>
              <div class="sign-col-underline" >${data.coordinator}</div>
              <div>)</div>
            </div>
            <div class="text-center" style="width: 80%">ผู้ประสานงาน สช.</div>

          </div>
          
          <div class="frame-footer-col">

            <div class="sign-row">
              <div class="text-right" style="width: 10%">ลงชื่อ</div>
              <div class="sign-col-underline" style="width: 80%">${data.director_signed_img}</div>
            </div>
            <div class="sign-row">
              <div class="text-right" style="width: 10%">(</div>
              <div class="sign-col-underline" >${data.director}</div>
              <div>)</div>
            </div>
            <div class="text-center" style="width: 90%">${data.director_position_name}</div>

            <div class="sign-row" style="margin-top: 50px;">
              <div class="text-right" style="width: 10%">ลงชื่อ</div>
              <div class="sign-col-underline" style="width: 70%">${data.approver_signed_img}</div>
              <div>ผู้อนุมัติ</div>
            </div>
            <div class="sign-row">
              <div class="text-right" style="width: 10%">(</div>
              <div class="sign-col-underline" >${data.approver}</div>
              <div>)</div>
            </div>
            <div class="text-center" style="width: 90%">${data.approver_position_name}</div>

          </div>
         

        </div>

      </div>

    </div>

  </body>
</html>`
}
const templateProgress = (data) => {
  return `
<!DOCTYPE html>
<html lang="th">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>แบบรายงานความก้าวหน้าโครงการ (รข.001)</title>
    ${style}
  </head>
  <body>

    <div class="section-to-print form-container">

      <div class="top-label">แบบ. รข 001</div>

      <div class="content-title">
        <div class="title-row">
          <div class="title-label">โครงการ</div>
          <div class="title-value">${data.mou_name}</div>
        </div>
        <div class="title-row">
          <div class="title-label">เลขที่ข้อตกลง</div>
          <div class="title-value">${data.contract_no}</div>
        </div>
        <div class="title-row">
          <div class="title-label">งวดที่</div>
          <div class="title-value">${data.installment}</div>
        </div>
      </div>
      
      <div class="content-frame">

        <div class="frame-label">ส่วนของผู้ประสานงาน สช.</div>
        
        <div class="frame-body">
          <label>ข้อคิดเห็นต่อความก้าวหน้าโครงการ / ข้อเสนอเพื่อปรับปรุง</label>
          <span>$comments</span>
        </div>

        <div class="frame-footer">

          <div class="frame-footer-col"></div>

          <div class="frame-footer-col">
            <div class="sign-row">
              <div class="text-right" style="width: 10%">ลงชื่อ</div>
              <div class="sign-col-underline" >${data.commenter_signed_img}</div>
              <div>ผู้ให้ข้อคิดเห็น</div>
            </div>
            <div class="sign-row">
              <div class="text-right" style="width: 10%">(</div>
              <div class="sign-col-underline" >${data.coordinator}</div>
              <div>)</div>
            </div>
            <div class="text-center" style="width: 80%">ผู้ประสานงาน สช.</div>
          </div>

        </div>

      </div>

    </div>

  </body>
</html>`
}

const templateWithdraw = (data) => {
  return `
<!DOCTYPE html>
<html lang="th">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>แบบขออนุมัติเบิกเงินงวด (รข.002)</title>
    ${style}
  </head>
  <body>

    <div class="section-to-print form-container">

      <div class="top-label">แบบ. รข 002</div>

      <div class="content-title">
        <div class="title-row">
          <div class="title-label">โครงการ</div>
          <div class="title-value">${data.mou_name}</div>
        </div>
        <div class="title-row">
          <div class="title-label">เลขที่ข้อตกลง</div>
          <div class="title-value">${data.contract_no}</div>
        </div>
        <div class="title-row">
          <div class="title-label">งวดที่</div>
          <div class="title-value">${data.installment}</div>
        </div>
      </div>
      
      <div class="content-frame">

        <div class="frame-label">ส่วนของ สช.</div>
        
        <div class="frame-body">
          <label>ข้อเสนอต่อการขออนุมัติเบิกเงิน</label>
          <span>$comments</span>
        </div>

        <div class="frame-footer">

          <div class="frame-footer-col">

            <div class="sign-row">
              <div class="text-right" style="width: 10%">ลงชื่อ</div>
              <div class="sign-col-underline" >${data.commenter_signed_img}</div>
              <div>ผู้ให้ข้อเสนอ</div>
            </div>
            <div class="sign-row">
              <div class="text-right" style="width: 10%">(</div>
              <div class="sign-col-underline" >${data.coordinator}</div>
              <div>)</div>
            </div>
            <div class="text-center" style="width: 80%">ผู้ประสานงาน สช.</div>

          </div>
          
          <div class="frame-footer-col">

            <div class="sign-row">
              <div class="text-right" style="width: 10%">ลงชื่อ</div>
              <div class="sign-col-underline" style="width: 80%">${data.director_signed_img}</div>
            </div>
            <div class="sign-row">
              <div class="text-right" style="width: 10%">(</div>
              <div class="sign-col-underline" >${data.director}</div>
              <div>)</div>
            </div>
            <div class="text-center" style="width: 90%">${data.director_position_name}</div>

            <div class="sign-row" style="margin-top: 50px;">
              <div class="text-right" style="width: 10%">ลงชื่อ</div>
              <div class="sign-col-underline" style="width: 70%">${data.approver_signed_img}</div>
              <div>ผู้อนุมัติ</div>
            </div>
            <div class="sign-row">
              <div class="text-right" style="width: 10%">(</div>
              <div class="sign-col-underline" >${data.approver}</div>
              <div>)</div>
            </div>
            <div class="text-center" style="width: 90%">${data.approver_position_name}</div>

          </div>
         

        </div>

      </div>

    </div>
    
  </body>
</html>`
}
const outputFormat = data => {

  data.EndExtendedDate = data.VersionNo == '1' ? null : data.CurrentEndDate;

  const formatFields = {
    MouName : 'name',
    ContractNo : 'code',
    StartDate : 'start_date',
    EndDate : 'end_date',
    EndExtendedDate : 'end_extended_date'
  };
  Object.entries(formatFields).forEach( ([key, newKey]) => data[newKey] = data[key] );

  data.project_status = data.CloseStatus == 'Y' ? 'CLOSED' : 'IN_PROGRESS';

  return data;
}

const getPersonnelProjects = async IdCard => {
  try {
    let data = [];
    const partnerInfo = await $partner.detailByIDCard(IdCard);
    const {PartnerCode, error_msg} = partnerInfo || {};
    if ( PartnerCode && !error_msg ) {
      const projectCols = [
        'MouId',
        'MouCode',
        'ContractNo',
        'MouBudget',
        'MouName',
        'OrgOwner',
        'CodeOwner',
        'OrgOwnerUnder',
        'CloseStatus',
        'VersionNo',
        'BgtYear',
        'StartDate'
      ];
      const closedCols = ['CloseId'];

      const result = await db.sequelize.query(
        `
        SELECT 
          ${projectCols.map( col => `tblmou_project.${col}`).join(',')}
          , mou_project_orig.EndDate
          , tblmou_project.EndDate as CurrentEndDate
          , ${closedCols.map( col => `tblmou_project_close.${col}`).join(',')}
        FROM tblmou_project
        LEFT JOIN (
          SELECT MouCode, EndDate
          FROM tblmou_project
          WHERE DeleteStatus='N' AND VersionNo='1'
        ) as mou_project_orig ON mou_project_orig.MouCode=tblmou_project.MouCode
        LEFT JOIN (
          SELECT MouId, MAX(CloseId) as CloseId 
          FROM tblmou_project_close
          GROUP BY MouId
        ) as corrected_close ON corrected_close.MouId=tblmou_project.MouId
        LEFT JOIN tblmou_project_close ON tblmou_project_close.CloseId=corrected_close.CloseId AND tblmou_project_close.CloseType<>'C'
        WHERE 
          tblmou_project.DeleteStatus='N' 
          AND tblmou_project.ActiveStatus='Y' 
          AND (
            tblmou_project.OrgOwner=:OrgOwner 
            OR tblmou_project.OrgOwnerUnder=:OrgOwnerUnder 
            OR tblmou_project.CodeOwner=:CodeOwner 
          )
        ORDER BY tblmou_project.BgtYear, tblmou_project.ContractNo
        `,
        { 
          replacements: {
            OrgOwner: PartnerCode,
            OrgOwnerUnder: PartnerCode,
            CodeOwner: PartnerCode,
          },
          type: QueryTypes.SELECT,
          logging: console.log
        });

      data = result.map( row => outputFormat(row) );
    }
    
    return data;
  } catch (error) {
    throw error;
  }
}


const getMouProject = async (query) => {
  let sql = ` SELECT mp.MouId, mp.MouCode,mp.ContractNo,NULL AS TaskId,NULL AS TaskNo FROM tblmou_project_task mpt
      INNER JOIN tblmou_project mp ON mp.MouId=mpt.MouId
      CROSS JOIN (SELECT @row_number := 0) AS rn
      WHERE mpt.DeleteStatus = 'N'
      AND mp.DeleteStatus = 'N'
      AND mp.ActiveStatus = 'Y'`;
  if(query.reference_with=='START_DATE') {
    sql += ` AND mp.StartDate='${query.reference_date}'`;
    sql += ` GROUP BY mp.MouId, MouCode ORDER BY MouId ASC`;
  } else if(query.reference_with=='END_DATE') {
    sql += ` AND mp.EndDate='${query.reference_date}'`;
    sql += ` GROUP BY mp.MouId, MouCode ORDER BY MouId ASC`;
  } else if(query.reference_with=='TASK_DATE') {
    sql = ` SELECT mou.MouId, mou.MouCode,mou.ContractNo,mou.TaskId,mou.TaskNo FROM (SELECT
        mp.MouId,
        mp.MouCode,
        mp.ContractNo,
        mpt.TaskId,
        mpt.TaskDate,
        @row_number := IF(@prev_mou_id = mp.MouId, @row_number + 1, 1) AS TaskNo,
        @prev_mou_id := mp.MouId
    FROM
        tblmou_project_task mpt
        INNER JOIN tblmou_project mp ON mp.MouId = mpt.MouId
        CROSS JOIN (SELECT @row_number := 0, @prev_mou_id := NULL) AS vars
    WHERE
        mpt.DeleteStatus = 'N'
        AND mp.DeleteStatus = 'N'
        AND mp.ActiveStatus = 'Y'
    ORDER BY
        mp.MouId, mpt.TaskId)  AS mou
        WHERE mou.TaskDate='${query.reference_date}' ORDER BY MouId ASC`;
  }

  console.log(sql);
  let list = await sequelize.query(sql, { type: QueryTypes.SELECT });
  //const count = await sequelize.query('SELECT COUNT(*) '+ sql, { type: QueryTypes.SELECT });

  return list
};

/**
 * 
comments', 'comment_by', 'comment_at', 'commenter_sign_ref_id', 'commenter_signed_at', 'commenter_sign_id_card',
        'directed_by', 'director_position_name', 'director_sign_ref_id', 'director_signed_at', 'director_sign_id_card',
        'approved_by', 'approver_position_name', 'approver_sign_ref_id', 'approver_signed_at', 'approver_sign_id_card',
 */
const generatePDF = async (data, res) => {
  let { 
    form , mou_name, contract_no, installment, thaid_token, iam_token, 
    commenter_sign_ref_id,
    director_sign_ref_id,
    approver_sign_ref_id,
    director_position_name,
    approver_position_name,
    row 
  } = data

  if(director_sign_ref_id) {
    const director_signed = await erp_get({form , mou_name, contract_no, installment, row, thaid_token, iam_token, ref : director_sign_ref_id}, 'signature');
    if(director_signed.signature) data.director_signed_img = `<img src="data:image/png;base64,${director_signed.signature}"/>`
    data.director = director_signed.name || ''
  }
  if(commenter_sign_ref_id) {
    const commenter_signed = await erp_get({form , mou_name, contract_no, installment, row, thaid_token, iam_token, ref : commenter_sign_ref_id}, 'signature');
    if(commenter_signed.signature) data.commenter_signed_img = `<img src="data:image/png;base64,${commenter_signed.signature}"/>`
    data.coordinator = commenter_signed.name || ''
  }
  if(approver_sign_ref_id) {
    const approver_signed = await erp_get({form , mou_name, contract_no, installment, row, thaid_token, iam_token, ref : approver_sign_ref_id}, 'signature');
    if(approver_signed.signature) data.approver_signed_img = `<img src="data:image/png;base64,${approver_signed.signature}"/>`
    data.approver = approver_signed.name || ''
  }

  // data.director_signed_section = await erp_get({...data}, 'regulation-section-26');
  // data.commenter_signed_section = await erp_get({...data}, 'regulation-section-26');
  // data.approver_signed_section = await erp_get({...data}, 'regulation-section-26');
  
  //console.log('data',data);

  let options = { format: 'A4' };
    
  let formtemplate = ``
  if(form=='รข.001') formtemplate = await templateProgress(data);
  else if(form=='รข.002') formtemplate = await templateWithdraw(data);
  else if(form=='รข.003') formtemplate = await templateClose(data);
  else if(form=='รข.004') formtemplate = await templateExtend(data);
  //console.log('formtemplateClose',formtemplateClose);

  // pdf.create(formtemplateClose, options).toBuffer(function(err, buffer) {
  //   if (err) return console.log(err);
  //   console.log(buffer); // { filename: '/app/businesscard.pdf' }
     
  //   // Set the response headers to indicate a PDF file
  //   res.setHeader('Content-Type', 'application/pdf');
  //   res.setHeader('Content-Disposition', 'attachment; filename=businesscard.pdf');
  //   res.end(buffer);
  // });

  let file = { content: formtemplate };
  const filename = `${form.replace('.','').replace(' ','')}_${contract_no.replace('.','').replace(' ','')}.pdf`
  const encodedFilename = encodeURIComponent(filename);
  console.log('filename',filename);
  let pdfFilePath = path.join('public/files/pdf/', filename);
  html_to_pdf.generatePdf(file, options).then(async output => {
    try {
      const base64String = output.toString('base64');
      console.log(`PDF Base64 :-`, base64String);
  
      // Send the PDF to the ERP API for signing
      const sign_pdf = await erp_post({
        form,
        mou_name,
        contract_no,
        installment,
        ref: '7af5f0088e76130adedc090e6ba5dbff',
        x: 10,
        y: 0,
        sigpage: 1,
        thaid_token,
        iam_token,
        pdf_file: base64String
      }, 'ca/sign');
  
      console.log(`sign_pdf`, sign_pdf);
  
      if (sign_pdf.pdf_data) {
        const signedPdfBuffer = Buffer.from(sign_pdf.pdf_data, 'base64');
  
        // Encode the filename for Content-Disposition header
        const contentDisposition = `attachment; filename*=UTF-8''${encodedFilename}`;

        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader('Content-Disposition', contentDisposition);
        res.end(signedPdfBuffer);
      } else {
        throw new Error('Signing the PDF failed');
      }
    } catch (error) {
      console.error("Error processing PDF:", error);
      res.status(500).send("Error processing PDF");
    }

    // Save the PDF file
    // fs.writeFile(pdfFilePath, output, async(err) => {
    //   if (err) {
    //     console.error("Error saving PDF:", err);
    //     res.status(500).send("Error generating PDF");
    //     return;
    //   }

    //   // Read the saved PDF file and send it as a response
    //   fs.readFile(pdfFilePath, (err, data) => {
    //     if (err) {
    //       console.error("Error reading PDF file:", err);
    //       res.status(500).send("Error generating PDF");
    //       return;
    //     }

    //     // Set headers and send the PDF
    //     res.setHeader('Content-Type', 'application/pdf');
    //     res.setHeader(`Content-Disposition', 'attachment; filename=${filename}`);
    //     res.end(data);
    //   });
    // });
  }).catch(error => {
    console.error("Error generating PDF:", error);
  });

};

const findPowerBiReportByBgtYear = async ( bgt_year ) => {
  const result = await db.sequelize.query(
    `
    SELECT 
      tblmou_project.MouId as ref_id
      , tblmou_project.MouId as mou_id
      , tblmou_project.MouCode as mou_code
      , tblmou_project.ContractNo as contract_no
      , tblmou_project.MouName as mou_name
      , org_partner.FullName as organization_name
      , IF( tblmou_project.SourceId=2, org_recipient_partner.FullName, recipient_partner.FullName ) as recipient_name
      , tblmou_project.StartDate as start_date
      , tblmou_project.EndDate as end_date
      , project_task.period
      , tblmou_project.MouBudget as budget
      , project_financial.ActualBudget as actual_budget
      , tblfinance_doccode.ProposalID as proposal_id
    FROM tblmou_project
    LEFT JOIN (
      SELECT 
        PartnerCode
        , Under as FullName
      FROM nh_in_partner
      WHERE DeleteStatus='N' AND SourceId='2'
    ) as org_partner ON org_partner.PartnerCode=tblmou_project.CodeOwner AND tblmou_project.SourceId='2'
    LEFT JOIN (
      SELECT 
        nh_in_partner.PartnerCode
        , CONCAT( IFNULL( nh_in_partner_prefix.PtnPrefixTH, '' ), IFNULL( nh_in_partner.PtnFname, '' ), ' ', IFNULL( nh_in_partner.PtnSname, '' ) ) as FullName
      FROM nh_in_partner
      LEFT JOIN nh_in_partner_prefix ON nh_in_partner_prefix.PrefixUid=nh_in_partner.PrefixUid AND nh_in_partner_prefix.DeleteStatus='N'
      WHERE nh_in_partner.DeleteStatus='N' AND nh_in_partner.SourceId='1'
    ) as org_recipient_partner ON org_recipient_partner.PartnerCode=tblmou_project.OrgOwnerUnder AND tblmou_project.SourceId='2'
    LEFT JOIN (
      SELECT 
        nh_in_partner.PartnerCode
        , CONCAT( IFNULL( nh_in_partner_prefix.PtnPrefixTH, '' ), IFNULL( nh_in_partner.PtnFname, '' ), ' ', IFNULL( nh_in_partner.PtnSname, '' ) ) as FullName
      FROM nh_in_partner
      LEFT JOIN nh_in_partner_prefix ON nh_in_partner_prefix.PrefixUid=nh_in_partner.PrefixUid AND nh_in_partner_prefix.DeleteStatus='N'
      WHERE nh_in_partner.DeleteStatus='N' AND nh_in_partner.SourceId='1'
    ) as recipient_partner ON recipient_partner.PartnerCode=tblmou_project.OrgOwner AND tblmou_project.SourceId='1'
    LEFT JOIN (
      SELECT 
        tblmou_project_task.TaskId
        , tblmou_project_task.MouId
        , COUNT(tblmou_project_task.TaskId) as period
      FROM tblmou_project_task
      WHERE tblmou_project_task.DeleteStatus='N'
      GROUP BY tblmou_project_task.MouId
    ) as project_task ON project_task.MouId=tblmou_project.MouId
    LEFT JOIN (
      SELECT 
        tblmou_project_task.TaskId
        , tblmou_project_task.MouId
        , tblfinance_doccode.CodeId
        , tblfinance_doccode.FormCode
        , tblfinance_doccode.Budget
        , SUM( tblfinance_doccode.Budget ) as ActualBudget
      FROM tblmou_project_task
      INNER JOIN tblfinance_doccode ON tblfinance_doccode.TaskId=tblmou_project_task.TaskId AND tblfinance_doccode.FormCode='FC003'
      WHERE tblmou_project_task.DeleteStatus='N'
      GROUP BY tblmou_project_task.MouId
    ) as project_financial ON project_financial.MouId=tblmou_project.MouId
    LEFT JOIN tblmou_project_doccode ON tblmou_project_doccode.MouCode=tblmou_project.MouCode
    LEFT JOIN tblfinance_doccode ON tblfinance_doccode.DocCode=tblmou_project_doccode.DocCode
    WHERE 
      tblmou_project.DeleteStatus='N' 
      AND tblmou_project.ActiveStatus='Y' 
      AND tblmou_project.BgtYear=:BgtYear
    ORDER BY tblmou_project.BgtYear, tblmou_project.ContractNo
    `,
    { 
      replacements: {
        BgtYear: bgt_year,
      },
      type: QueryTypes.SELECT,
      logging: console.log
    }
  );

  return result;
}
const findIssuesByProposalIds = async ( proposalIds ) => {

  return proposalIds
}
const postPowerBiReport = async ( bgt_year, mode ) => {
  const list = await findPowerBiReportByBgtYear(bgt_year);

  // const proposalIds = list.map( r => r.proposal_id ).filter( value => value );
  // const issues = await findIssuesByProposalIds( proposalIds );
  
  let sent = mode;
  if ( mode === 'ACTIVATE' ) {
    // send to power BI report
    const resp = await $powerBiMou.upsertMouProjectResults(list);
    if ( !resp?.success ) throw Error('Failed to send data to Power BI.');
    sent = resp;
  }

  return { sent, total: list.length, list };
}

module.exports = {
  getPersonnelProjects,
  getMouProject,
  generatePDF,
  postPowerBiReport
}