const { Router } = require('express');
//const middlewares = require('../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const reportService = require('../services/report');
const route = Router();

module.exports = (app) => {

    app.use('/report', route);
    app.use(errors());

    route.get('/list_source',
        async function (req, res) {
            console.log('report list');
            const list = await reportService.list_source(req.query);
            res.status(200).json({code : 0, isSuccess : true , count : list.length, data : list })
    })
    route.get('/list_reportD1', 
        celebrate({
            [Segments.QUERY]: Joi.object({
                BgtYear: Joi.string().required(),
                SourceExId: Joi.number().required(),
            })
        }),
        async function (req, res) {
            console.log('report list');
            const list = await reportService.list_reportD1(req.query);
            res.status(200).json({code : 0, isSuccess : true , count : list.length, data : list })
    })

    route.get('/list_reportD3', 
    celebrate({
        [Segments.QUERY]: Joi.object({
            BgtYear: Joi.string().required(),
            SourceExId: Joi.number().required(),
        })
        }),
        async function (req, res) {
            console.log('report list');
            try{
                const list = await reportService.list_reportD3(req.query);
                res.status(200).json({code : 0, isSuccess : true , count : list.length, data : list })
            } catch (err ) {
                console.log('list_reportD3',err)
            }
    })

    route.get('/list_reportD5', 
    celebrate({
        [Segments.QUERY]: Joi.object({
            BgtYear: Joi.string().required(),
            SourceExId: Joi.number().required(),
        })
        }),
        async function (req, res) {
            console.log('report list');
            const list = await reportService.list_reportD5(req.query);
            res.status(200).json({code : 0, isSuccess : true , count : list.length, data : list })
    })

    route.get('/list_reportD7', 
    celebrate({
        [Segments.QUERY]: Joi.object({
            BgtYear: Joi.string().required(),
            SourceExId: Joi.number().required(),
        })
        }),
        async function (req, res) {
            console.log('report list');
            const list = await reportService.list_reportD7(req.query);
            res.status(200).json({code : 0, isSuccess : true , count : list.length, data : list })
    })

    route.get('/list_reportD8', 
    celebrate({
        [Segments.QUERY]: Joi.object({
            BgtYear: Joi.string().required(),
            SourceExId: Joi.number().required(),
        })
        }),
        async function (req, res) {
            console.log('report list');
            try{
                const list = await reportService.list_reportD8(req.query);
                res.status(200).json({code : 0, isSuccess : true , count : list.length, data : list })
            } catch (err ) {
                console.log('list_reportD8',err)
            }
    })
    route.get('/list_reportD9', 
    celebrate({
        [Segments.QUERY]: Joi.object({
            BgtYear: Joi.string().required(),
            SourceExId: Joi.number().required(),
        })
        }),
        async function (req, res) {
            console.log('report list');
            try{
                const list = await reportService.list_reportD9(req.query);
                res.status(200).json({code : 0, isSuccess : true , count : list.length, data : list })
            } catch (err ) {
                console.log('list_reportD9',err)
            }
    })
};