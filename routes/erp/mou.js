const { Router } = require('express');
const { getAuth } = require('../../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const mouService = require('../../services/mou');
const route = Router();

module.exports = (app) => {
  app.use('/erp/mou', route);
  app.use(errors());

  route.get( '/personnel-projects', getAuth,
    celebrate({ 
      [Segments.QUERY]: { IdCard: Joi.string().required() }
    }),
    async (req, res) => {
      const {IdCard} = req.query;
      const data = await mouService.getPersonnelProjects(IdCard);
      res.status(200).json({code : 0, isSuccess : true , data})
    });
};