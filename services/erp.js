const { Op, QueryTypes } = require('sequelize');
const { sequelize } = require('../models/index')
const db = require("../models");
//const jwt_decode = require('jwt-decode')
const crypto = require('crypto');

const axios = require('axios');
const qs = require('qs');


exports.erp_get = async ( params, path) => {
  try {
    //console.log('#erp_get params',params);
    var data = qs.stringify(params);
    //console.log('#erp_get data',data);
    var config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: `${process.env.ERP_URL}${path}`,
      headers: { 
        'Accept': 'application/json',
        'API-KEY' : process.env.ERP_APIKEY
      },
      params: params
    };
    //console.log('config',config);
    
    let result = {};
    await axios(config).then( async (response) => {
      //console.log('#erp_get',response.data);
      result = response.data
    })
    .catch(function (error) {
      //console.log('#erp_get',error);
      result = error
    });
    return result
  } catch {
    return { error: 'OAuth2 Error!' }
  }
};
exports.erp_post = async ( params, path) => {
  try {
    //console.log('#erp_post params',params);
    var data = qs.stringify(params);
    //console.log('#erp_get data',data);
    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `${process.env.ERP_URL}${path}`,
      headers: { 
        'Accept': 'application/json',
        'API-KEY' : process.env.ERP_APIKEY
      },
      data: params
    };
    //console.log('config',config);
    
    let result = {};
    await axios.request(config)
    .then((response) => {
      //console.log('#erp_post',response.data);
      result = response.data
    })
    .catch(function (error) {
      //console.log('#erp_post',error);
      result = error
    });
    return result
  } catch {
    return { error: 'OAuth2 Error!' }
  }
};
