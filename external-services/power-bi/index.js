require('dotenv').config();
const httpRequest = require('../httpRequest');

const {POWER_BI_URL} = process.env;

module.exports = (options={}) => httpRequest({ baseURL: POWER_BI_URL, ...options });