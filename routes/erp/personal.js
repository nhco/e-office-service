const { Router } = require('express');
const { getAuth } = require('../../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const personalService = require('../../services/personal');
const route = Router();

module.exports = (app) => {

    function nocache(req, res, next) {
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.header('Expires', '-1');
        res.header('Pragma', 'no-cache');
        res.header('Access-Control-Allow-Origin', '*');

        /*header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
        header('Access-Control-Allow-Headers: *');*/
        next();
    }

    app.use('/erp/personal', route);
    app.use(errors());


    route.post('/list', getAuth, 
        celebrate({
            [Segments.BODY]: Joi.object({
                page: Joi.number().allow(null),
                limit: Joi.number().allow(null),
                search: Joi.string().allow(null,""),
                sort: Joi.string().allow(null,""),
                order: Joi.string().allow(null,""),
                startDate: Joi.string().allow(null),
                endDate: Joi.string().allow(null),
            })
        }),
        async function (req, res) {
            console.log('personal list');
            req.body.currentUser = req.currentUser;
            const list = await personalService.list(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : list.list , total : list.count[0].count,  })
    })

     route.get('/detail/:PersonalId', getAuth, async function (req, res) {
        console.log('personal detail');
        const detail = await personalService.detailByID(req.params.PersonalId,  req.currentUser);
        if(detail.error_msg) res.status(200).json({code : 0, isSuccess : false , error_msg : detail.error_msg })
        else res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.get('/detailByIDCard/:IDCard', getAuth, async function (req, res) {
        const detail = await personalService.detailByIDCard(req.params.IDCard,  req.currentUser);
        if(detail.error_msg) res.status(200).json({code : 0, isSuccess : false , error_msg : detail.error_msg })
        else res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.get('/list_prefix', getAuth, async function (req, res) {
        const list = await personalService.getPrefix();
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.get('/list_position', getAuth, async function (req, res) {
        const list = await personalService.getPosition();
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.get('/list_personal_type', getAuth, async function (req, res) {
        const list = await personalService.getPersonalType();
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.get('/list_personal_mate', getAuth, async function (req, res) {
        const list = await personalService.getPersonalMate();
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.get('/list_year', getAuth, async function (req, res) {
        const list = await personalService.getYear();
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.get('/list_bank', getAuth, async function (req, res) {
        const list = await personalService.getBank();
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.get('/list_round', getAuth, async function (req, res) {
        const list = await personalService.getRound(req.query.BgtYear);
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.get('/list_org', getAuth, async function (req, res) {
        const list = await personalService.getOrganization(req.query.BgtYear, req.query.RoundId, req.query.isOperation, req.query.isPersonal,  req.currentUser);
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.post('/list_personal_org',getAuth, 
        celebrate({
            [Segments.BODY]: Joi.object({
                page: Joi.number().allow(null),
                limit: Joi.number().allow(null),
                search: Joi.string().allow(null,""),
                sort: Joi.string().allow(null,""),
                order: Joi.string().allow(null,""),
                OrgId: Joi.string().allow(null),
                RoundId: Joi.string().allow(null),
            })
        }),
        async function (req, res) {
            console.log('list_personal_cattegory list');

            req.body.currentUser = req.currentUser;
            const list = await personalService.listPersonalOrg(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : list.list , total : list.count[0].count })
    })
    
    route.post('/absence',getAuth, 
        celebrate({
            [Segments.BODY]: Joi.object({
                PersonalID: Joi.number().required(),
                date: Joi.string().required(),
                type: Joi.string().allow(null,""),
            })
        }),
        async function (req, res) {
            console.log('absence');
            req.body.currentUser = req.currentUser;
            const detail = await personalService.getAbsence(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : detail})
    })
};