const { Router } = require('express');
const middlewares = require('../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const categoryService = require('../services/category');
const route = Router();

module.exports = (app) => {

    app.use('/category', route);
    app.use(errors());

    route.get('/', function (req, res) {
        console.log('category');
        res.send('Hello World!')
    })

    route.post('/list', 
        celebrate({
            [Segments.BODY]: Joi.object({
                page: Joi.number().allow(null),
                limit: Joi.number().allow(null),
                search: Joi.string().allow(null,""),
                sort: Joi.string().allow(null,""),
                order: Joi.string().allow(null,""),
            })
        }),
        async function (req, res) {
            console.log('category list');
            const list = await categoryService.list(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : list.list , total : list.count[0].count })
    })

     route.get('/detail/:TopicCategoryId', async function (req, res) {
        console.log('category detail');
        const detail = await categoryService.detail(req.params.TopicCategoryId);
        res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.post('/sync', async function (req, res) {
        console.log('category detail');
        const detail = await categoryService.sync(req.body);
        res.status(200).json({code : 0, isSuccess : true , data : detail})
    })
    
};