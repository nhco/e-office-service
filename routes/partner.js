const { Router } = require('express');
//const middlewares = require('../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const partnerService = require('../services/partner');
const route = Router();

const fields_personal = ["PartnerCode","SourceId","IDCard","PrefixUid","PtnFname","PtnSname","PtnSex","NationalityId","IDCardExpired","Passport","SpecialCondition","Remark"];

module.exports = (app) => {

    app.use('/partner', route);
    app.use(errors());

    route.post('/list', 
        celebrate({
            [Segments.BODY]: Joi.object({
                page: Joi.number().allow(null),
                limit: Joi.number().allow(null),
                search: Joi.string().allow(null,""),
                sort: Joi.string().allow(null,""),
                order: Joi.string().allow(null,""),
                SourceId: Joi.number().allow(null),
            })
        }),
        async function (req, res) {
            console.log('partner list');
            const list = await partnerService.list(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : list.list , total : list.count[0].count })
    })

     route.get('/detail/:PartnerId', async function (req, res) {
        console.log('partner detail');
        const detail = await partnerService.detail(req.params.PartnerId);
        if(detail.error_msg) res.status(200).json({code : 0, isSuccess : false , error_msg : detail.error_msg })
        else res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.get('/detailByIDCard/:IDCard', async function (req, res) {
        console.log('partner detail');
        const detail = await partnerService.detailByIDCard(req.params.IDCard);
        if(detail.error_msg) res.status(200).json({code : 0, isSuccess : false , error_msg : detail.error_msg })
        else res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.post('/create', 
        /*celebrate({
            [Segments.BODY]: Joi.object({
                page: Joi.number().allow(null),
                limit: Joi.number().allow(null),
                search: Joi.string().allow(null,""),
                sort: Joi.string().allow(null,""),
                order: Joi.string().allow(null,""),
            })
        }),*/
        async function (req, res) {
            console.log('partner create');
            const created = await partnerService.create(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : created })
    })

    route.put('/update/:PartnerCode', 
        async function (req, res) {
            console.log('partner update');
            const updated = await partnerService.update(req.body, req.params);
            res.status(200).json({code : 0, isSuccess : true , data : updated })
    })

    route.delete('/delete/:PartnerCode', 
        async function (req, res) {
            console.log('partner delete');
            const deleted = await partnerService.delete(req.params);
            res.status(200).json({code : 0, isSuccess : true , data : deleted })
    })

    route.post('/list2', 
        celebrate({
            [Segments.BODY]: Joi.object({
                page: Joi.number().allow(null),
                limit: Joi.number().allow(null),
                search: Joi.string().allow(null,""),
                sort: Joi.string().allow(null,""),
                order: Joi.string().allow(null,""),
                SourceId: Joi.number().allow(null),
            })
        }),
        async function (req, res) {
            console.log('partner list');
            const list = await partnerService.list2(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : list })
            //res.status(200).json({code : 0, isSuccess : true , data : list.list , total : list.count[0].count })
    })

    route.get('/list_category', async function (req, res) {
        console.log('partner detail');
        const list = await partnerService.listCategory();
        res.status(200).json({code : 0, isSuccess : true , data : list})
    })

    route.post('/list_personal_category', 
        celebrate({
            [Segments.BODY]: Joi.object({
                page: Joi.number().allow(null),
                limit: Joi.number().allow(null),
                search: Joi.string().allow(null,""),
                sort: Joi.string().allow(null,""),
                order: Joi.string().allow(null,""),
                CatGroupCode: Joi.string().allow(null),
                field: Joi.string().allow(null),
            })
        }),
        async function (req, res) {
            console.log('list_personal_cattegory list');
            const list = await partnerService.listPersonalCatGroup(req.body);
            res.status(200).json({code : 0, isSuccess : true , data : list.list , total : list.count[0].count })
    })
};