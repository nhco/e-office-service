const { Op, QueryTypes } = require('sequelize');
const { sequelize } = require('../models/index')
const { insertSQL, updateSQL, deleteSQL } = require('../services/tool')
//const moment = require('moment')
const moment = require('moment-timezone');
const db = require("../models");
const randomstring = require('randomstring');
const nh_in_partner = db.nh_in_partner;
const fields_org = [
  "PartnerCode","SourceId","TaxIdent","Under","PositionName","SpecialCondition","Remark","CreateDate","Enable_Partner"
];
const fields_personal = [
  "PartnerCode","SourceId","IDCard","PrefixUid","PtnFname","PtnSname","PtnSex","IDCardExpired",
  "Passport","SpecialCondition","Remark","CreateDate",
];
const fields_mobile = ["PartnerCode","Mobile","UseStatus"];
const fields_detail = ["PartnerId","PartnerCode","AddressGroupId","AddressDetail","Soi","Road","ProvinceCode","DistrictCode","SubDistrictCode","PostCode"];
const fields_telephone = ["DetailId","PartnerCode","AddressGroupId","Phone","Extend"];
const fields_fax = ["DetailId","PartnerCode","AddressGroupId","Fax","Extend"];
const fields_email = ["PartnerCode","Email","UseStatus"];
const fields_category = ["PartnerCode","CatGroupCode"];
const fields_log = [
  'Provider',
  'CategoryName',
  'Method',
  'UserId',
  'LogDate',
  'Token',
  'URL',
  'Data',
  'UserName',
];

exports.list = async (query) => {
  
  let sql = `SELECT * FROM nh_in_partner WHERE DeleteStatus='N'`;
  if(query.SourceId) {
    sql += ` AND SourceId=${query.SourceId}`;
  }
  if(query.page && query.limit) {
    sql += ` LIMIT ${((query.page-1)*query.limit)} , ${query.limit}`;
  }
  console.log(sql);
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });

  let sqlCount = `SELECT COUNT(*) as count FROM nh_in_partner WHERE DeleteStatus='N' AND EnableStatus='Y' `;
  const count = await sequelize.query(sqlCount , { type: QueryTypes.SELECT });

  return { list , count }
};

exports.count = async (query) => {
  let sqlCount = `SELECT COUNT(*) as count FROM nh_in_partner WHERE DeleteStatus='N' AND EnableStatus='Y' `;
  const count = await sequelize.query(sqlCount , { type: QueryTypes.SELECT });

  return count;
};

exports.detail = async (PartnerId) => {
  let sql = `SELECT * FROM nh_in_partner 
      LEFT JOIN nh_in_partner_prefix  prefix ON prefix.PrefixUid=nh_in_partner.PrefixUid
      WHERE PartnerId=${PartnerId} AND SourceId=1 AND nh_in_partner.DeleteStatus='N' AND nh_in_partner.EnableStatus='Y' `;
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
  if(data==null || data[0]==null) {
    return { error_msg : 'Not found data'}
  }
  if(data[0]) {
    

    const sqlAddress = `SELECT * FROM nh_in_partner_detail 
            LEFT JOIN nh_in_address_group ON nh_in_partner_detail.AddressGroupId=nh_in_address_group.AddressGroupId
            LEFT JOIN tblmaster_province AS Pro ON Pro.ProvinceCode=nh_in_partner_detail.ProvinceCode
            LEFT JOIN tblmaster_district AS Dis ON Dis.DistrictCode=nh_in_partner_detail.DistrictCode
            LEFT JOIN tblmaster_sub_district AS Sub ON Sub.SubDistrictCode=nh_in_partner_detail.SubDistrictCode 
            WHERE nh_in_partner_detail.PartnerCode='${data[0].PartnerCode}' AND nh_in_partner_detail.DeleteStatus='N'`;
    const address = await sequelize.query(sqlAddress , { type: QueryTypes.SELECT });
    
    const sqlEmail = `SELECT * FROM nh_in_email WHERE PartnerCode='${data[0].PartnerCode}' AND DeleteStatus='N'`;
    const email = await sequelize.query(sqlEmail , { type: QueryTypes.SELECT });

    const sqlPhone = `SELECT * FROM nh_in_phone WHERE PartnerCode='${data[0].PartnerCode}' AND DeleteStatus='N'`;
    const phone = await sequelize.query(sqlPhone , { type: QueryTypes.SELECT });

    const sqlMobile = `SELECT * FROM nh_in_mobile WHERE PartnerCode='${data[0].PartnerCode}' AND DeleteStatus='N'`;
    const mobile = await sequelize.query(sqlMobile , { type: QueryTypes.SELECT });

    const sqlFax = `SELECT * FROM nh_in_fax WHERE PartnerCode='${data[0].PartnerCode}' AND DeleteStatus='N'`;
    const fax = await sequelize.query(sqlFax , { type: QueryTypes.SELECT });

    const sqlCagtegory = `SELECT * FROM nh_in_categorygroup_ptn 
            LEFT JOIN nh_in_categorygroup ON nh_in_categorygroup.CatGroupCode=nh_in_categorygroup_ptn.CatGroupCode
            WHERE PartnerCode='${data[0].PartnerCode}' AND nh_in_categorygroup_ptn.DeleteStatus='N'`;
            console.log(sqlAddress);
    const category = await sequelize.query(sqlCagtegory , { type: QueryTypes.SELECT });

    if(data && data[0].brithday=='0000-00-00') data[0].brithday = null;
    return {...data[0], address : address, email : email, phone : phone, mobile : mobile, fax : fax, category: category}
  } 
  return null;
};

exports.detailByIDCard = async (IDCard) => {
  let sql = `SELECT * FROM nh_in_partner 
      LEFT JOIN nh_in_partner_prefix  prefix ON prefix.PrefixUid=nh_in_partner.PrefixUid
      WHERE REPLACE(IDCard, "-", "")='${IDCard}' AND SourceId=1 AND nh_in_partner.DeleteStatus='N' AND nh_in_partner.EnableStatus='Y' `;
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
  if(data==null || data[0]==null) {
    return { error_msg : 'Not found data'}
  }
  if(data[0]) {

    const sqlAddress = `SELECT * FROM nh_in_partner_detail 
            LEFT JOIN nh_in_address_group ON nh_in_partner_detail.AddressGroupId=nh_in_address_group.AddressGroupId
            LEFT JOIN tblmaster_province AS Pro ON Pro.ProvinceCode=nh_in_partner_detail.ProvinceCode
            LEFT JOIN tblmaster_district AS Dis ON Dis.DistrictCode=nh_in_partner_detail.DistrictCode
            LEFT JOIN tblmaster_sub_district AS Sub ON Sub.SubDistrictCode=nh_in_partner_detail.SubDistrictCode 
            WHERE nh_in_partner_detail.PartnerCode='${data[0].PartnerCode}' AND nh_in_partner_detail.DeleteStatus='N'`;
    
    const address = await sequelize.query(sqlAddress , { type: QueryTypes.SELECT });
    
    const sqlEmail = `SELECT * FROM nh_in_email WHERE PartnerCode='${data[0].PartnerCode}' AND DeleteStatus='N'`;
    const email = await sequelize.query(sqlEmail , { type: QueryTypes.SELECT });
    
    const sqlPhone = `SELECT * FROM nh_in_phone WHERE PartnerCode='${data[0].PartnerCode}' AND DeleteStatus='N'`;
    const phone = await sequelize.query(sqlPhone , { type: QueryTypes.SELECT });
    
    const sqlMobile = `SELECT * FROM nh_in_mobile WHERE PartnerCode='${data[0].PartnerCode}' AND DeleteStatus='N'`;
    const mobile = await sequelize.query(sqlMobile , { type: QueryTypes.SELECT });
    
    const sqlFax = `SELECT * FROM nh_in_fax WHERE PartnerCode='${data[0].PartnerCode}' AND DeleteStatus='N'`;
    const fax = await sequelize.query(sqlFax , { type: QueryTypes.SELECT });
    
    const sqlCagtegory = `SELECT * FROM nh_in_categorygroup_ptn 
            LEFT JOIN nh_in_categorygroup ON nh_in_categorygroup.CatGroupCode=nh_in_categorygroup_ptn.CatGroupCode
            WHERE PartnerCode='${data[0].PartnerCode}' AND nh_in_categorygroup_ptn.DeleteStatus='N'`;
    
    const category = await sequelize.query(sqlCagtegory , { type: QueryTypes.SELECT });
    
    if(data && data[0].brithday=='0000-00-00') data[0].brithday = null;
    return {...data[0], address : address, email : email, phone : phone, mobile : mobile, fax : fax, category: category}
  } 
  return null;
};

exports.list2 = async (query) => {
  const partners = await nh_in_partner.findAll({
    where : {SourceId : query.SourceId },
  });
  return partners;
};

exports.create = async (data) => {
  return this.upsert(data,null)
  console.log('data',data);
  var fields = [];
  //Set Default
  data.CreateDate = moment().utc().format('YYYY-MM-DD HH:mm:ss');
  data.UseStatus = 'Y';

  if(data.SourceId==1) {
    fields = fields_personal;
  } else if(data.SourceId==2)  {
    data.Enable_Partner = 'Y';
    fields = fields_org;
  }
  fields.push('UpdateDate')
  //fields.push('UseStatus')

  console.log('fields',fields);
  if(!data.PartnerCode) {
    data.PartnerCode = randomstring.generate({
      length: 12,
      charset: 'alphabetic'
    });
  }
  console.log('#create data',data,fields);
  const partner = await insertSQL('nh_in_partner', data, fields);console.log('partner',partner);
  data.PartnerId = partner[0] || null;
  if(data.Mobile) {
    const mobile = await insertSQL('nh_in_mobile', data, fields_mobile);
    console.log('mobile',mobile);
  }

  //CatGroup
  if(data.CatGroups && data.CatGroups.length>0){
    for await(var cat of data.CatGroups){
      cat.PartnerCode = data.PartnerCode;
      const catgroup = await insertSQL('nh_in_categorygroup_ptn', cat, fields_category);console.log('catgroup',catgroup);
    }
  } else {
    const catData = { CatGroupCode : 'CNV3111' ,PartnerCode : data.PartnerCod};
    const catgroup = await insertSQL('nh_in_categorygroup_ptn', catData, fields_category);console.log('catgroup',catgroup);
  }
  //Address
  if(data.Address && data.Address.length>0){
    console.log('address',data.Address);
    for await(var address of data.Address){
      address.PartnerCode = data.PartnerCode;
      address.UseStatus = 'Y';
      console.log('partner',partner);
      if(partner) {
        address.PartnerId = partner[0];
        const detail = await insertSQL('nh_in_partner_detail', address, fields_detail);console.log('detail',detail);
        console.log('detail',detail);
        if(detail){
          address.DetailId = detail[0];
          const phone = await insertSQL('nh_in_phone', address, fields_telephone);console.log('phone',phone);
          const fax = await insertSQL('nh_in_fax', address, fields_fax);console.log('fax',fax);
          const email = await insertSQL('nh_in_email', address, fields_email);console.log('email',email);
        }
      }
    }
  }
  return partner
};

exports.update = async (data, params) => {
  return this.upsert(data, params)
  data.UpdateDate = moment().utc().format('YYYY-MM-DD HH:mm:ss');
  data.UseStatus = 'Y';

  if(data.SourceId==1) {
    fields = fields_personal;
  } else if(data.SourceId==2)  {
    data.Enable_Partner = 'Y';
    fields = fields_org;
  }

  fields.push('UpdateDate')
  //fields.push('UseStatus')
  console.log('fields',fields);
  console.log('data',data);
  var where = `PartnerCode='${params.PartnerCode}' AND EnableStatus='Y' AND DeleteStatus='N'`;

  const partner = await updateSQL('nh_in_partner', data, fields, where);
  console.log('partner',partner);
  if(data.Mobile) {
    const mobile = await insertSQL('nh_in_mobile', data, fields_mobile);
    console.log('mobile',mobile);
  }

  //CatGroup
  if(data.CatGroups && data.CatGroups.length>0) await deleteSQL('nh_in_categorygroup_ptn', where);
  for await(var cat of data.CatGroups){
    cat.PartnerCode = params.PartnerCode;
    const catgroup = await insertSQL('nh_in_categorygroup_ptn', cat, fields_category);console.log('catgroup',catgroup);
  }
  //Address
  for await(var address of data.Address){
    const sqlDetail = `SELECT * FROM nh_in_partner_detail WHERE PartnerCode='${params.PartnerCode}' AND AddressGroupId='${address.AddressGroupId}'`;console.log('sqlDetail',sqlDetail);
    const queryDetail = await sequelize.query( sqlDetail, { type: QueryTypes.SELECT });console.log('queryDetail',queryDetail);

    address.UseStatus = 'Y';
    if(queryDetail && queryDetail[0] && queryDetail[0].DetailId){
      const email = await updateSQL('nh_in_email', address, fields_email, where + ` AND DetailId='${queryDetail[0].DetailId}'`);
      console.log('email',email);
      const detail = await updateSQL('nh_in_partner_detail', address, fields_detail, where + ` AND DetailId='${queryDetail[0].DetailId}'`);
      console.log('detail',detail);
      const phone = await updateSQL('nh_in_phone', address, fields_telephone, where + ` AND DetailId='${queryDetail[0].DetailId}'`);
      console.log('phone',phone);
      const fax = await updateSQL('nh_in_fax', address, fields_fax, where + ` AND DetailId='${queryDetail[0].DetailId}'`);
      console.log('fax',fax);
    }
  }
  
};

exports.upsert = async (data, params) => {
  console.log('#upsert data',data)
  data.UpdateDate = moment().utc().format('YYYY-MM-DD HH:mm:ss');
  data.UseStatus = 'Y';
  delete data.PartnerCode

  let detailPartner = {}

  if(data.SourceId==1) {
    fields = fields_personal;
    const sqlPartner = `SELECT * FROM nh_in_partner
        WHERE REPLACE(IDCard,'-','')=REPLACE('${data.IDCard}','-','')`;console.log('#upsert sqlPartner',sqlPartner);
    const queryPartner = await sequelize.query( sqlPartner, { type: QueryTypes.SELECT });console.log('#upsert queryPartner',queryPartner);
    detailPartner = queryPartner ? queryPartner[0] : {}
  } else if(data.SourceId==2)  {
    data.Enable_Partner = 'Y';
    fields = fields_org;

    const sqlPartner = `SELECT * FROM nh_in_partner
       WHERE REPLACE(TaxIdent,'-','')=REPLACE('${data.TaxIdent}','-','') ORDER BY UpdateDate DESC`;
    console.log('sqlPartner',sqlPartner);
    const queryPartner = await sequelize.query( sqlPartner, { type: QueryTypes.SELECT });
    console.log('#upsert queryPartner',queryPartner);
    detailPartner = queryPartner ? queryPartner[0] : {}
  }
  var where;
  var partner
  
  if(detailPartner){

    //Update Partner
    fields.push('UpdateDate')
    where = `PartnerCode='${detailPartner.PartnerCode}' AND EnableStatus='Y' AND DeleteStatus='N'`;
    partner = await updateSQL('nh_in_partner', data, fields, where);
    
  } else {
    //Insert Partner
    partner = await insertSQL('nh_in_partner', data, fields);console.log('partner',partner);
    data.PartnerId = partner[0] || null;
    
    const queryPartner = await sequelize.query( `SELECT * FROM nh_in_partner  WHERE PartnerId=${data.PartnerId} ORDER BY UpdateDate DESC`, { type: QueryTypes.SELECT });
    console.log('queryPartner',queryPartner);
    detailPartner = queryPartner ? queryPartner[0] : {}
  }
  console.log('partner',partner);
  //Insert Mobile
  if(data.Mobile) {
    const queryMobile = await sequelize.query(  `SELECT * FROM nh_in_mobile  WHERE PartnerCode='${detailPartner.PartnerCode}' AND Mobile='${data.Mobile}' `, { type: QueryTypes.SELECT });
    if(!queryMobile || queryMobile.length==0) await insertSQL('nh_in_mobile', data, fields_mobile);
  }
  if(data.Email) {
    const queryEmail = await sequelize.query(  `SELECT * FROM nh_in_email  WHERE PartnerCode='${detailPartner.PartnerCode}' AND Email='${data.Email}' `, { type: QueryTypes.SELECT });
    if(!queryEmail || queryEmail.length==0)  await insertSQL('nh_in_email', data, fields_email);
  }  
  //Delete & Insert CatGroup
  if(data.CatGroups && data.CatGroups.length>0) await deleteSQL('nh_in_categorygroup_ptn', where);
  for await(var cat of data.CatGroups){
    cat.PartnerCode = detailPartner.PartnerCode;
    const catgroup = await insertSQL('nh_in_categorygroup_ptn', cat, fields_category);console.log('catgroup',catgroup);
  }
  //Address
  for await(var address of data.Address){
      address.PartnerCode = detailPartner.PartnerCode;
      address.UseStatus = 'Y';
      address.PartnerId = detailPartner.PartnerId;
      console.log('address',address)
       
      const queryDetail = await sequelize.query(  `SELECT * FROM nh_in_partner_detail  WHERE PartnerCode='${detailPartner.PartnerCode}' AND AddressGroupId='${address.AddressGroupId}' `, { type: QueryTypes.SELECT });
      console.log('queryDetail',queryDetail)
      if(!queryDetail || queryDetail.length==0)  {
        const detail = await insertSQL('nh_in_partner_detail', address, fields_detail, where + ` AND AddressGroupId='${address.AddressGroupId}'`);
        console.log('detail',detail)
        address.DetailId = detail[0]
      } else {
        address.DetailId = queryDetail[0].DetailId
      }
        
      console.log('address',address)
      
      const queryPhone = await sequelize.query(  `SELECT * FROM nh_in_phone  WHERE PartnerCode='${detailPartner.PartnerCode}' AND Phone='${address.Phone}' `, { type: QueryTypes.SELECT });
      if(!queryPhone || queryPhone.length==0) await insertSQL('nh_in_phone', address, fields_telephone);
      
        const queryFax = await sequelize.query(  `SELECT * FROM nh_in_fax  WHERE PartnerCode='${detailPartner.PartnerCode}' AND Fax='${address.Fax}' `, { type: QueryTypes.SELECT });
      if(!queryFax || queryFax.length==0)  await insertSQL('nh_in_fax', address, fields_fax);
  }
  console.log('#upsert detailPartner',detailPartner);
  return detailPartner
  
};

exports.delete = async (params) => {
  if(params.PartnerCode) {
    var where = `PartnerCode='${params.PartnerCode}'`;

    await deleteSQL('nh_in_categorygroup_ptn', where);
    await deleteSQL('nh_in_partner', where);
    await deleteSQL('nh_in_mobile', where);
    await deleteSQL('nh_in_partner_detail', where);
    await deleteSQL('nh_in_email', where);
    await deleteSQL('nh_in_phone', where);
    await deleteSQL('nh_in_fax', where);

    return true;
  } else {
    return 'Not fountd PartnerCode';
  }
};


exports.listCategory = async () => {
  let sql = `SELECT CatGroupId,CatGroupName,CatGroupCode,CatGroupParent 
    FROM nh_in_categorygroup WHERE DeleteStatus='N' AND CatGroupParent=0 AND EnableStatus='Y' ORDER BY CatGroupName ASC`;
  console.log(sql);
  var result = [];
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });
  console.log(list);
  if(list.length>0){
    for await(const item of list ) {
      //console.log('->',item);
      const child = await this.childsCatGroup(item.CatGroupId);
      if(child.length>0){
        item.childs = child;
      }
      result.push(item);
    }
  }

  return { list : result  }
};

exports.childsCatGroup = async (ParentId) => {
  var result = [];
  let sql = `SELECT CatGroupId,CatGroupName,CatGroupCode,CatGroupParent 
    FROM nh_in_categorygroup WHERE DeleteStatus='N' AND EnableStatus='Y' AND CatGroupParent=${ParentId}`;
  //console.log('->',sql);
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });
  if(list.length>0){
    for await(const item of list ) {
      //console.log('-->> item',item);
      const child = await this.childsCatGroup(item.CatGroupId);
      //console.log('-->> child',child);
      if(child.length>0){
        item.childs = child;
      }
      result.push(item);
    }
  }
  return result;
}

exports.listPersonalCatGroup = async (query) => {
  
  let sql = ` FROM partner_view AS part
      LEFT JOIN nh_in_categorygroup_ptn AS cat ON part.PartnerCode=cat.PartnerCode
      WHERE part.DeleteStatus='N' AND part.EnableStatus='Y' AND cat.EnableStatus='Y' 
      AND cat.DeleteStatus='N' AND SourceId=1 AND cat.CatGroupCode='${query.CatGroupCode}'`;
  if(query.page && query.limit) {
    sql += ` LIMIT ${((query.page-1)*query.limit)} , ${query.limit}`;
  }
  let list;
  let sqlList = '';
  if(query.field && query.field=='all'){
    sqlList = 'SELECT * '+sql;
  } else {
    sqlList = 'SELECT part.PartnerId,part.PartnerCode,PrefixUid,IDCard,PtnFname,PtnSname,PtnSex,Email,Phone,AddressDetail,Soi,Road,Province,District,SubDistrictCode,PostCode '+sql;
  }
  list = await sequelize.query(sqlList , { type: QueryTypes.SELECT });
  const count = await sequelize.query('SELECT COUNT(*) '+ sql, { type: QueryTypes.SELECT });

  if(query.currentUser){
    const log = {
      Provider : 'API',
      Method : 'POST',
      UserId : query.currentUser.UserID || '',
      LogDate : moment().format('YYYY-MM-DD HH:mm:ss'),
      Token : query.currentUser.token || '',
      URL : 'partner/list_personal_category',
      Data: sqlList ,
      UserName: query.currentUser.Username || ''
    };
    const insert = await insertSQL('provider_log', log, fields_log);
  }

  return { list , count }
};