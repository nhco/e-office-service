const { Router } = require('express');
const user = require('./user');
const personal = require('./personal');
const partner = require('./partner');
const budget = require('./budget');
const tool = require('./tool');
const category = require('./category');
const eform = require('./eform');
const mou = require('./mou');
const report = require('./report');
const email = require('./email');
const erp_personal = require('./erp/personal');
const erp_partner = require('./erp/partner');
const erp_budget = require('./erp/budget');
const erp_tool = require('./erp/tool');
const erp_category = require('./erp/category');
const erp_eform = require('./erp/eform');
const erp_mou = require('./erp/mou');
const erp_report = require('./erp/report');
const erp_user = require('./erp/user');
const erp_inventory = require('./erp/inventory');

module.exports = () => {
  const app = Router();
  user(app);
  partner(app);
  personal(app);
  budget(app);
  tool(app);
  category(app);
  eform(app);
  mou(app);
  report(app);
  //ERP
  erp_user(app);
  erp_partner(app);
  erp_personal(app);
  erp_budget(app);
  erp_tool(app);
  erp_category(app);
  erp_eform(app);
  erp_mou(app);
  erp_report(app);
  erp_inventory(app);
  email(app);

  
  return app;
};
