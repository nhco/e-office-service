const { Op, QueryTypes } = require('sequelize');
const { sequelize } = require('../models/index')
const moment = require('moment')
const db = require("../models");

exports.detailFC001 = async (DocCode) => {
  console.log(DocCode);
  let sql = `SELECT 
      tm.*,t5.PItemCode,t5.PItemName,t9.PrjCode,t9.PrjName,t7.PrjActCode,
      t7.PrjActName,t8.SourceExName,t8.SourceCode,t4.LPlanGroupId,t4.LPlanGroupName
    FROM tblfinance_form_hold as tm 
    LEFT JOIN tblbudget_init_plan_item AS t5 ON t5.PItemCode = tm.PItemCode AND t5.BgtYear=tm.BgtYear 
    LEFT JOIN tblbudget_init_plan_item_group AS t4 ON t5.LPlanGroupId = t4.LPlanGroupId
    LEFT JOIN tblbudget_project_detail AS t6 ON t6.PrjDetailId=tm.PrjDetailId
    LEFT JOIN tblbudget_project_activity AS t7 ON t7.PrjActCode = tm.PrjActCode AND t6.PrjDetailId=t7.PrjDetailId
    LEFT JOIN tblbudget_project AS t9 ON t9.PrjId=t6.PrjId
    LEFT JOIN tblbudget_init_source_external AS t8 ON t8.SourceExId=tm.SourceExId
    WHERE tm.DocCode='${DocCode}'`;
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return data;
};

exports.detailAnnounce = async (TypeIntCode) => {
  console.log(TypeIntCode);
  let sql = `SELECT tr.IntId,tr.IntCode,tr.SubCode,tr.DocTypeId,tr.Topic,tr.TypeIntId,tr.SignBy,tr.SignDate,tr.EffectDate,tr.EnableStatus,tr.DeleteStatus,tr.CancelStatus,
    tz.TypeIntCode,tz.TypeIntId,td.DocTypeCode,tr.Comment,td.DocTypeName,
    CONCAT(tz.TypeIntCode,'/',td.DocTypeCode,'-',LPAD(tr.IntCode, 2, '0'),IF(tr.SubCode,tr.SubCode,''),'/',SUBSTRING(SUBSTRING_INDEX(tr.SignDate, '-', 1) +543,3,4)) AS Code
    FROM tblintra_announce_internal as tr
    LEFT JOIN tblintra_anounce_type_internal_document as tg ON tr.TypeIntId=tg.TypeIntId
    LEFT JOIN tblintra_anounce_type_internal as tz ON  tr.TypeIntId=tz.TypeIntId
    LEFT JOIN tblintra_anounce_document as td ON  td.DocTypeId=tr.DocTypeId
    WHERE tz.LevelCode='1' 
    AND tr.EnableStatus='Y' 
    AND tr.DeleteStatus='N' 
    AND tr.DocTypeId=1 
    AND CONCAT(tz.TypeIntCode,'/',td.DocTypeCode,'-',LPAD(tr.IntCode, 2, '0'),IF(tr.SubCode,tr.SubCode,''),'/',SUBSTRING(SUBSTRING_INDEX(tr.SignDate, '-', 1) +543,3,4))='${TypeIntCode}'
    GROUP BY tr.IntId,tr.IntCode,tr.SubCode,tr.DocTypeId,tr.Topic,tr.TypeIntId,tr.SignBy,tr.SignDate,tr.EffectDate,tr.EnableStatus,tr.DeleteStatus,tr.CancelStatus,
    tz.TypeIntCode,tz.TypeIntId,td.DocTypeCode`;

  console.log(sql);
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });

  if(data && data.length>0){
    let sqlDoc = `SELECT af.*,doc.DocPath,doc.DocName,doc.DocFile,DocSize,DocDetail,DocExtension
      FROM tblintra_anounce_internal_file af
      LEFT JOIN tblintra_edoc_center AS doc ON doc.DocId=af.DocId
      WHERE IntId='${data[0].IntId}' `;

    console.log(sqlDoc);
    const document = await sequelize.query(sqlDoc , { type: QueryTypes.SELECT });
    return { data, document };
  } else {
    return { message : 'Not found' }
  }
};
