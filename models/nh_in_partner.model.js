'use strict';
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  const nh_in_partner = sequelize.define("nh_in_partner", {
    PartnerId:{
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true 
    }, 
    PartnerCode:  DataTypes.STRING, 
    SourceId:  DataTypes.INTEGER,  
    PrefixUid:  DataTypes.STRING, 
    AltPrefix:  DataTypes.STRING, 
    PtnFname:  DataTypes.STRING, 
    PtnSname:  DataTypes.STRING, 
    PositionName:  DataTypes.STRING, 
    ExpertUid:  DataTypes.STRING, 
    Under:  DataTypes.STRING, 
    PtnSex:  DataTypes.STRING, 
    Picture:  DataTypes.STRING, 
    PicturePath:  DataTypes.STRING, 
    IDCard:  DataTypes.STRING(13), 
    Passport:  DataTypes.STRING, 
    SpecialCondition:  DataTypes.STRING, 
    Remark:  DataTypes.STRING, 
    PartnerType:  DataTypes.STRING, 
    HowToContact:  DataTypes.INTEGER, 
    ReturnUid:  DataTypes.STRING, 
    DataSource:  DataTypes.STRING,// enum('Unknow', 'Convert', 'Excel', 'UserInput')
    Public:  DataTypes.STRING(1), 
    Ordering: DataTypes.INTEGER, 
    EnableStatus: DataTypes.STRING(1), 
    EnableStatusMedia: DataTypes.STRING(1), 
    DeleteStatus: DataTypes.STRING(1), 
    CreateDate: DataTypes.DATE, 
    CreateBy: DataTypes.INTEGER, 
    UpdateDate: DataTypes.DATE, 
    UpdateBy: DataTypes.INTEGER, 
    TaxIdent: DataTypes.STRING, 
    address_long: DataTypes.STRING, 
    MediaUsed: DataTypes.STRING(1), 
    interested: DataTypes.STRING, 
    notes: DataTypes.STRING, 
    isAssociate: DataTypes.INTEGER, 
    Enable_Partner: DataTypes.STRING(1), 
    rate: DataTypes.INTEGER, 
    UnderCode: DataTypes.STRING, 
    CountryParent: DataTypes.STRING, 
    CountryId: DataTypes.STRING, 
    CEO: DataTypes.STRING, 
    ENewsletter: DataTypes.STRING(1), 
    NetworkInterUsed: DataTypes.STRING(1), 
    EnableStatusInventory: DataTypes.STRING(1), 
    BossName: DataTypes.STRING, 
    brithday: DataTypes.DATE, 
    othercontact: DataTypes.STRING, 
    perID: DataTypes.INTEGER, 
    ImportRow: DataTypes.INTEGER, 
    IDCardExpired: DataTypes.DATE, 
    LifeLong: DataTypes.STRING(1), 
    EnableStatusInter: DataTypes.STRING(1), 
    MediaProduct: DataTypes.STRING(1), 
    PartnerRefId: DataTypes.STRING, 
    NationalityId: DataTypes.INTEGER, 
    OrganizationType:DataTypes.STRING, 
    OrganizationStyle: DataTypes.STRING, 
    GranteeType: DataTypes.STRING
  }, {
    tableName: 'nh_in_partner'
  });

  return nh_in_partner;
};