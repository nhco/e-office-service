const { Router } = require('express');
const middlewares = require('../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const eformService = require('../services/eform');
const route = Router();

module.exports = (app) => {

    app.use('/eform', route);
    app.use(errors());

    route.get('/', function (req, res) {
        console.log('eform');
        res.send('Hello World!')
    })

    route.get('/fc001/detail', async function (req, res) {
        console.log('eform detail');
        const detail = await eformService.detailFC001(req.query.DocCode);
        res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.get('/anounce_internal/list', async function (req, res) {
        console.log('anounce_internal list');
        const detail = await eformService.listAnnounce(req.query.search);
        res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

    route.get('/anounce_internal/detail', async function (req, res) {
        console.log('anounce_internal detail');
        const detail = await eformService.detailAnnounce(req.query.TypeIntCode);
        res.status(200).json({code : 0, isSuccess : true , data : detail})
    })

};