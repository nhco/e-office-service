const { Router } = require('express');
const middlewares = require('../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const mouService = require('../services/mou');
const route = Router();

module.exports = (app) => {
  app.use('/mou', route);
  app.use(errors());

  route.get( '/personnel-projects',
    celebrate({ 
      [Segments.QUERY]: { IdCard: Joi.string().required() }
    }),
    async (req, res) => {
      const {IdCard} = req.query;
      const data = await mouService.getPersonnelProjects(IdCard);
      res.status(200).json({code : 0, isSuccess : true , data})
    });
  
  route.get( '/mou-project',
    celebrate({ 
      [Segments.QUERY]: { 
        reference_with: Joi.string().required() ,
        reference_date: Joi.string().required() ,
      }
    }),
    async (req, res) => {
      const data = await mouService.getMouProject(req.query);
      res.status(200).json({code : 0, isSuccess : true , data})
    });

    route.post( '/generate-pdf',
      celebrate({ 
        [Segments.BODY]: { 
          form: Joi.string().required(),
          mou_name: Joi.string().allow(null,""),
          contract_no: Joi.string().allow(null,""),
          installment: Joi.string().allow(null,""),
          iam_token: Joi.string().allow(null,""),
          thaid_token: Joi.string().allow(null,""),
          commenter_sign_ref_id:Joi.string().allow(null,""),
          director_sign_ref_id:Joi.string().allow(null,""),
          approver_sign_ref_id:Joi.string().allow(null,""),
          director_position_name:Joi.string().allow(null,""),
          approver_position_name:Joi.string().allow(null,""),
          row:Joi.array().allow(null),
        }
      }),
      async (req, res) => {
        try {
          console.log('req.body',req.body);
          // Generate the PDF
          const pdfBuffer = await mouService.generatePDF(req.body, res);
          
          
        } catch (error) {
          console.error("Error generating PDF:", error);
          res.status(500).send('Error generating PDF');
        }
      });

  route.post( 
    '/power-bi-report',
    celebrate({ 
      [Segments.BODY]: { 
        mode: Joi.string().required(),
        bgt_year: Joi.string().required(),
      }
    }),
    async (req, res) => {
      try {
        const {mode, bgt_year} = req.body;
        const data = await mouService.postPowerBiReport(bgt_year, mode);
        res.status(200).json({code : 0, isSuccess : true , data })
      } catch (error) {
        console.error(error);
        res.status(500).json({ code : 0, isSuccess : false , message: error.message })
      }
    });
};