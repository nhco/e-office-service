const { Op, QueryTypes } = require('sequelize');
const { sequelize } = require('../models/index')
const moment = require('moment')
const db = require("../models");
const fields_tools = ["CategoryParentId","CategoryName","CreateBy","UpdateBy","CreateDate","UpdateDate","EnableStatus","DeleteStatus"];

exports.list = async (query) => {
  let sql = `SELECT * FROM tblgis_tool WHERE DeleteStatus='N' AND CategoryParentId=0`;
  if(query.SourceId) {
    sql += ` AND SourceId=${query.SourceId}`;
  }
  sql += ` ORDER BY CategoryName ASC`;
  if(query.page && query.limit) {
    sql += ` LIMIT ${((query.page-1)*query.limit)} , ${query.limit}`;
  }
  console.log(sql);

  let sqlCount = `SELECT COUNT(*) as count FROM tblgis_tool WHERE DeleteStatus='N'  AND EnableStatus='Y' AND CategoryParentId=0 `;
  const count = await sequelize.query(sqlCount , { type: QueryTypes.SELECT });

  var result = [];
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });
  if(list.length>0){
    for await(const item of list ) {
      //console.log('->',item);
      const child = await this.childs(item.CategoryId);
      if(child.length>0){
        item.childs = child;
      }
      result.push(item);
    }
  }

  return { list : result , count }
};

exports.childs = async (ParentId) => {
  var result = [];
  let sql = `SELECT * FROM tblgis_tool WHERE DeleteStatus='N' AND EnableStatus='Y' AND CategoryParentId=${ParentId}`;
  console.log('->',sql);
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });
  if(list.length>0){
    for await(const item of list ) {
      console.log('-->> item',item);
      const child = await this.childs(item.CategoryId);
      console.log('-->> child',child);
      if(child.length>0){
        item.childs = child;
      }
      result.push(item);
    }
  }
  return result;
}

exports.detail = async (CategoryId) => {
  let sql = `SELECT * FROM tblgis_tool WHERE CategoryId=${CategoryId} AND DeleteStatus='N' AND EnableStatus='Y'`;
  const data = await sequelize.query(sql , { type: QueryTypes.SELECT });
  if(data){
    const child = await this.childs(data[0].CategoryId);
    if(child.length>0){
      data[0].childs = child;
    }
    return data[0];
  } else return null;
};

exports.sync = async (data) => {

  var rootCategoryId;
  var result = []
  for await(const item of data){
    if(item.refId) {
      let sql = `SELECT * FROM tblgis_tool WHERE CategoryId=${item.refId} AND DeleteStatus='N' AND EnableStatus='Y'`;console.log(sql);
      const root = await sequelize.query(sql , { type: QueryTypes.SELECT });console.log(root);
      rootCategoryId = root[0].CategoryId;
    } else {
      const rootData = {
        CategoryParentId : 0,
        CategoryName : item.name,
        UpdateBy : 999,
        CreateBy : 999,
        CreateDate : moment().format('YYYY-MM-DD HH:mm:ss'),
        UpdateDate : moment().format('YYYY-MM-DD HH:mm:ss'),
        DeleteStatus : 'N',
        EnableStatus: 'Y'
      };
      const insert = await this.insertSQL('tblgis_tool', rootData, fields_tools);console.log(insert);
      console.log('insert',insert);
      rootCategoryId = insert[0];
    }
    console.log('rootCategoryId',rootCategoryId);
    const category = await this.insertChilds(rootCategoryId, item.children);

    result.push({
      id : item.id,
      refId : rootCategoryId,
      children : category
    })
  }
  return result
};

exports.insertChilds = async (parentId, children) => {
  var result = [];
  const delelte = await this.updateSQL('tblgis_tool', { DeleteStatus : 'Y'}, fields_tools, `CategoryParentId=${parentId}`);
  console.log('delelte',delelte);
  
  if(children && children.length>0){
    for await(const item of children ) {
      var categoryId;
      if(item.refId) {
        let sql = `SELECT * FROM tblgis_tool WHERE CategoryId=${item.refId} `;console.log(sql);
        const category = await sequelize.query(sql , { type: QueryTypes.SELECT });
        categoryId = category[0].CategoryId;

        const catData = {
          CategoryParentId : parentId,
          CategoryName : item.name,
          UpdateBy : 999,
          UpdateDate : moment().format('YYYY-MM-DD HH:mm:ss'),
          DeleteStatus : 'N',
          EnableStatus: 'Y'
        };
        const update = await this.updateSQL('tblgis_tool', catData, fields_tools, `CategoryId=${categoryId}`);
        console.log('update',update);
      } else {
        const catData = {
          CategoryParentId : parentId,
          CategoryName : item.name,
          UpdateBy : 999,
          CreateBy : 999,
          CreateDate : moment().format('YYYY-MM-DD HH:mm:ss'),
          UpdateDate : moment().format('YYYY-MM-DD HH:mm:ss'),
        };
        const insert = await this.insertSQL('tblgis_tool', catData, fields_tools);
        console.log('insert',insert);
        categoryId = insert[0];
      }
      
      const resCat = await this.insertChilds(categoryId, item.children);
      result.push({
        id : item.id,
        refId : categoryId,
        children : resCat
      });
    }
  }
  return result;
}

exports.insertSQL = async (table , data, fields) => {

  var fieldList = [];
  var valueList = [];
  var paramsList = [];

  for (const [key, value] of Object.entries(data)) {
    if(fields.includes(key)){
      valueList.push(value);
      fieldList.push(key);
      paramsList.push('?');
    }
  }
  // console.log('fieldList',fieldList);
  // console.log('valueList',valueList);
  // console.log('paramsList',paramsList);

  const sql = `INSERT INTO ${table} (${fieldList.join(',')}) VALUES (${paramsList.join(',')})`;
  //console.log()
  const created = await sequelize.query(sql, {
     type: QueryTypes.INSERT,
     replacements: valueList,
    },
  );

  return created
};

exports.updateSQL = async (table , data, fields, where) => {

  var fieldList = [];

  for (const [key, value] of Object.entries(data)) {
    if(fields.includes(key)){
      fieldList.push(`${key}='${value}'`);
    }
  }
  //console.log('fieldList',fieldList);
  //console.log(`UPDATE ${table} SET ${fieldList.join(',')} WHERE ${where}`);

  const updated = await sequelize.query(
    `UPDATE ${table} SET ${fieldList.join(',')} WHERE ${where}`,
    {
     type: QueryTypes.UPDATE,
     //replacements: valueList,
    },
  );

  return updated
};

exports.deleteSQL = async (table, where) => {
  const current = moment().format('YYYY-MM-DD');
  const deleted = await sequelize.query(
    `UPDATE ${table} SET DeleteStatus='Y' , UpdateDate = '${current}' WHERE ${where}`,
    {
     type: QueryTypes.UPDATE,
     //replacements: valueList,
    },
  );
  return deleted
};
