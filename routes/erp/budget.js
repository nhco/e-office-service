const { Router } = require('express');
const { getAuth } = require('../../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const budgetService = require('../../services/budget');
const route = Router();

module.exports = (app) => {

    app.use('/erp/budget', route);
    app.use(errors());

    route.get('/version_plan',getAuth, 
        celebrate({
            [Segments.QUERY]: Joi.object({
                BgtYear: Joi.number().required(),
                OrgId: Joi.number().allow(null),
            })
        }),
        async function (req, res) {
            console.log('budget list');
            const list = await budgetService.version_plan(req.query,  req.currentUser);
            res.status(200).json({code : 0, isSuccess : true , data : list })
    })

    route.get('/list_group', getAuth,
        async function (req, res) {
            const list = await budgetService.list_group(req.query,  req.currentUser);
            res.status(200).json({code : 0, isSuccess : true , data : list })
    })

    route.get('/list_plan_main', getAuth,
        async function (req, res) {
            const list = await budgetService.list_plan_main(req.query,  req.currentUser);
            res.status(200).json({code : 0, isSuccess : true , data : list })
    })

    route.get('/list_cost', getAuth,
        async function (req, res) {
            const list = await budgetService.list_cost(req.query,  req.currentUser);
            res.status(200).json({code : 0, isSuccess : true , data : list })
    })

    route.get('/list_plan',getAuth, 
        celebrate({
            [Segments.QUERY]: Joi.object({
                BgtYear: Joi.number().required(),
                OrgId: Joi.number().allow(null),
            })
        }),
        async function (req, res) {
            const list = await budgetService.list_plan(req.query,  req.currentUser);
            res.status(200).json({code : 0, isSuccess : true , data : list })
    })

    route.get('/list_project', getAuth,
        celebrate({
            [Segments.QUERY]: Joi.object({
                BgtYear: Joi.number().required(),
                SCTypeId: Joi.number().required(),
                ScreenLevel: Joi.number().required(),
                OrgId: Joi.number().allow(null),
            })
        }),
        async function (req, res) {
            console.log('list_project');
            const list = await budgetService.list_project(req.query,  req.currentUser);
            res.status(200).json({code : 0, isSuccess : true , data : list })
    })

    route.get('/list_activity', getAuth,
        celebrate({
            [Segments.QUERY]: Joi.object({
              BgtYear: Joi.number().required(),
              PrjDetailId: Joi.number().required(),
            })
        }),
        async function (req, res) {
            console.log('list_project');
            const list = await budgetService.list_activity(req.query,  req.currentUser);
            res.status(200).json({code : 0, isSuccess : true , data : list })
    })

    route.get('/detail_policy', getAuth,
        celebrate({
            [Segments.QUERY]: Joi.object({
                BgtYear: Joi.number().required(),
                PItemCode: Joi.string().required(),
            })
        }),
        async function (req, res) {
            console.log('budget list');
            const list = await budgetService.detail_policy(req.query);
            res.status(200).json({code : 0, isSuccess : true , data : list })
    })

    route.get('/group_policy', getAuth,
        celebrate({
            [Segments.QUERY]: Joi.object({
                BgtYear: Joi.number().required(),
                PGroupId: Joi.number().required(),
            })
        }),
        async function (req, res) {
            console.log('budget list');
            const list = await budgetService.group_policy(req.query);
            res.status(200).json({code : 0, isSuccess : true , data : list })
    })
    
    route.get('/item_purposes', getAuth,
      celebrate({
        [Segments.QUERY]: Joi.object({
          purposeCodes: Joi.array().required(),
        })
      }),
      async function (req, res) {
        const list = await budgetService.getItemPurposes(req.query);
        res.status(200).json({code : 0, isSuccess : true , data : list })
      }
    )

    route.get('/list_policies', getAuth,
      celebrate({
        [Segments.QUERY]: Joi.object({
          PItemCodes: Joi.array().required(),
        })
      }),
      async function (req, res) {
        const list = await budgetService.getListPolicyByPItemCodes(req.query,  req.currentUser);
        res.status(200).json({code : 0, isSuccess : true , data : list })
      }
    )
    route.get('/list_pay', 
        // celebrate({
        //   [Segments.QUERY]: Joi.object({
        //     PItemCodes: Joi.array().required(),
        //   })
        // }),
        async function (req, res) {
          const list = await budgetService.getListPay(req.query,  req.currentUser);
          res.status(200).json({code : 0, isSuccess : true , data : list })
        }
      )
};