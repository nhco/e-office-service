const httpServ = require('./index');

const $serv = httpServ();
const uri = 'v1/report';

const findAllMouProjectResult = () => {
  return $serv.get(`/${uri}/list_project_result`).then( resp => resp.data );
}
const upsertMouProjectResults = data => {
  return $serv.post(`${uri}/upsert_project_results`, data).then( resp => resp.data );
}

module.exports = {
  findAllMouProjectResult,
  upsertMouProjectResults,
};