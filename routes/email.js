const { Router } = require('express');
const middlewares = require('../middlewares/auth');
const { celebrate, Joi, errors, Segments } = require('celebrate');
const emailService = require('../services/email');
const route = Router();

module.exports = (app) => {

    app.use('/email', route);
    app.use(errors());

    route.post('/send', 
        celebrate({
            [Segments.BODY]: Joi.object({
                from: Joi.string().required().allow(null,""),
                to: Joi.array().required(),
                cc: Joi.array().required(),
                subject: Joi.string().required().allow(null,""),
                html: Joi.string().allow(null,""),
            })
        }),
        async function (req, res) {
            console.log('sendEmail body',req.body)
            const sended = await emailService.sendEmail(req.body);
            res.status(200).json({code : 0, isSuccess : true , sended : sended })
    })
    
};