// Polyfill atob for Node.js
if (typeof atob === 'undefined') {
  global.atob = function (b64Encoded) {
      return Buffer.from(b64Encoded, 'base64').toString('binary');
  };
}
const { Op, QueryTypes } = require('sequelize');
const { sequelize } = require('../models/index')
const db = require("../models");
const ord = require('ord');
const jwt = require('jsonwebtoken');
const address_type  = db.address_type;
const {jwtDecode} = require('jwt-decode')
const crypto = require('crypto');
const iconv = require('iconv-lite');

const axios = require('axios');
const qs = require('qs');

const fields_user = ["Username","Password","UserType","FirstName","LastName","Email","CustomerKey","SecretKey"];

exports.list = async (query) => {
  const sql = `SELECT * FROM authen_users WHERE DeleteStatus='N'`;
  const list = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return list
};

exports.detail = async (UserID) => {
  const sql = `SELECT * FROM authen_users WHERE UserID=${UserID}`;
  const detail = await sequelize.query(sql , { type: QueryTypes.SELECT });
  return detail
};

exports.create = async (data, transaction) => {
  const {customerKey, secretKey} = await generateKeys()
  data.CustomerKey = customerKey
  data.SecretKey = secretKey
  data.Password = unicodeToAscii(data.Password)
  const created = await this.insertSQL('authen_users', data, fields_user);
  // const created = await address_type.create(
  //   data, transaction);
  return created
};

exports.update = async (id, data, transaction) => {
  data.Password = unicodeToAscii(data.Password)
  const updated = await this.updateSQL('authen_users', data, fields_user);
  return updated
};

exports.delete = async (id, transaction) => {
  const deleted = await address_type.destroy({
     where :  { id : id } , 
  }, transaction);
  return deleted
};

exports.insertSQL = async (table , data, fields) => {

  var fieldList = [];
  var valueList = [];
  var paramsList = [];

  for (const [key, value] of Object.entries(data)) {
    if(fields.includes(key)){
      valueList.push(value);
      fieldList.push(key);
      paramsList.push('?');
    }
  }
  console.log('fieldList',fieldList);
  console.log('valueList',valueList);
  console.log('paramsList',paramsList);
  console.log('SQL',`INSERT INTO ${table} (${fieldList.join(',')}) VALUES (${paramsList.join(',')})`);

  const created = await sequelize.query(
    `INSERT INTO ${table} (${fieldList.join(',')}) VALUES (${paramsList.join(',')})`,
    {
     type: QueryTypes.INSERT,
     replacements: valueList,
    },
  );

  return created
};

exports.updateSQL = async (table , data, fields, where) => {

  var fieldList = [];

  for (const [key, value] of Object.entries(data)) {
    if(fields.includes(key)){
      fieldList.push(`${key}='${value}'`);
    }
  }
  console.log('fieldList',fieldList);
  console.log(`UPDATE ${table} SET ${fieldList.join(',')} WHERE ${where}`);

  const updated = await sequelize.query(
    `UPDATE ${table} SET ${fieldList.join(',')} WHERE ${where}`,
    {
     type: QueryTypes.UPDATE,
     //replacements: valueList,
    },
  );

  return updated
};

exports.login = async (query) => {
  const password = UnicodeToAscii(query.password);console.log(password);
  const sql = `SELECT tblpersonal.* ,authen_users.*
    FROM authen_users 
    LEFT JOIN tblpersonal ON tblpersonal.PersonalId=authen_users.PersonalId
    WHERE LOWER(Username)='${query.username}' AND Password='${password}' AND LOWER(Active) = 'y'`;
  //console.log(sql);
  const detail = await sequelize.query(sql , { type: QueryTypes.SELECT });
  if(detail && detail[0]){
    if(detail[0].Password) delete(detail[0].Password);
    if(detail[0].AccessToken) delete(detail[0].AccessToken);

    //Find structure
    const sqlStructure = `SELECT org.OrgId,org.OrgIdRef,org.OrgRoundCode,org.OrgName,org.OrgShortName
      FROM tblstructure_operation_personal AS per
      LEFT JOIN tblstructure_operation AS org ON per.PersonalParent=org.OrgId AND per.ParentType='O'
      LEFT JOIN tblstructure_operation_round AS round ON round.RoundId=org.OrgRound
      WHERE PersonalId='${detail[0].PersonalId}' AND round.CurrentUse = 'Y'
      AND round.EnableStatus = 'Y' AND round.DeleteStatus = 'N'`;
    console.log(sqlStructure);
    const structure = await sequelize.query(sqlStructure , { type: QueryTypes.SELECT });
    
    detail[0].structure = structure;

    const token = jwt.sign(
        detail[0], 
        process.env.TOKEN_KEY,
        {
            expiresIn: process.env.TOKEN_EXPIRED,
        }
    );
    const sqlUpdate = `UPDATE authen_users SET AccessToken='${token}' ,LastLogin=Now(),Expired=(NOW() + INTERVAL 24 HOUR) WHERE UserID='${detail[0].UserID}' `;
    console.log(sqlUpdate);
    const updateToken = await sequelize.query(sqlUpdate , { type: QueryTypes.UPDATE });

    return { detail : detail[0], token }
  } else {
    return false;
  }
};

exports.loginEmail = async (query) => {
  const sql = `SELECT tblpersonal.* ,authen_users.*
    FROM authen_users 
    LEFT JOIN tblpersonal ON tblpersonal.PersonalId=authen_users.PersonalId
    WHERE LOWER(tblpersonal.Email)='${query.email}'  AND LOWER(Active) = 'y'`;
  console.log(sql);
  const detail = await sequelize.query(sql , { type: QueryTypes.SELECT });
  if(detail && detail[0]){
    if(detail[0].Password) delete(detail[0].Password);
    if(detail[0].AccessToken) delete(detail[0].AccessToken);

    //Find structure
    const sqlStructure = `SELECT org.OrgId,org.OrgIdRef,org.OrgRoundCode,org.OrgName,org.OrgShortName
      FROM tblstructure_operation_personal AS per
      LEFT JOIN tblstructure_operation AS org ON per.PersonalParent=org.OrgId AND per.ParentType='O'
      LEFT JOIN tblstructure_operation_round AS round ON round.RoundId=org.OrgRound
      WHERE PersonalId='${detail[0].PersonalId}' AND round.CurrentUse = 'Y'
      AND round.EnableStatus = 'Y' AND round.DeleteStatus = 'N'`;
    console.log(sqlStructure);
    const structure = await sequelize.query(sqlStructure , { type: QueryTypes.SELECT });
    
    detail[0].structure = structure;

    const token = jwt.sign(
        detail[0], 
        process.env.TOKEN_KEY,
        {
            expiresIn: process.env.TOKEN_EXPIRED,
        }
    );
    const sqlUpdate = `UPDATE authen_users SET AccessToken='${token}' ,LastLogin=Now(),Expired=(NOW() + INTERVAL 24 HOUR) WHERE UserID='${detail[0].UserID}' `;
    console.log(sqlUpdate);
    const updateToken = await sequelize.query(sqlUpdate , { type: QueryTypes.UPDATE });
    console.log('updateToken',updateToken);
    return { detail : detail[0], token }
  } else {
    return false;
  }
};


exports.loginProvider = async (query,headers) => {
  console.log('headers',headers);
  const password = UnicodeToAscii(query.password);console.log(password);
  const sql = `SELECT tblpersonal.* ,authen_users.*
    FROM authen_users 
    LEFT JOIN tblpersonal ON tblpersonal.PersonalId=authen_users.PersonalId
    WHERE LOWER(Username)='${query.username}' AND Password='${password}' AND LOWER(Active) = 'y'`;
  console.log('sql',sql);
  const detail = await sequelize.query(sql , { type: QueryTypes.SELECT });
  if(detail && detail[0]){
    
    if(detail[0].Password) delete(detail[0].Password);
    if(detail[0].AccessToken) delete(detail[0].AccessToken);

    console.log('SecretKey',detail[0].SecretKey,headers.secret_key);
    if(detail[0].SecretKey && detail[0].SecretKey!=headers.secret_key) {
      return 'SecretKey is not collect'
    }
    console.log('CustomerKey',detail[0].CustomerKey,headers.customer_key);
    if(detail[0].CustomerKey && detail[0].CustomerKey!=headers.customer_key) {
      return 'CustomerKey is not collect'
    }

    //Find structure
    const sqlStructure = `SELECT org.OrgId,org.OrgIdRef,org.OrgRoundCode,org.OrgName,org.OrgShortName
      FROM tblstructure_operation_personal AS per
      LEFT JOIN tblstructure_operation AS org ON per.PersonalParent=org.OrgId AND per.ParentType='O'
      LEFT JOIN tblstructure_operation_round AS round ON round.RoundId=org.OrgRound
      WHERE PersonalId='${detail[0].PersonalId}' AND round.CurrentUse = 'Y'
      AND round.EnableStatus = 'Y' AND round.DeleteStatus = 'N'`;
    console.log(sqlStructure);
    const structure = await sequelize.query(sqlStructure , { type: QueryTypes.SELECT });
    
    detail[0].structure = structure;

    const token = jwt.sign(
        detail[0], 
        process.env.TOKEN_KEY,
        {
            expiresIn: process.env.TOKEN_EXPIRED,
        }
    );
    const sqlUpdate = `UPDATE authen_users SET AccessToken='${token}' ,LastLogin=Now(),Expired=(NOW() + INTERVAL 24 HOUR) WHERE UserID='${detail[0].UserID}' `;
    console.log(sqlUpdate);
    const updateToken = await sequelize.query(sqlUpdate , { type: QueryTypes.UPDATE });

    return { detail : detail[0], token }
  } else {
    return false;
  }
};

exports.oauth2 = async (req, res) => {
  try {

    var data = qs.stringify({
      'grant_type': 'authorization_code',
      'client_id': req.body.client_id || 'e-office',
      'client_secret': req.body.client_secret || process.env.KEYCLOAK_SECRETKEY,
      'code': req.body.code,
      'redirect_uri': req.body.redirect_uri
    });
    console.log('data',data);
    var config = {
      method: 'post',
      url: process.env.KEYCLOAK_TOKEN_URL,
      headers: { 
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data : data
    };
    console.log('config',config);
    
    let result = {};
    await axios(config).then( async (response) => {
      console.log(response.data);
      if(response.data.access_token && req.body.client_id=='partner'){
        const decode = jwtDecode(response.data.access_token)
        //const decode = jwt.verify(response.data.access_token, {complete: true})
        console.log('#oauth2 decode',decode);
        /*response.data.token = jwt.sign(
          {
            FirstName : decode.given_name || '',
            LastName : decode.family_name || '',
            Username : decode.preferred_username || '',
            UserType : "free"
          }, 
          process.env.TOKEN_KEY,
          {
              expiresIn: process.env.TOKEN_EXPIRED,
          }
        );*/
        result = await this.loginEmail({ email : decode.email})
        //console.log('#oauth2 result',result);
      } else {
        result = response.data
      }
    })
    .catch(function (error) {
      console.log(error);
      result = error
    });
    console.log('#oauth2 result',result);
    return result
  } catch {
    return { error: 'OAuth2 Error!' }
  }
};

function UnicodeToAscii(text){
  var Ascii = '';
  for(var i=0; i<text.length; i++){
    const charAt = padNumber(text.substr( i, 1).charCodeAt(0)-3+7,3);
    Ascii += ''+charAt;
  }
  return Ascii;
}

function AsciiToUnicode($Text){
  $txt_len = strlen($Text);
  for($i=0;$i<$txt_len;$i++){
    $Asc_text = substr($Text, $i, 3) + 3 - 7;
    $t_chr = chr($Asc_text);
    $Char = $Char.$t_chr; 
    $i=$i+2;
  }
  return $Char;
}
function padNumber(number, pad) {
    var N = Math.pow(10, pad);
    return number < N ? ("" + (N + number)).slice(1) : "" + number
}

const generateKeys = () => {
  // สร้าง customer key และ secret key โดยใช้ random bytes
  const customerKey = crypto.randomBytes(8).toString('hex');
  const secretKey = crypto.randomBytes(16).toString('hex');

  return { customerKey, secretKey };
}

function unicodeToAscii(text) {
  let ascii = '';
  const textLength = text.length;

  for (let i = 0; i < textLength; i++) {
      const chrText = text.charAt(i);
      const tAscii = chrText.charCodeAt(0);
      const asciiValue = tAscii - 3 + 7;

      // Convert the ASCII value to a string with leading zeros
      const asciiString = String(asciiValue).padStart(3, '0');
      
      ascii += asciiString;
  }

  return ascii;
}