const { Op } = require('sequelize');
const { sequelize } = require('../models/index')
const db = require("../models");
const address_type  = db.address_type;

exports.list = async (query) => {
  const whereModel = {};
  if (query.search) {
    whereModel[Op.or] = [
      { firstname: { [Op.like]: `%${query.firstname}%` } },
    ];
  }
  var list = await address_type.findAll(
    { 
      where: whereModel 
    });
  return list
};

exports.detail = async (id) => {
  const detail = await address_type.findOne({id});
  return detail
};

exports.create = async (data, transaction) => {
  const created = await address_type.create(
    data, transaction);
  return created
};

exports.update = async (id, data, transaction) => {
  const updated = await address_type.update(
    data,
    { where : { id : id } }, transaction);
  const detail  = await this.detail(id);
  return detail
};

exports.delete = async (id, transaction) => {
  const deleted = await address_type.destroy({
     where :  { id : id } , 
  }, transaction);
  return deleted
};
